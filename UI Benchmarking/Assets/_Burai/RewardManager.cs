﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Requirement
{
	public Text requirementText;
	public GameObject requirementClearedImage;
}

[System.Serializable]
public class EionExp
{
	public Image eionIcon;
	public Image eionExpBar;
}

[System.Serializable]
public class ItemPool
{
	public Item item;
	[Range(1, 100)]
	public int minValue = 1;
	[Range(1, 100)]
	public int maxValue = 1;
	[Range(0.01f, 100.00f)]
	public float dropRate = 1;
}

public class RewardManager : MonoBehaviour
{
    #region Singleton

    public static RewardManager instance;
    private void Awake()
    {
        if (!instance)
        {
            instance = this;
        }
    }

    #endregion

    [Header("Star")]
    public GameObject[] stars = new GameObject[3];

	[Header("Stage Status")]
	public Text statusText;
	public Text stageText;

	[Header("Eion")]
	public EionExp leaderEion;	// Center
	public EionExp leftEion;	// Left
	public EionExp rightEion;	// Right

	[Header("Requirement Objects")]
    public Requirement[] requirementHolder = new Requirement[3];
    

    [Header("ActualRequirement")]
    public RequirementsForStar[] requirementsForStars = new RequirementsForStar[3];
    public RequirementsStats requirementsStats = new RequirementsStats();
    public RequirementsStats currentRequirementsStats = new RequirementsStats();

	[Header("Reward")]
	public Transform rewardTable;
	public GameObject itemIcon;
	public List<ItemPool> itemPools;

	[Header("Testing")]
	public bool generateReward = false;
    public bool endResults = false;

    public int EXP;
    public float timerForEXP;
    public int howFastForEXP;
    

	private List<ItemInfo> itemGot = new List<ItemInfo>();

	private List<GameObject> itemRewards = new List<GameObject>();

    private int[] tempSaveDataHolder = new int[3];
    

   

    // Use this for initialization
    void Start ()
    {
        EXP = 0;
        stars[0].SetActive(false);
        stars[1].SetActive(false);
        stars[2].SetActive(false);
        tempSaveDataHolder[0] = 0;
        tempSaveDataHolder[1] = 0;
        tempSaveDataHolder[2] = 0;
            //LevelInfo lI = new LevelInfo();
            //foreach (LevelInfo temp in DataManager.userData.levelInfo){
            //    if (temp.levelID == StageManager.instance.level.levelID)
            //    {
            //        lI = temp;
            //    }
            //}
            //if (lI.levelID != null)
            //{
            //    for (int i = 0; i < lI.missionID.Length; i++)
            //    {
            //        if (lI.missionID[i] == 1)
            //        {
            //            requirementHolder[i].requirementClearedImage.SetActive(true);
            //            stars[i].SetActive(true);
            //            tempSaveDataHolder[i] = lI.missionID[i];
            //        }
                    
            //    }
            //}
            
         //   requirementsForStars[0] = StageManager.instance.level.requirement1;
       //     requirementsForStars[1] = StageManager.instance.level.requirement2;
      //      requirementsForStars[2] = StageManager.instance.level.requirement3;
            requirementsStats.Reset();
            currentRequirementsStats.Reset();
        requirementHolder[0].requirementText.text = requirementsForStars[0].requirement + " : " + requirementsForStars[0].number.ToString();
        requirementHolder[1].requirementText.text = requirementsForStars[1].requirement + " : " + requirementsForStars[1].number.ToString();
        requirementHolder[2].requirementText.text = requirementsForStars[2].requirement.ToString();
        
	}

	private void Update()
	{
        if (generateReward)
		{
			generateReward = false;
			GenerateReward();
		}
        if (endResults)
        {
            if (timerForEXP < 1f)
            {
                timerForEXP += Time.deltaTime;
            }
        }
	}

	public void GenerateReward()
	{
		itemGot.Clear();
		while (itemRewards.Count > 0)
		{
			Destroy(itemRewards[0]);
			itemRewards.RemoveAt(0);
		}

		foreach (ItemPool ip in itemPools)
		{
			if (ip.item != null)
			{
				float isDrop = Random.Range(0.0f, 100.0f);
				if (isDrop <= ip.dropRate)
				{
					ItemInfo temp = new ItemInfo();
					temp.itemIndex = ip.item.index;
					temp.itemAmount = Random.Range(ip.minValue, ip.maxValue);
					itemGot.Add(temp);
				}
			}
		}
		SaveReward();
		DisplayReward();
	}

	private void SaveReward()
	{
		foreach (ItemInfo ia in itemGot)
		{
			Item temp = ItemManager.instance.GetItem(ia.itemIndex);
			if (temp.itemType == ItemType.Currency)
			{
				// Currency
				if (temp.name == "Ein")
				{
					//DataManager.userData.ein += ia.itemAmount;
				}

				if (ItemManager.instance.GetItem(ia.itemIndex).name == "Zulrite")
				{
					//DataManager.userData.zulrite += ia.itemAmount;
				}
			}
		}
		DataManager.UploadData();
	}

	public void DisplayReward()
	{
		foreach(ItemInfo i in itemGot)
		{
			Item tempItem = ItemManager.instance.GetItem(i.itemIndex);
			GameObject temp = Instantiate(itemIcon, rewardTable);
			temp.GetComponent<ItemIconManager>().itemIcon.sprite = tempItem.sprite;
			temp.GetComponent<ItemIconManager>().itemAmount.text = i.itemAmount.ToString();
			itemRewards.Add(temp);
		}
	}

    public void EndGame()
    {
        requirementsStats.SetScore(GameplayManager.instance.GetScore());
        if (RequirementCaculationManager.instance)
        {
            requirementsStats.SetTotalDamage(RequirementCaculationManager.instance.GetTotalDamage());
        }
        
        requirementsStats.SetNumberOfRounds(GameplayManager.instance.GetTurnCount());
        requirementsStats.SetBallClear(LauncherBehaviour.instance.hiddenBalls.Count);
        requirementsStats.SetCombos(ComboManager.instance.GetHighestCombo());
        requirementsStats.SetClear(true);
        StarUp();
        StartCoroutine(UIManager.instance.GetComponent<ClearScreenScript>().PlayAnimation());
        DataManager.UploadData();
        //DataManager.ReadDataFromJson();
        //SkillUsage, Use less eions, use team type, 
    }

    public void StarUp()
    {
        int temp = 0;
        for (int i = 0; i < requirementsForStars.Length; i++)
        {
            if (requirementsForStars[i].requirement == RequirementsTypes.Score)
            {
                if (requirementsForStars[i].number <= requirementsStats.GetScore())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            } else if (requirementsForStars[i].requirement == RequirementsTypes.TotalDamage)
            {
                if (requirementsForStars[i].number <= requirementsStats.GetTotalDamage())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
            else if (requirementsForStars[i].requirement == RequirementsTypes.DamageInOneRound)
            {
                if (requirementsForStars[i].number <= requirementsStats.GetDamageInOneRound())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
            else if (requirementsForStars[i].requirement == RequirementsTypes.Combos)
            {
                if (requirementsForStars[i].number <= requirementsStats.GetCombos())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
            else if (requirementsForStars[i].requirement == RequirementsTypes.NumberOfRounds)
            {
                if (requirementsForStars[i].number <= requirementsStats.GetNumberOfRounds())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
            else if (requirementsForStars[i].requirement == RequirementsTypes.DestroyedObstaclesInOneRound)
            {
                if (requirementsForStars[i].number <= requirementsStats.GetDestroyedObstaclesInOneRound())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
            else if (requirementsForStars[i].requirement == RequirementsTypes.ActivatePowerUp)
            {
                if (requirementsForStars[i].number <= requirementsStats.GetActivatePowerUp())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
            else if (requirementsForStars[i].requirement == RequirementsTypes.AvoidTraps)
            {
                if (requirementsForStars[i].number >= requirementsStats.GetAvoidTraps())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
            else if (requirementsForStars[i].requirement == RequirementsTypes.SkillUsage)
            {
                if (requirementsForStars[i].number <= requirementsStats.GetSkillUsage())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
            else if (requirementsForStars[i].requirement == RequirementsTypes.BallClear)
            {
                if (requirementsForStars[i].number <= requirementsStats.GetBallClear())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
            else if (requirementsForStars[i].requirement == RequirementsTypes.ClearOneWaveUnderXTurn)
            {
                if (requirementsForStars[i].number >= requirementsStats.GetClearOneWaveUnderXTurn())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
            else if (requirementsForStars[i].requirement == RequirementsTypes.UseTeamTypeEions)
            {
                if (requirementsForStars[i].number <= requirementsStats.GetUseTeamTypeEions())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
            else if (requirementsForStars[i].requirement == RequirementsTypes.UseLessEions)
            {
                if (requirementsForStars[i].number <= requirementsStats.GetUseLessEions())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
            else if (requirementsForStars[i].requirement == RequirementsTypes.Clear)
            {
                if (requirementsStats.GetClear())
                {
                    EXP += 1000;
                    requirementHolder[i].requirementClearedImage.SetActive(true);
                    temp++;
                    tempSaveDataHolder[i] = 1;
                }
            }
        }
        for (int i = 0; i < temp; i++)
        {
            if (stars[i])
            {
                if (!stars[i].activeSelf)
                {
                    UIManager.instance.GetComponent<ClearScreenScript>().starsAwaitingAnimation.Add(stars[i]);
                } 
            }
            
        }
    }
    public int[] SaveData()
    {
        return tempSaveDataHolder;
    } 
    
}

[System.Serializable]
public class RequirementsStats
{
    public float Score = 0;
    public float TotalDamage = 0;
    public float DamageInOneRound = 0;
    public int Combos = 0;
    public int NumberOfRounds =1;
    public int DestroyedObstaclesInOneRound = 0;
    public int ActivatePowerUp = 0;
    public int AvoidTraps = 0;
    public int SkillUsage = 0;
    public int BallClear = 0;
    public int ClearOneWaveUnderXTurn = 500;
    public int UseLessEions = 0;
    public int UseTeamTypeEions = 0;
    public bool clear = false;
    /// <summary>
    /// Functions
    /// </summary>
    #region

    public void Reset()
    {
        Score = 0;
        TotalDamage = 0;
        DamageInOneRound= 0;
        Combos= 0;
        NumberOfRounds= 1;
        DestroyedObstaclesInOneRound= 0;
        ActivatePowerUp= 0;
        AvoidTraps= 0;
        SkillUsage= 0;
        BallClear= 0;
        ClearOneWaveUnderXTurn= 500;
        UseLessEions= 0;
        UseTeamTypeEions= 0;
        clear = false;
    }
    public void SetScore(float temp)
    {
        Score = temp;
    }
    public void SetTotalDamage(float temp)
    {
        TotalDamage = temp;
    }
    public void SetDamageInOneRound(float temp)
    {
        DamageInOneRound = temp;
    }
    public void SetCombos(int temp)
    {
        Combos = temp;
    }
    public void SetNumberOfRounds(int temp)
    {
        NumberOfRounds = temp;
    }
    public void SetDestroyedObstaclesInOneRound(int temp)
    {
        DestroyedObstaclesInOneRound = temp;
    }
    public void SetActivatePowerUp(int temp)
    {
        ActivatePowerUp += temp;
    }
    public void SetSkillUsage(int temp)
    {
        SkillUsage = temp;
    }
    public void SetAvoidTraps(int temp)
    {
        AvoidTraps += temp;
    }
    public void SetBallClear(int temp)
    {
        BallClear = temp;
    }
    public void SetClearOneWaveUnderXTurn(int temp)
    {
        ClearOneWaveUnderXTurn = temp;
    }
    public void SetUseLessEions(int temp)
    {
        UseLessEions = temp;
    }
    public void SetUseTeamTypeEions(int temp)
    {
        UseTeamTypeEions = temp;
    }

    public void SetClear(bool temp)
    {
        clear = temp;
    }

    public float GetScore()
    {
        return Score;
    }
    public float GetTotalDamage()
    {
        return TotalDamage;
    }
    public float GetDamageInOneRound()
    {
        return DamageInOneRound;
    }
    public int GetCombos()
    {
        return Combos;
    }
    public int GetNumberOfRounds()
    {
        return NumberOfRounds;
    }
    public int GetDestroyedObstaclesInOneRound()
    {
        return DestroyedObstaclesInOneRound;
    }
    public int GetActivatePowerUp()
    {
        return ActivatePowerUp;
    }
    public int GetSkillUsage()
    {
        return SkillUsage;
    }
    public int GetAvoidTraps()
    {
        return AvoidTraps;
    }
    public int GetBallClear()
    {
        return BallClear;
    }
    public int GetClearOneWaveUnderXTurn()
    {
        return ClearOneWaveUnderXTurn;
    }
    public int GetUseLessEions()
    {
        return UseLessEions;
    }
    public int GetUseTeamTypeEions()
    {
        return UseTeamTypeEions;
    }
    public bool GetClear()
    {
        return clear;
    }

    public void CompareRequirements(RequirementsStats rS)
    {
        if (rS.DamageInOneRound >= DamageInOneRound)
        {
            SetDamageInOneRound(rS.GetDamageInOneRound());
        }
        if (rS.DestroyedObstaclesInOneRound >= DestroyedObstaclesInOneRound)
        {
            SetDestroyedObstaclesInOneRound(rS.DestroyedObstaclesInOneRound);
        }
        if (rS.ClearOneWaveUnderXTurn <= ClearOneWaveUnderXTurn)
        {
            SetClearOneWaveUnderXTurn(rS.ClearOneWaveUnderXTurn);
        }
    }
    #endregion
}
