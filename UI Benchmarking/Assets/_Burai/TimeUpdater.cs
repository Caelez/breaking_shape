﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TimeUpdater : MonoBehaviour {

	/*public static TimeUpdater instance;
	//private Data userData = new Data();
	private TimeSpan energyCurrentTime;
    private double tSeconds;



    private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(this);
		}
		
	}

	public struct JsonDateTime
	{
		public long value;
		public static implicit operator DateTime(JsonDateTime jdt)
		{
            //Debug.Log("Converted to time");
			return DateTime.FromFileTimeUtc(jdt.value);
		}
		public static implicit operator JsonDateTime(DateTime dt)
		{
			//Debug.Log("Converted to JDT");
			JsonDateTime jdt = new JsonDateTime();
			jdt.value = dt.ToFileTimeUtc();
            return jdt;
		}
	}

	// Use this for initialization
	void Start () {
        //userData = DataManager.instance.GetUserData();
        
        if (DataManager.userData.previousDateLastLogin == "")
		{
			DateTime d1 = DateTime.Now;
			d1.AddDays(-1);
			var json = JsonUtility.ToJson((JsonDateTime)d1);
            DataManager.userData.previousDateLastLogin = json;
		}
        Debug.Log(DataManager.userData);
        //Debug.Log(DataManager.userData.previousDateLastLogin);
        DateTime dt = JsonUtility.FromJson<JsonDateTime>(DataManager.userData.previousDateLastLogin);
		DateTime now = DateTime.Now;
		if (dt.Day < now.Day)
		{
			var json = JsonUtility.ToJson((JsonDateTime)now);
            DataManager.userData.previousDateLastLogin = json;
			DataManager.WriteDataInJson();
		}
	}
	
	// Update is called once per frame
	void Update () {
       // DateTime nowTime = DateTime.Now;
       // DateTime newTime = nowTime.AddMinutes((double)((userData.maxStamina - userData.currentStamina) * 5f));
        //var json = JsonUtility.ToJson((JsonDateTime)newTime);
        //userData.estmatedDateTimeBeforeFullStamina = json;
        //DateTime dt = JsonUtility.FromJson<JsonDateTime>(userData.estmatedDateTimeBeforeFullStamina);
      
      
		if (DataManager.userData.estmatedDateTimeBeforeFullStamina != "")
		{
            DateTime CurrentTime = JsonUtility.FromJson<JsonDateTime>(DataManager.userData.estmatedDateTimeBeforeFullStamina);
            CurrentTime = JsonUtility.FromJson<JsonDateTime>(DataManager.userData.estmatedDateTimeBeforeFullStamina);
			double tS = (CurrentTime - DateTime.Now).TotalMinutes;
            tSeconds = (CurrentTime - DateTime.Now).TotalSeconds;
            energyCurrentTime = CurrentTime - DateTime.Now;
			if (DataManager.userData.maxStamina - Math.Ceiling(Math.Ceiling(tS)/5) != DataManager.userData.currentStamina)
			{
                DataManager.userData.currentStamina = DataManager.userData.maxStamina - (int)Math.Ceiling(Math.Ceiling(tS) / 5);
				if (DataManager.userData.currentStamina > DataManager.userData.maxStamina)
				{
                    DataManager.userData.currentStamina = DataManager.userData.maxStamina;
				}
                DataManager.userData.previousStamina = DataManager.userData.currentStamina;
				DataManager.WriteDataInJson();
			}
			if (CurrentTime < DateTime.Now)
			{
                DataManager.userData.estmatedDateTimeBeforeFullStamina = "";
			}
		}
	}

	public void UpdateTimer()
	{
        
		if (DataManager.userData.estmatedDateTimeBeforeFullStamina == "" || DataManager.userData.estmatedDateTimeBeforeFullStamina == null)
		{
			DateTime nowTime = DateTime.Now;
			nowTime = nowTime.AddMinutes((DataManager.userData.maxStamina - DataManager.userData.currentStamina) * 5f);
			string json = JsonUtility.ToJson((JsonDateTime)nowTime);
            DataManager.userData.estmatedDateTimeBeforeFullStamina = json;
			DateTime dt = JsonUtility.FromJson<JsonDateTime>(DataManager.userData.estmatedDateTimeBeforeFullStamina);
		}
		else
		{
			DateTime timeFromJason = JsonUtility.FromJson<JsonDateTime>(DataManager.userData.estmatedDateTimeBeforeFullStamina);
			int newTime = DataManager.userData.previousStamina - DataManager.userData.currentStamina;
			timeFromJason = timeFromJason.AddMinutes(newTime * 5f);
			string json = JsonUtility.ToJson((JsonDateTime)timeFromJason);
            DataManager.userData.estmatedDateTimeBeforeFullStamina = json;
            DataManager.userData.previousStamina = DataManager.userData.currentStamina;
		}
	}

	public TimeSpan GetEnergyDateTime()
	{
		return energyCurrentTime;
	}

    public double GetSecondsEnegeryDateTime()
    {
        return tSeconds;
    }

	//public Data GetData()
	//{
	//	return userData;
	//}*/
}
