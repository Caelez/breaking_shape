﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour {

    public bool DestroyObjectAnimator;
    float timer;

	// Use this for initialization
	void Start () {
	}
	
	

    public IEnumerator DestroyAfterAnimation()
    {
        yield return new WaitForSeconds(GetComponent<Animator>().GetCurrentAnimatorClipInfo(0).Length);
        Destroy(gameObject);
    }
 }
