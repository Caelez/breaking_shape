﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCounterScript : MonoBehaviour {

    private float currentDamage;
    private float timer;
    private Vector3 baseSize;
    private Vector3 basePosition;
    public float sizeOfNumbers;
    public bool critColor;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (timer < 1f)
        {
            timer += Time.deltaTime;
        }
        else
        {
            Destroy(this.gameObject);
        }
        transform.GetChild(0).GetComponent<TextMesh>().text = currentDamage.ToString();
        transform.GetChild(0).GetChild(0).GetComponent<TextMesh>().text = currentDamage.ToString();
        transform.position = Vector3.Lerp(basePosition, transform.up + basePosition, timer );
        if (!critColor)
        {
            transform.localScale = Vector3.Lerp(baseSize, new Vector3(3,3,3), timer);
        }
    }

    public void UpdateDamageCounter(float damage)
    {
        currentDamage += damage;
        timer = 0f;
        basePosition = transform.position;
        baseSize = new Vector3(1f,1f,1f);
        sizeOfNumbers = 100;
    }


}
