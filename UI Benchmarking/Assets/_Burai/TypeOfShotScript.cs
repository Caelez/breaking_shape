﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TypeOfShotScript : MonoBehaviour {

    public List<GameObject> ActivatedBalls = new List<GameObject>();
    public TypeOfShot typeOfShot;
    public bool splitOnCommand;
    public bool explosionOnCommand;
    public bool explosionOnImpact;
    public bool released;
    public int typeOfShotInt;
    public GameObject shotTypeSprites;
    public List<GameObject> ExplosionEffects;
    public List<GameObject> TempBalls;
    public Sprite[] spriteOfShotTypes = new Sprite[6];
    
    [SerializeField] private float speed = 10.0f;

    public GameObject ball;
    // Use this for initialization
    void Start () {
        typeOfShotInt = (int)typeOfShot;
        shotTypeSprites.GetComponent<SpriteRenderer>().sprite = spriteOfShotTypes[typeOfShotInt];
        if (typeOfShot == 0)
        {
            shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor = Color.cyan;
        }
        else if (typeOfShot == 0)
        {
            shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor = Color.blue;
        }
        else if (typeOfShot == 0)
        {
            shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor = new Color(252, 102, 0);
        }
        else if (typeOfShot == 0)
        {
            shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor = Color.yellow;
        }
        else if (typeOfShot == 0)
        {
            shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor = Color.red;
        }
        else if (typeOfShot == 0)
        {
            shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor = Color.white;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Shoot(List<GameObject> temp)
    {
        StartCoroutine(ShootTheBalls(temp));
    }

    public void ActivateActivatedBallFromActivated(GameObject newBall)
    {
        if (typeOfShot == TypeOfShot.Impact)
        {
            if (ActivatedBalls.Contains(newBall))
            {
                foreach (GameObject go in LauncherBehaviour.instance.balls)
                {
                    go.SetActive(true);
                    go.transform.position = newBall.transform.position;
                    go.layer = 8;
                    go.transform.GetComponent<BallBehaviour>().SetReadyToFire(false);
                    go.transform.rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
                    go.GetComponent<Rigidbody2D>().AddForce(go.transform.up * speed, ForceMode2D.Impulse);
                }
                ActivatedBalls.Remove(newBall);
                Destroy(newBall);
                return;
            }
        }
        if (explosionOnImpact)
        {
//            GameObject g = Instantiate(GameplayManager.instance.AOEEffect, transform.position, Quaternion.identity);
//            ExplosionEffects.Add(g);
//            g.GetComponent<AOEEffectScript>().SetData(newBall.GetComponent<BallBehaviour>().GetBaseDamage(), 500, newBall, newBall.GetComponent<BallBehaviour>().GetAdditionalDamage(), newBall.GetComponent<BallBehaviour>().GetCritRate(), newBall.GetComponent<BallBehaviour>().GetCritDamage());
//            g.GetComponent<CircleCollider2D>().enabled = true;
            newBall.GetComponent<BallBehaviour>().ResetObject() ;
        }
    }

    public void RemoveBallFromActivated(GameObject gO)
    {
        ActivatedBalls.Remove(gO);
        BallPool.instance.Despawn(gO);
        gO.GetComponent<BallBehaviour>().StopAllCoroutines();
    }

    IEnumerator ShootTheBalls(List<GameObject> list)
    {
        List<GameObject> tempList = new List<GameObject>();
        explosionOnImpact = false;
        foreach (GameObject g in list)
        {
            tempList.Add(g);
        }
        foreach (GameObject g in tempList)
        {
            g.GetComponent<Collider2D>().isTrigger = false;
        }
        if (typeOfShot == TypeOfShot.Straight)
        {
            foreach (GameObject g in tempList)
            {
                g.SetActive(true);
                g.transform.position = this.transform.position;
                g.transform.rotation = this.transform.rotation;
                g.GetComponent<Rigidbody2D>().AddForce(transform.up * speed, ForceMode2D.Impulse);
                g.GetComponent<BallBehaviour>().SetReadyToFire(false);
                
                yield return new WaitForSeconds(0.2f);
            }
        }
        else if (typeOfShot == TypeOfShot.Impact)
        {
            GameObject newBall = Instantiate(ball);
            newBall.SetActive(true);
            ActivatedBalls.Add(newBall);
            newBall.transform.position = this.transform.position;
            newBall.transform.rotation = this.transform.rotation;
            newBall.name = "NewBall";
            newBall.GetComponent<Rigidbody2D>().AddForce(newBall.transform.up * speed, ForceMode2D.Impulse);

            newBall.transform.localScale = new Vector3(.35f, .35f, 1) + new Vector3(list.Count * .15f, list.Count * .15f, 0);
            if (newBall.transform.localScale.x > 2f)
            {
                newBall.transform.localScale = new Vector3(4, 4, 4);
            }
        }
        else if (typeOfShot == TypeOfShot.Split)
        {
            int temp = 0;
            foreach (GameObject g in tempList)
            {
                if (tempList.Count % 2 == 1)
                {
                    g.SetActive(true);
                    g.transform.position = this.transform.position;
                    Quaternion rotation = new Quaternion();
                    if (list.Count < 7)
                    {
                        rotation = this.transform.rotation * Quaternion.Euler(0, 0, 30 * (temp - (list.Count / 2)));
                    }
                    else
                    {
                        rotation = this.transform.rotation * Quaternion.Euler(0, 0, 80 + (160f * ((float)temp / (float)tempList.Count)));
                    }
                    g.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotation, 1);
                    g.GetComponent<Rigidbody2D>().AddForce(g.transform.up * speed, ForceMode2D.Impulse);
                    temp++;
                }
                else
                {
                    g.SetActive(true);
                    g.transform.position = this.transform.position;
                    Quaternion rotation = new Quaternion();
                    if (tempList.Count < 7)
                    {
                        rotation = this.transform.rotation * Quaternion.Euler(0, 0, 15 * (temp - (list.Count / 2)));
                    }
                    else
                    {
                        rotation = this.transform.rotation * Quaternion.Euler(0, 0, 80 + (160f * ((float)temp / (float)tempList.Count)));
                    }
                    g.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotation, 1);
                    g.GetComponent<Rigidbody2D>().AddForce(g.transform.up * speed, ForceMode2D.Impulse);
                    temp++;                 
                }
                g.GetComponent<BallBehaviour>().SetReadyToFire(false);              
            }            
        }
        else if (typeOfShot == TypeOfShot.Gatling)
        {
            foreach (GameObject g in tempList)
            {
                g.SetActive(true);
                g.transform.position = this.transform.position;
                Quaternion rotation = this.transform.rotation * Quaternion.Euler(0, 0, Random.Range(-10, 10));
                g.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotation, 1);
                g.GetComponent<Rigidbody2D>().AddForce(g.transform.up * speed*2f, ForceMode2D.Impulse);
                g.GetComponent<BallBehaviour>().SetReadyToFire(false);
                yield return new WaitForSeconds(0.05f);
            }            
        }
        else if (typeOfShot == TypeOfShot.Shotgun)
        {
            foreach (GameObject g in tempList)
            {
                g.SetActive(true);
                g.transform.position = this.transform.position;
                Quaternion rotation = this.transform.rotation * Quaternion.Euler(0, 0, Random.Range(-45f, 45f));
                g.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotation, 1);
                g.GetComponent<Rigidbody2D>().AddForce(g.transform.up * speed *4f, ForceMode2D.Impulse);
                g.GetComponent<BallBehaviour>().SetReadyToFire(false);
            }
        }
        else if (typeOfShot == TypeOfShot.Penetration)
        {
            foreach (GameObject g in tempList)
            {
                g.GetComponent<Collider2D>().isTrigger = true;
                g.SetActive(true);
                g.transform.position = this.transform.position;
                g.transform.rotation = this.transform.rotation;
                g.GetComponent<Rigidbody2D>().AddForce(g.transform.up * speed, ForceMode2D.Impulse);
                g.GetComponent<BallBehaviour>().SetReadyToFire(false);
                yield return new WaitForSeconds(0.2f);
            }
        }else if (typeOfShot == TypeOfShot.Explosive)
        {
            explosionOnImpact = true;
            foreach (GameObject g in tempList)
            {
                g.SetActive(true);
                g.transform.position = this.transform.position;
                g.transform.rotation = this.transform.rotation;
                g.GetComponent<Rigidbody2D>().AddForce(transform.up * speed, ForceMode2D.Impulse);
                g.GetComponent<BallBehaviour>().SetReadyToFire(false);
                yield return new WaitForSeconds(0.2f);
            }
        }
        yield return new WaitForSeconds(0);
    }

    public bool CheckIfAllBallsHome()
    {
        foreach (GameObject ball in LauncherBehaviour.instance.balls)
        {
            if (!ball.GetComponent<BallBehaviour>().GetReadyToFire())
            {
                return false;
            }
        }
        return true;  
    }

    public bool CheckIfExplosionsAreStillAround()
    {
        if (ExplosionEffects.Count > 0)
        {
            return true;
        }
        return false;
    }

    public void AddToTempList(GameObject g)
    {
        TempBalls.Add(g);
    }

    public void changeBulletStyle()
    {
        typeOfShotInt++;
       
        if (typeOfShotInt >= 4)
        {
            typeOfShotInt = 0;
        }
        if (typeOfShotInt == 0)
        {
            shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor = Color.cyan;
        }
        else if (typeOfShotInt == 1)
        {
            shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor = Color.blue;
          
        }
        else if (typeOfShotInt == 2)
        {
            shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor = shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor2;
        }
        else if (typeOfShotInt == 3)
        {
            shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor = Color.yellow;
        }
        else if (typeOfShotInt == 4)
        {
            shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor = Color.red;
        }
        else if (typeOfShotInt == 5)
        {
            shotTypeSprites.GetComponent<SpriteGlow.SpriteGlowEffect>().GlowColor = Color.white;
        }
        shotTypeSprites.GetComponent<SpriteRenderer>().sprite = spriteOfShotTypes[typeOfShotInt];
        typeOfShot = (TypeOfShot)typeOfShotInt;
    }
}
