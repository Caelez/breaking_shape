﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DyrionScript : MonoBehaviour {

	public bool test;
	int currentCounter = 0;
	int maxCounter = 0;
	int bitshift;
	public TypeOfDyrion typeOfDyrion;
	public GameObject Pentagrams;
    public GameObject Shield;
    public GameObject currentShield;
    int amountOfShield = 0;


	public enum TypeOfDyrion
	{
		None = 0,
		Circles,
		Triangle,
		Diamond,
		Pentagon,
		Hexagon,
		Square,
		Max
	}

	// Use this for initialization
	void Start () {
		bitshift = 1 << 10;
		bitshift = ~bitshift;
        ActivateHexagonEffect();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.K))
		{
			UpdateDyrionCounter();
		}
	}

	public void SetDyrion(TypeOfDyrion tOD, int Cooldown)
	{
		typeOfDyrion = tOD;
		maxCounter = Cooldown;
	}

	public void UpdateDyrionCounter()
	{
		if (currentCounter > maxCounter - 1)
		{
			if (typeOfDyrion == TypeOfDyrion.Circles)
			{
				GetComponent<Collider2D>().enabled = false;
				RaycastHit2D raycastHit2DLeft = Physics2D.Raycast(transform.position, -transform.right, 100, bitshift);
				RaycastHit2D raycastHit2DRight = Physics2D.Raycast(transform.position, transform.right, 100, bitshift);
				
				if (raycastHit2DLeft.transform.GetComponent<ObstacleBehaviour>() != null)
				{
					raycastHit2DLeft.transform.GetComponent<ObstacleBehaviour>().SetHealth(raycastHit2DLeft.transform.GetComponent<ObstacleBehaviour>().GetHealth() + 10f);
				}
				if (raycastHit2DRight.transform.GetComponent<ObstacleBehaviour>() != null)
				{
					raycastHit2DRight.transform.GetComponent<ObstacleBehaviour>().SetHealth(raycastHit2DRight.transform.GetComponent<ObstacleBehaviour>().GetHealth() + 10f);
				}
				GetComponent<Collider2D>().enabled = true;
			}
			else if (typeOfDyrion == TypeOfDyrion.Triangle)
			{
				GetComponent<MapObjectsBase>().MoveUp();
			}
			else if (typeOfDyrion == TypeOfDyrion.Diamond)
			{
                GetComponent<Collider2D>().enabled = false;
            }
			else if (typeOfDyrion == TypeOfDyrion.Hexagon)
			{

			}
			currentCounter = 0;
		}
        if (currentCounter > 1)
        {
            GetComponent<Collider2D>().enabled = true;
        }
		currentCounter++;
	}

	public TypeOfDyrion GetDyrionType()
	{
		return typeOfDyrion;
	}

	public void ActivateEffects()
	{
		if (TypeOfDyrion.Pentagon == typeOfDyrion)
		{
			Instantiate(Pentagrams, transform.position, Quaternion.identity);
			Vector2 tempVector2 = new Vector2(transform.position.x,transform.position.y);
			bool tempBool = true;
			int tries = 0;
			do
			{
				tries++;
				tempVector2 = tempVector2 + (Random.insideUnitCircle * 2f);
				Collider2D[] c2D = Physics2D.OverlapCircleAll(tempVector2, Pentagrams.GetComponent<SpriteRenderer>().bounds.size.x / 2f);
				if (c2D.Length < 1)
				{
					tempBool = false;
				}
				
			} while (tempBool && tries < 50);
            if (tries < 50)
            {
                Instantiate(Pentagrams, new Vector3(tempVector2.x, tempVector2.y, 0), Quaternion.identity);
            }
		}
	}

    public void ActivateSquareEffects()
    {
        if (typeOfDyrion == TypeOfDyrion.Square)
        {
            GetComponent<ObstacleBehaviour>().SetHealth(GetComponent<ObstacleBehaviour>().GetHealth());
        }
    }

    public void ActivateHexagonEffect()
    {
        typeOfDyrion = TypeOfDyrion.Hexagon;
        amountOfShield = 10;
        currentShield = Instantiate(Shield, transform.position, Quaternion.identity);
        currentShield.transform.SetParent(transform);
    }

    public void MinusShieldHP()
    {
        if (TypeOfDyrion.Hexagon != typeOfDyrion)
        {
            return;
        }
        amountOfShield--;
        if (amountOfShield <= 0)
        {
            Destroy(currentShield);
        }
    }

    public int GetShieldHP()
    {
        return amountOfShield;
    }
}
