﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObjectsBase : MonoBehaviour
{
	protected int iD;
	protected float speed = 2.0f;
    protected string descriptionOfObject;
    protected Vector3 destination = Vector3.zero;

	protected bool b_move = false;
	protected bool cantMove = false;
	protected bool isPushedBelow;
	public bool deathAlready;
	public bool set;
	[SerializeField] protected float m_health;
    protected float baseHealth;
   
    protected bool firstTimeBaseHealth = false;
	protected GameObject mirroredObject;
	public int placedID;
	private bool once;
    protected bool hitWithSkill;
    protected bool rotateAround;

	public enum WhichSide
	{
		Left,
		Middle,
		Right,
		Random,
		Max
	}

	protected WhichSide whichSide;

	protected virtual void Update()
	{
        MoveToDestination();
    }

	protected virtual void ResetObjectData()
    {
        speed = 2.0f;
        destination = transform.position;
        b_move = false;
        isPushedBelow = false;
    }

    public void ResetGameObject()
    {
        ResetObjectData();
    }

    public virtual float GetBaseHP()
    {
        return baseHealth;
    }

    public virtual float GetHealth()
    {
        return m_health;
    }

    public virtual float GetSpeed()
    {
        return speed;
    }

    public virtual string GetDescription()
    {
        return descriptionOfObject;
    }

    public virtual void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

	public virtual void MoveUpInvoke()
	{
		Invoke("MoveUp", .05f);
	}

    public virtual void HitWithSkill(bool temp)
    {
        hitWithSkill = temp;
        Debug.Log("Temp");
    }

	public virtual void MoveUp()
    {
        if (StageManager.instance != null)
		{
            b_move = false;

            destination = transform.position;
			Vector3 tempVector3 = transform.position + (Vector3.up * GetMoveUp()) ;
			Collider2D[] c2D = Physics2D.OverlapCircleAll(tempVector3, GetComponent<Collider2D>().bounds.size.x * .5f);
			bool test = false;

			if (c2D.Length == 0)
			{
				destination = tempVector3;
                b_move = true;
				test = true;
			}

			if (!test)
			{
				foreach (Collider2D c2 in c2D)
				{
					if (c2.GetComponent<MapObjectsBase>() != null)
					{
						if (!c2.GetComponent<MapObjectsBase>().GetCantMove())
						{
                            if ( c2.GetComponent<MapObjectsBase>().GetBMove())
                            {
                                destination = tempVector3;
                                b_move = true;
                                test = true;
                                break;
                            }
						}
					}
					else if (!c2.CompareTag("Collector"))
                    {
                        destination = tempVector3;
                        b_move = true;
                        test = true;
                        break;
                    }
                }
			}
			
			if (!test)
			{
                bool whichSideToCheck = Random.value > .5f;

                if (whichSideToCheck)
                {
                    tempVector3 = destination + (Vector3.up * GetMoveUp()) + -(Vector3.right * GetMoveUp());
                }
                else
                {
                    tempVector3 = destination + (Vector3.up * GetMoveUp()) + (Vector3.right * GetMoveUp());
                }
                if (CheckIfDestinationIsFree(tempVector3))
                {
                    destination = tempVector3;
                    b_move = true;
                }
                whichSideToCheck = !whichSideToCheck;
                if (whichSideToCheck)
                {
                    tempVector3 = destination + (Vector3.up * GetMoveUp()) + -(Vector3.right * GetMoveUp());
                }
                else
                {
                    tempVector3 = destination + (Vector3.up * GetMoveUp()) + (Vector3.right * GetMoveUp());
                }
                if (CheckIfDestinationIsFree(tempVector3))
                {
                    destination = tempVector3;
                    b_move = true;
                }
            }
            if (StageManager.instance)
            {
                foreach (GameObject mOB in StageManager.instance.obstacles)
                {
                    if (mOB != this.gameObject)
                    {
                        if (mOB.GetComponent<MapObjectsBase>() != null)
                        {
                            if (Vector3.Distance(destination, mOB.GetComponent<MapObjectsBase>().getDestination()) < Mathf.Abs(GetComponent<Collider2D>().bounds.size.x / 2f + mOB.GetComponent<Collider2D>().bounds.size.x / 2f))
                            {
                                b_move = false;
                                destination = transform.position;

                            }
                            if (Vector3.Distance(destination, mOB.transform.position) < Mathf.Abs(GetComponent<Collider2D>().bounds.size.x / 2f + mOB.GetComponent<Collider2D>().bounds.size.x / 2f))
                            {
                                if (mOB.GetComponent<MapObjectsBase>().GetCantMove())
                                {
                                    b_move = false;
                                    destination = transform.position;
                                }
                                else if (!mOB.GetComponent<MapObjectsBase>().GetBMove())
                                {
                                    b_move = false;
                                    destination = transform.position;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        else if (SpawnerBehaviour.instance)
		{
			destination = transform.position + Vector3.up;
			b_move = true;
		}

		return;
	
	}

	public virtual void MoveDown()
    {
        destination = destination + Vector3.down;
        b_move = true;
	}

	public void SetMirroredObject(GameObject mO)
	{
		mirroredObject = mO;
	}

	public GameObject GetMirroredObject()
	{
		return mirroredObject;
	}

	public void SetWhichSide(WhichSide wS)
	{
		whichSide = wS;
	}

	public WhichSide GetWhichSide()
	{
		return whichSide;
	}

	public bool GetCantMove()
	{
		return cantMove;
	}

	public bool GetBMove()
	{
		return b_move;
	}

	public Vector3 getDestination()
	{
		return destination;
	}

    public void MoveToDestination()
    {
        
        if (b_move && !cantMove)
        {
            transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime * speed);

            if (Mathf.Abs(transform.position.y - destination.y) < 0.1f)
            {
                transform.position = destination;
                b_move = false;
            }
        }
        else
        {
            destination = transform.position;
            b_move = false;
        }
        if (rotateAround)
        {

            transform.Rotate(0, 1, 0);
        }
    }



   

    public virtual void CheckAfterEffect()
    {
        if (isPushedBelow) // Check is being pushed down
        {
            isPushedBelow = false;
            GetComponent<Collider2D>().enabled = true;
            GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    public virtual void SetID(int i)
    {
        iD = i;
    }


	private bool CheckIfDestinationIsFree(Vector3 newDestination)
	{

		Collider2D[] c2D = Physics2D.OverlapCircleAll(newDestination, GetComponent<Collider2D>().bounds.size.x / 2f);
		foreach (Collider2D c2 in c2D)
		{
			if (c2.GetComponent<MapObjectsBase>() != null)
			{
				if (!c2.GetComponent<MapObjectsBase>().GetCantMove())
				{
					return false;
				}
				else
				{
					foreach (GameObject mOB in StageManager.instance.obstacles)
					{
						if (mOB != this.gameObject)
						{
							if (Vector3.Distance(newDestination, mOB.GetComponent<MapObjectsBase>().getDestination()) < Mathf.Abs(GetComponent<Collider2D>().bounds.size.x/2f + mOB.GetComponent<Collider2D>().bounds.size.x/2f))
							{
								return false;
							}
							if (Vector3.Distance(newDestination, mOB.transform.position) < Mathf.Abs(GetComponent<Collider2D>().bounds.size.x/2f + mOB.GetComponent<Collider2D>().bounds.size.x/2f))
							{
								return false;
							}
						}
					}
					return true;
				}
			}
			else if (c2.CompareTag("Wall"))
			{
				return false;
			}
		}
		if (c2D.Length == 0)
		{
			return true;
		}
		return false;
	}

    public int GetMoveUp()
    {
        int moves;
        if (GameplayManager.instance.GetTurnCount() < 5)
        {
            moves = 2;
        }
        else if (GameplayManager.instance.GetTurnCount() < 15)
        {
            moves = 3;
        }
        else if (GameplayManager.instance.GetTurnCount() < 35)
        {
            moves = 4;
        }
        else if (GameplayManager.instance.GetTurnCount() < 75)
        {
            moves = 5;
        }
        else
        {
            moves = 6;
        }
        return moves;
    }
}
