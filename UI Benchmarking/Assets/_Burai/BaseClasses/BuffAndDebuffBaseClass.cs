﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffAndDebuffBaseClass : MonoBehaviour {

    public bool beingDestroyed;
    private bool buff;
    private bool permanent;
    private int duration;
    private int damageIncrease;
    private int damageRequiredForBreak;
    private int layer;
    private float mass;
    private float otherIncrease;
    private Vector3 scale;
    private StatusEffects sE;

    
	// Use this for initialization
	void Start () {
		
	}
	

    public void SetData(bool buffOrNot, bool permananentOrNot, int durationOfEffect, int damage, int changeLayer, int damageRequired, float increase, float gravity, StatusEffects statusEffects)
    {
        buff = buffOrNot;
        permanent = permananentOrNot;
        duration = durationOfEffect;
        damageIncrease = damage;
        damageRequiredForBreak = damageRequired;
        otherIncrease = increase;
        mass = gravity;
        layer = changeLayer;
        sE = statusEffects;

        Activate();
    }

    private void Activate()
    {
       ActivateDebuffs();
    }

    public int GetDamage()
    {
        return damageIncrease;
    }

    public float GetIncrease()
    {
        return otherIncrease;
    }

    public float GetMass()
    {
        return mass;
    }

    public int GetLayer()
    {
        return layer;
    }

    public StatusEffects GetStatusEffect()
    {
        return sE;
    }

    public void DamageThis()
    {
        damageRequiredForBreak--;
    }

    public int HowManyHitsMore()
    {
        return damageRequiredForBreak;
    }



    private void ActivateDebuffs()
    {
        if (sE == StatusEffects.Frozen)
        {
            // gameObject.layer = 0;
            LauncherBehaviour.instance.balls.Remove(this.gameObject);
            StageManager.instance.obstacles.Add(this.gameObject);
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        }
        else if (sE == StatusEffects.Spectered)
        {
            GetComponent<Collider2D>().enabled = false;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
    } 

    public void ActivateBlackHole()
    {
        Vector3 newPosition = transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        Vector3 dir = newPosition - transform.position;
        dir = -dir.normalized;
        GetComponent<Rigidbody2D>().AddForce(dir.normalized * 50.0f, ForceMode2D.Impulse);
    }

    public void ReduceDuration()
    {
        if (!permanent)
        {
            duration--;
        }
        
        if (duration == 0 && !permanent)
        {
            beingDestroyed = true;
            Destroy(this);
            if (sE == StatusEffects.Frozen)
            {
                LauncherBehaviour.instance.balls.Add(this.gameObject);
                StageManager.instance.obstacles.Remove(this.gameObject);
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            }
        }
    }

    public void ChangeFromFrozen()
    {
        LauncherBehaviour.instance.balls.Add(this.gameObject);
        StageManager.instance.obstacles.Remove(this.gameObject);
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
    }
}
