﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowFadeOutScript : MonoBehaviour {

    private float timer;
    private Vector3 baseSize;
    private float destroyTimer;

    // Use this for initialization
    void Start () {
        GetComponent<Rigidbody2D>().mass = Random.Range(.5f, 1.5f);
        GetComponent<Rigidbody2D>().gravityScale = Random.Range(.5f, 1.5f);
        transform.localScale = new Vector3(Random.Range(.3f, 1f), Random.Range(.3f, 1f));
        baseSize = transform.localScale;
        destroyTimer = Random.Range(.8f, 1.1f);
       // GetComponent<SpriteGlow.SpriteGlowEffect>().GlowBrightness = 10;
    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        transform.localScale = Vector3.Lerp(baseSize, new Vector3(0.05f, 0.05f, 0f), timer);
       // GetComponent<SpriteGlow.SpriteGlowEffect>().GlowBrightness = Mathf.Lerp(10, 0, timer);
        if (timer > destroyTimer)
        {
            gameObject.SetActive(false);
        }
	}

    public void ResetObject()
    {
        timer = 0;
        GetComponent<Rigidbody2D>().mass = Random.Range(.5f, 1.5f);
        GetComponent<Rigidbody2D>().gravityScale = Random.Range(.5f, 1.5f);
        transform.localScale = new Vector3(Random.Range(.3f, 1f), Random.Range(.3f, 1f));
        baseSize = transform.localScale;
        destroyTimer = Random.Range(.8f, 1.1f);
       // GetComponent<SpriteGlow.SpriteGlowEffect>().GlowBrightness = 10;
    }
}
