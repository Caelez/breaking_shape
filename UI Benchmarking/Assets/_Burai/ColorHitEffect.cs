﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorHitEffect : MonoBehaviour {

    public Gradient NoneEffect;
    public Gradient FireEffect;
    public Gradient ThunderEffect;
    public Gradient NatureEffect;
    public Gradient WaterEffect;
    public Gradient EarthEffect;
    private bool animationDone = true;
    

    // Use this for initialization
    void Start () {
		
	}
	
	

    public void GradientChange( )
    {
        ElementType eT = (ElementType)(Random.Range(0, (int)ElementType.Total_ElementType));
        gameObject.SetActive(true);
        animationDone = false;
        GetComponent<Animator>().Play("BasicHitEffect", -1, 0f);
        if (eT == ElementType.None)
        {
            GetComponent<SpriteRenderer>().color = NoneEffect.Evaluate(Random.Range(0f, 1f));
        }
        else if (eT == ElementType.Fire)
        {
            GetComponent<SpriteRenderer>().color = FireEffect.Evaluate(Random.Range(0f, 1f));
        }
        else if (eT == ElementType.Thunder)
        {
            GetComponent<SpriteRenderer>().color = ThunderEffect.Evaluate(Random.Range(0f, 1f));
        }
        else if (eT == ElementType.Nature)
        {
            GetComponent<SpriteRenderer>().color = NatureEffect.Evaluate(Random.Range(0f, 1f));
        }
        else if (eT == ElementType.Earth)
        {
            GetComponent<SpriteRenderer>().color = EarthEffect.Evaluate(Random.Range(0f, 1f));
        }
        else if (eT == ElementType.Water)
        {
            GetComponent<SpriteRenderer>().color = WaterEffect.Evaluate(Random.Range(0f, 1f));
        }
        
        StartCoroutine(AnimationDone());
    }

    private IEnumerator AnimationDone()
    {

        yield return new WaitForSeconds(GetComponent<Animator>().GetCurrentAnimatorClipInfo(0).Length);
        animationDone = true;
        gameObject.SetActive(false);
    }

    public bool GetIfAnimationDone()
    {
        return animationDone;
    }
}
