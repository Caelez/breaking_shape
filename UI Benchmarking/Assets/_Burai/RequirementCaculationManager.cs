﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequirementCaculationManager : MonoBehaviour {

    public static RequirementCaculationManager instance;

    float totalDamage;
    float damageInOneTurn;
    int clearWaveByTurn = 5000;
    int destroyedObstaclesInOneRound;


    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }
    public void ResetOneTurn()
    {
        damageInOneTurn = 0;
        destroyedObstaclesInOneRound = 0;
    }

    public void ResetWave()
    {
        clearWaveByTurn = 5000;
        damageInOneTurn = 0;
        destroyedObstaclesInOneRound = 0;
    }

    public void SetDamageInOneTurn(float temp)
    {
        damageInOneTurn += temp;
    }

    public void SetClearWaveBy(int temp)
    {
        clearWaveByTurn = temp;
    }

    public void SetDestroyedObstaclesInOneRound(int temp)
    {
        destroyedObstaclesInOneRound += temp;
    }

    public void SetTotalDamage(float temp)
    {
        totalDamage += temp;
    }

    public float GetDamage()
    {
        return damageInOneTurn;
    }

    public int GetClearWaveRounds()
    {
        return clearWaveByTurn;
    }

    public int GetDestroyedObjects()
    {
        return destroyedObstaclesInOneRound;
    }
    public float GetTotalDamage()
    {
        return totalDamage;
    }

    public void ChangeRewardManager()
    {
        RewardManager.instance.currentRequirementsStats.SetClearOneWaveUnderXTurn(clearWaveByTurn);
        RewardManager.instance.currentRequirementsStats.SetDamageInOneRound(damageInOneTurn);
        RewardManager.instance.currentRequirementsStats.SetDestroyedObstaclesInOneRound(destroyedObstaclesInOneRound);
        RewardManager.instance.requirementsStats.CompareRequirements(RewardManager.instance.currentRequirementsStats);
    }
}
