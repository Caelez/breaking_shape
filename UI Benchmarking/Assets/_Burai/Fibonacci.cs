﻿#define Fibonacci
using System;

public class Fibonacci {



    public static float FibonacciCallStraightEXP(int n)
    {
        float a = 500f;

        // In N steps compute Fibonacci sequence iteratively.
        for (int i = 0; i < n - 1; i++)
        {
            float temp = a;
            a *= .25f;
            a += temp;
        }
        return a;
    }

    public static float FibonacciCallALLEXP(int n)
    {
        float a = 500f;
        float b = 500f;
        if (n < 1)
        {
            a = 0;
            b = 0;
        }
       
        // In N steps compute Fibonacci sequence iteratively.
        for (int i = 0; i < n - 1; i++)
        {
            float temp = a;
            a *= .25f;
            a += temp;
            b += a;
        }
        return b;
    }
}

