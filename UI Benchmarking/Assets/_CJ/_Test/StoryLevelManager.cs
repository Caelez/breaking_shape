﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct LevelButton
{
	public GameObject levelButton;
	public string conditionLevelID;
}

public class StoryLevelManager : MonoBehaviour
{
	public List<LevelButton> levelButtons = new List<LevelButton>();

	// Use this for initialization
	void Start ()
	{
		CheckLevelClear();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void CheckLevelClear()
	{
		//if (DataManager.userData.levelInfo.Count > 0)
		//{
		//	foreach (LevelButton lb in levelButtons)
		//	{
		//		lb.levelButton.SetActive(false);
		//		foreach (LevelInfo li in DataManager.userData.levelInfo)
		//		{
		//			if (li.levelID == lb.conditionLevelID)
		//			{
		//				lb.levelButton.SetActive(true);
		//				lb.levelButton.GetComponent<Button>().interactable = li.isClear;

		//				break;
		//			}
		//		}
		//	}
		//}
		//else
		//{
		//	foreach (LevelButton lb in levelButtons)
		//	{
		//		lb.levelButton.SetActive(false);
		//	}
		//}

		foreach (LevelButton lb in levelButtons)
		{
			lb.levelButton.SetActive(false);
		}

		//DataManager.userData.level;
	}
}
