﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PlayerInfoManager : MonoBehaviour
{
    #region Singleton

    public static PlayerInfoManager instance;
    private void Awake()
    {
        if (!instance)
        {
            instance = this;
        }
    }

    #endregion

    public Image icon;
    public Text userName;
    public Text level;
    public Text exp;
    public Text ein;// common in-game currency
    public Text zulrite;// credit currency (crystal)
	public Text[] texts = new Text[6];
    public Text debugs;


	// Use this for initialization
	void Start ()
    {
        Time.timeScale = 1.0f;
		UpdateUICall();
      
		StartCoroutine(UpdateUI());
    }
	
	// Update is called once per frame
	void Update ()
    {

    }



    public void UpdateUICall()
    {
        texts[0].text = DataManager.userData.username;
		//texts[1].text = DataManager.userData.level.ToString();
		
		//texts[5].text = DataManager.userData.currentStamina.ToString() + " / " + DataManager.userData.maxStamina.ToString();
	}

	IEnumerator UpdateUI()
	{
		do
		{
            UpdateUICall();
            yield return new WaitForSeconds(.5f);
		} while (true);
	}
}
