﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SnapScrollRect : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
	public ScrollRect scrollRect;
	public RectTransform rectTransform; // content rect transform
	public List<Vector3> pagePostions; // all the pages position

	public Vector3 currentPosition;
	public Vector3 targetPosition;

	public int selectedPage;
	public float speed = 1.0f;
	private float distance; // disatance between each pages
	private bool isDrag = false;
	//private bool isDragging = false;

	//public bool selectedPos;

	[Header("Testing")]
	public bool update;

	// Use this for initialization
	void Start ()
	{
		rectTransform = scrollRect.content; // Get the rect transfrom of the content
		distance = rectTransform.rect.width / rectTransform.childCount; // Get the distance of each pages

		float offsetX = rectTransform.rect.width / 2.0f; // to calculate the half width of rect

		for (int i = 0; i < rectTransform.childCount; ++i)
		{
			float posX = distance * i - offsetX + (distance / 2.0f);
			pagePostions.Add(Vector3.left * posX);
			//Debug.Log(distance * i - offsetX + (distance / 2.0f));
		}
		
		selectedPage = 0;
		UpdatePosition();
	}

	// Update is called once per frame
	void Update ()
	{
		//Debug.Log(rect.content.position);
		if (update)
		{
			update = false;
			UpdatePosition();
		}

		if (!isDrag)
		{
			if (rectTransform.localPosition != targetPosition)
			{
				rectTransform.localPosition = Vector3.Lerp(rectTransform.localPosition, targetPosition, speed * Time.deltaTime);
				if (Vector3.Distance(rectTransform.localPosition, targetPosition) <= 0.15f)
				{
					rectTransform.localPosition = targetPosition;
					currentPosition = rectTransform.localPosition;
					//isLerp = false;
				}
			}
		}
	}

	public void GetNearestPosition()
	{
		Debug.Log("Getting Nearest Position");

		for (int i = 0; i < pagePostions.Count; ++i)
		{
			if (pagePostions[i] != currentPosition)
			{
				if (Vector3.Distance(rectTransform.localPosition, pagePostions[i]) < (distance / 5.0f))
				{
					selectedPage = i;
					UpdatePosition();
					break;
				}
			}
		}

		Debug.Log("Exit Getting Nearest Position");
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		isDrag = true;
	}

	public void OnDrag(PointerEventData eventData)
	{
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		isDrag = false;
		GetNearestPosition();
	}

	public void UpdatePosition()
	{
		targetPosition = pagePostions[selectedPage];
		Debug.Log(targetPosition);
	}
}
