﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LootItem
{
	//public bool foldout;
	[HideInInspector] public bool foldout;
	public string index;
	public int value;
}

[CreateAssetMenu(menuName = "LootTable")]
[System.Serializable]
public class LootTable : ScriptableObject
{
	[HideInInspector] public float percentage = 0.0f;
	[HideInInspector] public bool foldout = false;
	[HideInInspector] public List<LootItem> items = new List<LootItem>();
	[HideInInspector] public int totalItem = 0;
}
