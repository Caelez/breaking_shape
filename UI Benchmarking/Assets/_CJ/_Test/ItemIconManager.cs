﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemIconManager : MonoBehaviour
{
	public ItemInfo itemInfo;
	public Image itemIcon;
	public Text itemAmount;
	public Button itemButton;

	public void UpdateUI()
	{
		itemAmount.text = itemInfo.itemAmount.ToString();
	}

	public void Interatable(bool status)
	{
		itemButton.enabled = status;
	}

	public void UseItem()
	{
		if (itemInfo.itemAmount > 0)
		{
			ItemManager.instance.GetItem(itemInfo.itemIndex).Use();
			--itemInfo.itemAmount;
			InventoryPanelManager.instance.UpdateData(itemInfo);
			UpdateUI();

			if (itemInfo.itemAmount <= 0)
			{
				InventoryPanelManager.instance.RemoveItem(gameObject);
			}
		}
	}
}
