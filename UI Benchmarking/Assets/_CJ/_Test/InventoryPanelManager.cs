﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryPanelManager : MonoBehaviour
{
	#region Singleton
	public static InventoryPanelManager instance;
	private void Awake()
	{
		if (!instance)
		{
			instance = this;
		}
		else
		{
			if (instance != this)
			{
				Destroy(gameObject);
			}
		}
	}
	#endregion

	public GameObject itemIcon;
	public ScrollRect scrollRect;
	public Transform content;

	public List<GameObject> itemLists = new List<GameObject>();
	//public List<ItemInfo> itemInfos = new List<ItemInfo>();

	// Use this for initialization
	void Start ()
	{
		if (!scrollRect)
		{
			scrollRect = GetComponentInChildren<ScrollRect>();
		}

		if (!content)
		{
			content = scrollRect.content;
		}

		//itemInfos = DataManager.userData.itemLists;
		CheckItem();
	}

	public void CheckItem()
	{
		ClearContentUI();
		//if (DataManager.userData.itemLists.Count > 0)
		//{
		//	//itemInfos = DataManager.userData.itemLists;
		//	for (int i = 0; i < DataManager.userData.itemLists.Count; ++i)
		//	{
		//		AddItem(DataManager.userData.itemLists[i]);
		//	}
		//}
	}

	public void AddItem(ItemInfo itemInfo)
	{
		Item tempItem = ItemManager.instance.GetItem(itemInfo.itemIndex);
		if (tempItem)
		{
			GameObject temp = Instantiate(itemIcon, content);
			temp.GetComponent<ItemIconManager>().itemInfo = itemInfo;
			temp.GetComponent<ItemIconManager>().itemIcon.sprite = tempItem.sprite;
			temp.GetComponent<ItemIconManager>().itemAmount.text = itemInfo.itemAmount.ToString();
			temp.GetComponent<Button>().enabled = true;
			itemLists.Add(temp);
		}
	}

	public void RemoveItem(GameObject icon)
	{
		if (itemLists.Contains(icon))
		{
			itemLists.Remove(icon);
			Destroy(icon);
		}
	}

	public void UpdateData(ItemInfo info)
	{
		//foreach (ItemInfo i in DataManager.userData.itemLists)
		//{
		//	if (i.itemIndex == info.itemIndex)
		//	{
		//		i.itemAmount = info.itemAmount;
		//		if (i.itemAmount <= 0)
		//		{
		//			DataManager.userData.itemLists.Remove(i);
		//		}
		//		break;
		//	}
		//}
		DataManager.UploadData();
	}

	public void ClearContentUI()
	{
		if (itemLists.Count > 0)
		{
			for (int i = 0; i < itemLists.Count; ++i)
			{
				Destroy(itemLists[i]);
			}
			itemLists.Clear();
		}
	}
}
