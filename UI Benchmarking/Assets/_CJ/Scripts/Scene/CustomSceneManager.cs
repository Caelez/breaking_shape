﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CustomSceneManager : MonoBehaviour
{
	#region Singleton

	public static CustomSceneManager instance;
	void Awake ()
	{
		if(!instance)
		{
			instance = this;
		}
		else
		{
			if(this != instance)
			{
				Destroy(this.gameObject);
			}
		}
		DontDestroyOnLoad(this.gameObject);
	}

	#endregion
	
	public static string screenName;
	public GameObject[] loadingScreens = new GameObject[3];
    public GameObject loadingBar;
	public Image fill;
	public Text progressPercentage;
	public Level wavesList;
	public string stageName;

    public void OpenMenu(GameObject panel)
	{
		panel.SetActive(true);
	}

	public void CloseMenu(GameObject panel)
	{
		panel.SetActive(false);
	}

	public static void LoadCustomScene(string sceneName)
	{
		SwitchToScene(sceneName);
	}

    // Load Main Menu scene
	public static void LoadMainMenuScene()
	{
        SwitchToScene("MainMenuScene");
	}

	// Load Play Scene
	public static void LoadPlayScene()
	{
		SwitchToScene("PlayScene");
	}

	// Load Shop Scene
	public static void LoadShopScene()
	{
		SwitchToScene("ShopScene");
	}

	public static void SwitchToScene(string sn)
	{
		Time.timeScale = 1.0f;
		screenName = sn;
		instance.StartCoroutine(LoadAsynchronously());
	}

    // Load loading screen when the scene to load is not ready
    public static IEnumerator LoadAsynchronously()
	{
		AudioManager.instance.ChangeScene();
		AsyncOperation m_operation = SceneManager.LoadSceneAsync(screenName);
        for (int i = 0; i < instance.loadingScreens.Length; i++)
        {
            instance.loadingScreens[i].SetActive(false);
        }

		int selectedScene = Random.Range(0, instance.loadingScreens.Length - 1);

		instance.loadingScreens[selectedScene].SetActive(true);
        instance.loadingBar.SetActive(true);

        while (!m_operation.isDone)
		{
			float progression = Mathf.Clamp01(m_operation.progress / 0.9f);
			instance.fill.fillAmount = progression;
			instance.progressPercentage.text = "Loading " + (progression * 100.0f).ToString("F0") + "%";

			yield return null;
		}

        instance.loadingScreens[selectedScene].SetActive(false);
        instance.loadingBar.SetActive(false);
        AudioManager.instance.SceneCompelete();
        yield return null;
	}

	public static void ExitGame()
	{
		Application.Quit();
	}
}
