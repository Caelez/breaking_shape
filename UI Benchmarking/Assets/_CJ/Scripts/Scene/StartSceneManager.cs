﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class StartSceneManager : MonoBehaviour
{
	#region Singleton
	public static StartSceneManager instance;
	private void Awake()
	{
		if (!instance)
		{
			instance = this;
		}
	}
	#endregion

	public Text DebugText;

	[Header("Panel Management")]
	public GameObject TapToStartGameObject;
	public GameObject loginButtonPanel;
	public GameObject loginPanel;
	public GameObject registerPanel;

	[Header("Audio Management")]
	public AudioClip sceneAudioClip;
	public bool mute;
	public GameObject muteButton;
	public Sprite muteSprite;
	public Sprite unmuteSprite;

	void Start ()
	{
		//CheckIsLoggedIn();

		// Audio Management
		if (sceneAudioClip)
		{
			if (AudioManager.instance.audioBGMSource.clip != sceneAudioClip)
			{
				AudioManager.instance.PlayBGM(sceneAudioClip);
			}
		}
	}

	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
            Debug.Log("Pressed Escape");
			CustomSceneManager.ExitGame();
		}
	}

	#region Panel Managemenet
	public void MainMenuScene()
	{
		CustomSceneManager.LoadMainMenuScene();
	}

	public void DisplayTapToStart()
	{
		TapToStartGameObject.gameObject.SetActive(true);
	}

	public void HideTapToStart()
	{
		TapToStartGameObject.gameObject.SetActive(false);
	}

	public void DisplayRegisterPanel()
	{
		registerPanel.SetActive(true);
	}

	public void HideRegisterPanel()
	{
		registerPanel.SetActive(false);
	}

	public void DisplayLoginPanel()
	{
		loginPanel.SetActive(true);
	}

	public void HideLoginPanel()
	{
		loginPanel.SetActive(false);
	}

	public void DisplayLoginButtonPanel()
	{
		loginButtonPanel.SetActive(true);
	}

	public void HideLoginButtonPanel()
	{
		loginButtonPanel.SetActive(false);
	}
	#endregion

	#region Audio Management
	public void Mute()
	{
		mute = !mute;
		AudioManager.instance.MuteAll(mute);
		if (mute)
		{
			muteButton.GetComponent<Image>().sprite = muteSprite;
		}
		else
		{
			muteButton.GetComponent<Image>().sprite = unmuteSprite;
		}
	}
	#endregion
}
