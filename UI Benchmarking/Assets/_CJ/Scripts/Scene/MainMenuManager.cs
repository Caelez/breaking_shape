﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
	#region Singleton

	public static MainMenuManager instance;

	private void Awake()
	{
		if (!instance)
		{
			instance = this;
		}
	}

	#endregion

	[Header("User Info Panel")]
	public Text coin;
	public Text crystal;

	[Header("Skill Info Panel")]
	public Image icon;
	public Text text;
	public Image level;

	[Header("Popup Window Management")]
	public GameObject popupPanel;

	[Header("Audio Management")]
	public AudioClip sceneClip;
	public GameObject muteButton;
	public Sprite muteSprite;
	public Sprite unmuteSprite;
	private bool mute;

	[HideInInspector]
	public SkillInfo selectedSkill;

	// Use this for initialization 
	void Start ()
	{
		// User Info Panel
		coin.text = DataManager.userData.coin.ToString();
		crystal.text = DataManager.userData.crystal.ToString();

		// Audio Management
		if (sceneClip)
		{
			if (AudioManager.instance.audioBGMSource.clip != sceneClip)
			{
				AudioManager.instance.PlayBGM(sceneClip);
			}
		}

		//slider.value = AudioManager.instance.audioBGMSource.volume;
		mute = AudioManager.instance.GetMuteAll();

		if (mute)
		{
			muteButton.GetComponent<Image>().sprite = muteSprite;
		}
		else
		{
			muteButton.GetComponent<Image>().sprite = unmuteSprite;
		}
	}

	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			CustomSceneManager.ExitGame();
		}
    }

	#region Skill Info Management

	public void SetSkillInfoPanel(SkillInfo info)
	{
		selectedSkill = info;
		UpdateSkillInfoPanel();
	}

	public void UpdateSkillInfoPanel()
	{
		icon.sprite = selectedSkill.icon;
		text.text = selectedSkill.description;
		level.fillAmount = selectedSkill.level / 10.0f;
		
	}

	#endregion

	// Manage the switch scene function for this scene
	#region Scene Management

	// Load Play Scene
	public void PlayScene()
	{
		CustomSceneManager.LoadPlayScene();
	}

	// Load Shop Scene
	public void ShopScene()
	{
		CustomSceneManager.LoadShopScene();
	}

	// Load Start Scene
	public void StartScene()
	{
		CustomSceneManager.LoadCustomScene("StartScene");
	}

	#endregion

	// Manage the popup window for this scene
	#region Popup Window Management

	public void IncreaseSkillLevel()
	{
		DataManager.instance.IncreaseSkillLevelUsingIndex(selectedSkill.index);
		selectedSkill = DataManager.instance.GetSkillInfo(selectedSkill.index);
		UpdateSkillInfoPanel();
		HidePopupPanel();
	}

	public void DisplayPopupPanel()
	{
		popupPanel.SetActive(true);
	}


	public void HidePopupPanel()
	{
		popupPanel.SetActive(false);
	}

	#endregion

	// Manage the audio function
	#region Audio Management

	public void Mute()
	{
		mute = !mute;
		AudioManager.instance.MuteAll(mute);
		if (mute)
		{
			muteButton.GetComponent<Image>().sprite = muteSprite;
		}
		else
		{
			muteButton.GetComponent<Image>().sprite = unmuteSprite;
		}
	}

	public void PlayButtonSFX()
	{
		AudioManager.instance.PlayButtonSFX();
	}
	
	#endregion
}
