﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopSceneManager : MonoBehaviour
{
	#region Singleton

	public static ShopSceneManager instance;

	private void Awake()
	{
		if (!instance)
		{
			instance = this;
		}
	}

	#endregion

	[Header("Popup Window Management")]
	public GameObject popupPanel;

	[Header("Audio Management")]
	public AudioClip sceneClip;
	public GameObject muteButton;
	public Sprite muteSprite;
	public Sprite unmuteSprite;
	private bool mute;

	// Use this for initialization
	void Start ()
	{
		// Audio Managemenet
		if (sceneClip)
		{
			if (AudioManager.instance.audioBGMSource.clip != sceneClip)
			{
				AudioManager.instance.PlayBGM(sceneClip);
			}
		}
	}

	// Manage the switch scene function for this scene
	#region Scene Management
	// Load Main Menu Scene
	public void MainMenuScene()
	{
		CustomSceneManager.LoadMainMenuScene();
	}
	#endregion

	// Manage the popup window for this scene
	#region Popup Window Management

	public void DisplayPopupPanel()
	{
		popupPanel.SetActive(true);
	}

	public void HidePopupPanel()
	{
		popupPanel.SetActive(false);
	}
	
	#endregion

	// Manage the audio function
	#region Audio Management

	public void Mute()
	{
		mute = !mute;
		AudioManager.instance.MuteAll(mute);
		if (mute)
		{
			muteButton.GetComponent<Image>().sprite = muteSprite;
		}
		else
		{
			muteButton.GetComponent<Image>().sprite = unmuteSprite;
		}
	}

	public void PlayButtonSFX()
	{
		AudioManager.instance.PlayButtonSFX();
	}

	#endregion
}
