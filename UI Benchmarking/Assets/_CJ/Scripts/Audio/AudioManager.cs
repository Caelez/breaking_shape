﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class AudioClipType
{
	public string name;
	public List<AudioClip> clips;
}

public class AudioManager : MonoBehaviour
{
	#region Singleton
	public static AudioManager instance = null;

	void Awake ()
	{
		if(instance == null)
		{
			instance = this;
		}
		else if(instance != this)
		{
			Destroy(gameObject);
		}
		
		DontDestroyOnLoad(this.gameObject);
	}
    #endregion
    public AudioClip enemyHitClip;
    [Header("SFX Management")]
	public List<AudioClipType> SFXLists;
    public List<AudioClip> enemyHitClips;
    public List<AudioClip> enemyDestroyClips;
    public AudioSource audioSFXSource;
	[HideInInspector] public bool SFXMute;

	// BallHit
	public int ballHitSFX;
	private int ballHitListIndex = -1;
	public float lowPitchRange = 0.95f;
	public float highPitchRange = 1.05f;

	// Button
	public int buttonSFX;
	private int buttonListIndex = -1;

	[Header("BGM Management")]
	public AudioSource audioBGMSource;
	[HideInInspector] public bool BGMMute;

   

	private void Start()
	{

	}

	// Update is called once per frame
	void Update ()
	{
       
    }

	public void MuteAll(bool state)
	{
		SetSFXMute(state);
		SetBGMMute(state);
	}

	public void ChangeScene()
	{
		audioSFXSource.mute = false;
		audioBGMSource.mute = false;
	}

	public void SceneCompelete()
	{
		audioSFXSource.mute = SFXMute;
		audioBGMSource.mute = BGMMute;
	}

	#region BallHit SFX
	// Look for the BallHit List in the SFXLists
	private bool LookForBallHitSFXList()
	{
		if (ballHitListIndex != -1)
		{
			return true;
		}

		for (int i = 0; i < SFXLists.Count; ++i)
		{
			if (SFXLists[i].name == "BallHit")
			{
				ballHitListIndex = i;
				return true;
			}
		}
		return false;
	}

	// Selected next ballHitSFX
	public void NextBallHitSFX()
	{
		if (LookForBallHitSFXList())
		{
			if (ballHitSFX < SFXLists[ballHitListIndex].clips.Count - 1)
			{
				++ballHitSFX;
			}
			UpdateBallHitSFXText();
		}
		else
		{
			Debug.LogWarning("BallHit SFX not found");
		}
	}

	// Selected previous ballHitSFX
	public void PrevBallHitSFX()
	{
		if (LookForBallHitSFXList())
		{
			if (ballHitSFX > 0)
			{
				--ballHitSFX;
			}
			UpdateBallHitSFXText();
		}
		else
		{
			Debug.LogWarning("BallHit SFX not found");
		}
	}

	// Play the ball hit sound
	public void PlayBallHitSFX()
	{
		if (LookForBallHitSFXList())
		{
			float randPitch = Random.Range(lowPitchRange, highPitchRange);
			audioSFXSource.pitch = randPitch;
			audioSFXSource.clip = SFXLists[ballHitListIndex].clips[ballHitSFX];
			audioSFXSource.Play();
		}
		else
		{
			Debug.LogWarning("BallHit SFX not found");
		}
	}

	// Update the text of the current SFX we currently using
	public void UpdateBallHitSFXText()
	{
		//GameplayManager.instance.ballHitSFXText.text = "Clip " + (ballHitSFX + 1).ToString();
	}
	#endregion

	#region Button SFX
	// Look for the BallHit List in the SFXLists
	private bool LookForButtonSFXList()
	{
		if (buttonListIndex != -1)
		{
			return true;
		}

		for (int i = 0; i < SFXLists.Count; ++i)
		{
			if (SFXLists[i].name == "Button")
			{
				buttonListIndex = i;
				return true;
			}
		}
		return false;
	}

	// Player normal button hit sound
	public void PlayButtonSFX()
	{
		if (LookForButtonSFXList())
		{
			audioSFXSource.clip = SFXLists[buttonListIndex].clips[buttonSFX];
			audioSFXSource.Play();
		}
		else
		{
			Debug.LogWarning("Button SFX not found");
		}
	}

	// Selected next buttonSFX
	public void NextButtonSFX()
	{
		if (LookForButtonSFXList())
		{
			if (buttonSFX < SFXLists[buttonListIndex].clips.Count - 1)
			{
				++buttonSFX;
			}
		}
		else
		{
			Debug.LogWarning("Button SFX not found");
		}
	}

	// Selected previous buttonSFX
	public void PrevButtonSFX()
	{
		if (LookForButtonSFXList())
		{
			if (buttonSFX > 0)
			{
				--buttonSFX;
			}
		}
		else
		{
			Debug.LogWarning("Button SFX not found");
		}
	}

	// Get the current clip name that playing
	public string GetCurrentButtonSFX()
	{
		if (LookForButtonSFXList())
		{
			return SFXLists[buttonListIndex].clips[buttonSFX].name;
		}
		return null;
	}
	#endregion

	public void PlaySFX(AudioClip clip)
    {
        audioSFXSource.PlayOneShot(clip);
    }

    public void PlayEnemyHitSFX()
    {
        audioSFXSource.PlayOneShot(enemyHitClips[Random.Range(0,enemyHitClips.Count)]);
    }

    public void PlayEnemyHitEffect()
    {
        audioSFXSource.PlayOneShot(enemyHitClip);
    }

    public void PlayEnemyDestroyEffect()
    {
        audioSFXSource.PlayOneShot(enemyDestroyClips [Random.Range(0, enemyDestroyClips.Count)]);
    }

    public void PlayBGM(AudioClip clip)
	{
		audioBGMSource.clip = clip;
		PlayBGM();
	}

	public void PlayBGM()
	{
		audioBGMSource.Play();
	}

	public void SetSFXMute(bool isMute)
	{
		SFXMute = isMute;
		audioSFXSource.mute = SFXMute;
	}

	public void SetBGMMute(bool isMute)
	{
		BGMMute = isMute;
		audioBGMSource.mute = BGMMute;
		
		if (isMute)
		{
			audioBGMSource.Stop();
		}
		else
		{
			audioBGMSource.Play();
		}
	}

	public bool GetMuteAll()
	{
		if (BGMMute && SFXMute)
		{
			return true;
		}
		return false;
	}
}
