﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeEffect : MonoBehaviour
{
	private Image image;
	private SpriteRenderer spriteRenderer;
	public float fadeSpeed = 0.1f;
	[Range(0.0f, 1.0f)]
	public float maxFadeValue = 1.0f;
	private bool isFading = true;

	// Use this for initialization
	void Start ()
	{
		isFading = true;
		if(GetComponent<Image>())
		{
			image = GetComponent<Image>();
		}
		else if(GetComponent<SpriteRenderer>())
		{
			spriteRenderer = GetComponent<SpriteRenderer>();
		}
	}

	void LateUpdate ()
	{
		float fadeDelta = fadeSpeed * Time.deltaTime;

		if(image)
		{
			float alpha = image.color.a;

			if(isFading)
			{
				alpha -= fadeDelta;
				
				if(alpha <= maxFadeValue)
				{
					isFading = false;
				}
			}
			else
			{
				alpha += fadeDelta;
				
				if(alpha >= 1.0f)
				{
					isFading = true;
				}
			}

			image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
		}

		if(spriteRenderer)
		{
			float alpha = spriteRenderer.color.a;

			if(isFading)
			{
				alpha -= fadeDelta;

				if(alpha <= maxFadeValue)
				{
					isFading = false;
				}
			}
			else
			{
				alpha += fadeDelta;

				if(alpha >= 1.0f)
				{
					isFading = true;
				}
			}

			spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, alpha);
		}
	}
}
