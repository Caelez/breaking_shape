﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour {

	#region Singleton

	public static ItemManager instance;
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
		}

		//LookForEionsDetails();
		DontDestroyOnLoad(gameObject);
	}

	#endregion

	[SerializeField] private List<Item> itemLists = new List<Item>();
	//[SerializeField] private List<string> itemIndexs = new List<string>();

	public Item GetItem(string index)
	{
		foreach (Item i in itemLists)
		{
			if (i.index == index)
			{
				return i;
			}
		}
		return null;
	}
}
