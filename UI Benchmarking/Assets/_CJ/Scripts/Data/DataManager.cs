﻿using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Data : NetworkManager.ServerResponse
{
	public string username = "";

	// Skill
	public List<SkillLevel> skillLevel;

	// Currency
	public int coin = 0;
	public int crystal = 0;
}

[Serializable]
public class Login
{
    public string email;
    public string id;
}

[Serializable]
public class SkillLevel
{
	public string index;
	public int level;
}

[Serializable]
public class SkillInfo
{
	public string name;
	public string index;
	public int level;
	public Sprite icon;
	public string description;
}

[Serializable]
public class ItemInfo
{
    public string itemIndex;
    public int itemAmount;
}

[Serializable]
public class JsonData : NetworkManager.ServerResponse
{
	public string data;
}

public class DataManager : MonoBehaviour
{
    #region Singleton

    public static DataManager instance;
    private void Awake()
    {
        if (!instance)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(gameObject);

        Init();
    }

	#endregion
	
	public static Data userData = new Data();
    public static string UniqueID = "";

    // Server Key - DO NOT TOUCH THIS!!!!
    private const string serverKey = "TA0014";

	// Server Link
	private const string domainURL = "https://www.trisavior.com/project_sms/";
    private const string uploadData = domainURL + "UploadData.php";
    private const string downloadData = domainURL + "DownloadData.php";

	[Header("Skill Database")]
	public List<SkillDatabase> skillDatabases = new List<SkillDatabase>();

	// Run on Awake
	private void Init()
	{
		if (!File.Exists(Application.persistentDataPath + "/userData.json"))
		{
			userData = new Data();
			SaveData();
		}
		else
		{
			LoadData();
		}
	}

	public SkillInfo GetSkillInfo(string index)
	{
		foreach (SkillDatabase sd in skillDatabases)
		{
			if (sd.index == index)
			{
				return GetSkillInfo(sd);
			}
		}
		return null;
	}

	public SkillInfo GetSkillInfo(SkillDatabase data)
	{
		SkillInfo skill = new SkillInfo();
		skill.name = data.name;
		skill.index = data.index;
		skill.level = GetSkillLevelUsingIndex(data.index);
		skill.icon = data.icon;
		skill.description = data.description;
		return skill;
	}

	public int GetSkillLevelUsingIndex(string index)
	{
		foreach (SkillLevel sl in userData.skillLevel)
		{
			if (sl.index == index)
			{
				return sl.level;
			}
		}
		return 1;
	}

	public void IncreaseSkillLevel(SkillDatabase data)
	{
		IncreaseSkillLevelUsingIndex(data.index);
	}

	public void IncreaseSkillLevelUsingIndex(string index)
	{
		foreach (SkillLevel sl in userData.skillLevel)
		{
			if (sl.index == index)
			{
				sl.level++;
				if (sl.level > 10)
				{
					sl.level = 10;
				}
				DataManager.SaveData();
				break;
			}
		}
	}

	// Write Json file
	public static void SaveData()
	{
		string jsonData = JsonUtility.ToJson(userData);
		File.WriteAllText(Application.persistentDataPath + "/userData.json", jsonData);
	}

	// Read Json file
	public static void LoadData()
	{
		string jsonData = File.ReadAllText(Application.persistentDataPath + "/userData.json");
		userData = JsonUtility.FromJson<Data>(jsonData);
	}

	// Upload Data
	public static void UploadData(Action<bool> onCompleteCallbackWithParameter = null)
	{
		WWWForm saveDataForm = new WWWForm();
		saveDataForm.AddField("serverKey", serverKey);
//		saveDataForm.AddField("uuid", FirebaseAuth.DefaultInstance.CurrentUser.UserId);
//		saveDataForm.AddField("username", FirebaseAuth.DefaultInstance.CurrentUser.DisplayName);
		saveDataForm.AddField("data", JsonUtility.ToJson(userData));

		NetworkManager.instance.SetParameterObject(onCompleteCallbackWithParameter);
		NetworkManager.instance.Run(uploadData, saveDataForm, null, OnCompleteUploadData);
	}

	private static void OnCompleteUploadData(string message, object parameterObject)
	{
		NetworkManager.ServerResponse res = JsonUtility.FromJson<Data>(message);
		Action<bool> _callback = (Action<bool>)parameterObject;
		if (_callback != null)
		{
			_callback(res.status == "success");
		}
		Debug.Log("OnCompleteUpload");
	}

	// Download Data
	public static void DownloadData(Action<bool> onCompleteCallbackWithParameter = null)
	{
		WWWForm saveDataForm = new WWWForm();
		saveDataForm.AddField("serverKey", serverKey);
//		saveDataForm.AddField("uuid", FirebaseAuth.DefaultInstance.CurrentUser.UserId);
		
		NetworkManager.instance.SetParameterObject(onCompleteCallbackWithParameter);
		NetworkManager.instance.Run(downloadData, saveDataForm, null, OnCompleteDownloadData);
	}

	private static void OnCompleteDownloadData(string message, object parameterObject)
	{
		JsonData jsonData = JsonUtility.FromJson<JsonData>(message);
		userData = JsonUtility.FromJson<Data>(jsonData.data);
		Action<bool> _callback = (Action<bool>)parameterObject;
		if (_callback != null)
		{
			_callback(jsonData.status == "success");
		}
		Debug.Log("OnCompleteDownload");
	}
}
