﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillUI : MonoBehaviour
{
	public string index;

	public void UpdateSkillInfo()
	{
		MainMenuManager.instance.SetSkillInfoPanel(DataManager.instance.GetSkillInfo(index));
	}
}
