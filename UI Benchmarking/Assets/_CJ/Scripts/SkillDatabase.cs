﻿using UnityEngine;

[CreateAssetMenu(menuName = "Skill Database")]
public class SkillDatabase : ScriptableObject
{
	public string index;
	new public string name;
	public Sprite icon;
	public string description;
}
