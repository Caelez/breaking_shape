﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


// LOCK DO NOT TOUCH IT EVEN IF YOU WANT TO!!!!


public class NetworkManager : MonoBehaviour
{
    public static NetworkManager instance;
    private float timeOutDuration = 30.0f;
    
    private WWWForm currentForm = null;
    private Action<string> onCompleteCallback = null;
    private Action<string, object> onCompleteCallbackWithParameter = null;
    private object parameterObject = null;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

	public void Run(string url, WWWForm wwwForm, Action<string> onCompleteCallback, Action<string, object> onCompleteCallbackWithParameter)
    {
        // Setting up
        this.currentForm = wwwForm;
        this.onCompleteCallback = onCompleteCallback;
        this.onCompleteCallbackWithParameter = onCompleteCallbackWithParameter;

		// Process
		WWW webData = new WWW(url, currentForm);
		StartCoroutine(RunWWW(webData));
	}

    private IEnumerator RunWWW(WWW webData)
    {
		StartSceneManager.instance.DebugText.text += "\nRunning...";
		float _processingTime = 0.0f;
        float _processingStartTime = Time.time;
        ResponseType _responseType = ResponseType.None;
        string _responseResult = "";
        string _extraInfo = "";

        do
        {
            yield return null;
            _processingTime = Time.time - _processingStartTime;
            
        } while (webData.isDone == false && _processingTime <= timeOutDuration);

        if(webData.isDone == true)
        {
            _responseResult = webData.text;
			if (StartSceneManager.instance && webData.error != "")
			{
				StartSceneManager.instance.DebugText.text += "\n" + webData.error;
			}

            if(_responseResult != "")
            {
                try
                {
                    ServerResponse serverResponse = JsonUtility.FromJson<ServerResponse>(_responseResult);
					if (serverResponse.status == "success")
					{
						_responseType = ResponseType.Success;
					}
					else if(serverResponse.status == "failed")
					{
						_responseType = ResponseType.Failed_Error;
					}
                }
                catch (Exception e)
                {
                    _extraInfo = "Exception Error: " + e.Message;
                }
            }
            else
            {
                _extraInfo = "Empty string from WWW text";
            }
        }

        string _debugLog = "\nResponse Type: " + _responseType.ToString();
        _debugLog += "\nResponse Result: " + _responseResult;
        _debugLog += "\nExtra Information: " + ((_extraInfo != "") ? _extraInfo : "None");

        if (_responseType == ResponseType.Success)
        {
			StartSceneManager.instance.DebugText.text += "\nChecking...";
			if (onCompleteCallback != null)
            {
				StartSceneManager.instance.DebugText.text += "\nCallBack";
				onCompleteCallback(_responseResult);
            }
            if(onCompleteCallbackWithParameter != null)
            {
				StartSceneManager.instance.DebugText.text += "\nCallBack";
				onCompleteCallbackWithParameter(_responseResult, parameterObject);
            }
			StartSceneManager.instance.DebugText.text += "\nEnd Checking";
		}

		Debug.Log("< RETURN > " + _debugLog);
		if (_responseType == ResponseType.Success)
		{
			StartSceneManager.instance.DebugText.text += "\nSuccess";
			StartSceneManager.instance.DebugText.text += "\n" + _responseResult;
		}
		else
		{
			if (StartSceneManager.instance)
			{
				StartSceneManager.instance.DebugText.text += _debugLog;
			}
		}
    }

    public void SetParameterObject(object parameterObject)
    {
        this.parameterObject = parameterObject;
    }

    [System.Serializable]
    public class ServerResponse
    {
        public string status = "";
        public string message = "";
    }
}
