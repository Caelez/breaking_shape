﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillListPanelManager : MonoBehaviour
{
	public GameObject skillUI;
	public RectTransform content;

	// Use this for initialization
	void Start()
	{
		if (DataManager.userData.skillLevel.Count <= 0)
		{
			foreach (SkillDatabase sd in DataManager.instance.skillDatabases)
			{
				SkillLevel sl = new SkillLevel();
				sl.index = sd.index;
				sl.level = 1;
				DataManager.userData.skillLevel.Add(sl);
			}
			DataManager.SaveData();
		}

		foreach (SkillLevel sl in DataManager.userData.skillLevel)
		{
			GameObject go = Instantiate(skillUI, content);
			go.GetComponent<SkillUI>().index = sl.index;
			go.GetComponent<Image>().sprite = DataManager.instance.GetSkillInfo(sl.index).icon;
		}

		MainMenuManager.instance.SetSkillInfoPanel(DataManager.instance.GetSkillInfo(DataManager.instance.skillDatabases[0].index));
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
}
