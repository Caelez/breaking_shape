﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTestingManager : MonoBehaviour
{
    public bool autoUpdate = true;
	public Level level;
	public int waveNumber = 1;

	private List<GameObject> obstacles = new List<GameObject>();

    [HideInInspector]
    public bool levelFoldouts;
    [HideInInspector]
    public bool waveFoldouts;

    private void OnValidate()
    {
        //GenerateWave();
    }

    public IEnumerator ShowObstacles()
    {
        yield return new WaitForEndOfFrame();

        ClearObstacles();

        if (!level)
        {
            Debug.LogWarning("Null Level");
            yield return null;
        }

        if (waveNumber > level.wavesList.Count || waveNumber <= 0)
        {
            Debug.LogError("Invalid Wave Number");
            yield return null;
        }

        if (!level.wavesList[waveNumber - 1])
        {
            Debug.LogError("Selected Wave is empty");
            yield return null;
        }

        if (waveNumber - 1 >= 0)
        {
            foreach (WaveInfo waveInfo in level.wavesList[waveNumber - 1].wave)
            {
                if(waveInfo.prefab)
                {
                    GameObject go = Instantiate(waveInfo.prefab, waveInfo.position, Quaternion.Euler(0.0f, 0.0f, waveInfo.rotation));
                    if(waveInfo.scale != 0.0f)
                    {
                        go.transform.localScale = new Vector3(waveInfo.scale, waveInfo.scale, 1.0f);
                    }

                    if (go.CompareTag("Obstacle"))
                    {
                        go.GetComponent<ObstacleBehaviour>().SetHealth(waveInfo.health);

                        if (waveInfo.sprite)
                        {
                            go.GetComponent<SpriteManager>().UseSprite(waveInfo.sprite);
                        }
                    }

                    obstacles.Add(go);
                }
                else
                {
                    Debug.LogWarning("Warning. An empty object in the list.");
                }
            }
        }
        
        yield return null;
    }

    public void GenerateWave()
    {
        StartCoroutine(ShowObstacles());
    }

    public void OnWaveUpdated()
    {
        if(autoUpdate)
        {
            StartCoroutine(ShowObstacles());
        }
    }

    public void ClearObstacles()
    {
        if (obstacles.Count > 0)
        {
            foreach (GameObject go in obstacles)
            {
                DestroyImmediate(go);
            }
        }

        obstacles.Clear();
    }
}
