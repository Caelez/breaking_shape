﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Test
{
	public virtual void Display()
	{
		Debug.Log("Original");
		return;
	}
}

public class Test02 : Test
{
	public override void Display()
	{
		Debug.Log("Override");
		return;
	}
}

public class StartupManager : MonoBehaviour
{
	
//	private IEnumerator Start ()
//	{
//		while(!LocalizationManager.instance.GetIsReady())
//		{
//			yield return null;
//		}

//		SceneManager.LoadScene("TestScene02");
//	}

	public List<GameObject> game = new List<GameObject>();

	
	public void CallLoadLocalizedText(string fileName)
	{
		LocalizationManager.instance.LoadLocalizedText(fileName);
	}
}

