﻿/// The location of this folder varies per platform
/// IMPORTANCE NOTE* CASE-SENSITIVE
/// Windows/Mac OS	-	path = Application.dataPath + "/StreamingAssets";
/// iOS				-	path = Application.dataPath + "/Raw";
/// Android			-	path = "jar:file://" + Application.dataPath + "!/assets/";
/// More info --> https://docs.unity3d.com/Manual/StreamingAssets.html

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class LocalizationManager : MonoBehaviour
{
	#region Singleton

	public static LocalizationManager instance;

	private void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else if(instance != this)
		{
			Destroy(this.gameObject);
		}

		DontDestroyOnLoad(gameObject);
	}

	#endregion

	private Dictionary<string, string> localizedText;
	private bool isReady = false;
	private string missingTextString = "Localized text not found";
	
	public void LoadLocalizedText(string fileName)
	{
		localizedText = new Dictionary<string, string>();
		string filePath = Path.Combine(Application.streamingAssetsPath, fileName);
		if(File.Exists(filePath))
		{
			string dataAsJson = File.ReadAllText(filePath);
			LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

			for (int i = 0; i < loadedData.items.Length; i++)
			{
				localizedText.Add(loadedData.items[i].key, loadedData.items[i].value);
			}

			Debug.Log("Data loaded, dictionary contains: " + localizedText.Count + " entries");
		}
		else
		{
			Debug.LogError("Cannot find file");
		}

		//isReady = true;
		SceneManager.LoadScene("TestScene02");
	}

	public string GetLocalizedValue(string key)
	{
		string result = missingTextString;
		if(localizedText.ContainsKey(key))
		{
			result = localizedText[key];
		}

		return result;
	}

	public bool GetIsReady()
	{
//		isReady = !isReady;
		return !isReady;
	}
}
