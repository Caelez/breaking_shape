﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TrapBehaviour))]
public class TrapBehaviourEditor : Editor
{
	public override void OnInspectorGUI()
	{
		TrapBehaviour myTarget = (TrapBehaviour)target;
		myTarget.SetSpeed(EditorGUILayout.FloatField("Speed", myTarget.GetSpeed()));
		myTarget.trapType = (TrapType)EditorGUILayout.EnumPopup("Trap Type", myTarget.trapType);
        

		if(GUI.changed)
		{
			EditorUtility.SetDirty(myTarget);
		}
	}
}
