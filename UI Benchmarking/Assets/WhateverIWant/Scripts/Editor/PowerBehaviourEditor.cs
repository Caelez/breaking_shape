﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PowerBehaviour))]
public class PowerBehaviourEditor : Editor
{
	public override void OnInspectorGUI()
	{
		PowerBehaviour myTarget = (PowerBehaviour)target;
		myTarget.SetSpeed(EditorGUILayout.FloatField("Speed", myTarget.GetSpeed()));
		myTarget.powerType = (PowerType)EditorGUILayout.EnumPopup("Power Type", myTarget.powerType);


		if(myTarget.powerType == PowerType.ExtraBall1)
		{
			myTarget.ball = (GameObject)EditorGUILayout.ObjectField("Ball", myTarget.ball, typeof(GameObject), true);
		}
        else if (myTarget.powerType == PowerType.ExtraBall2)
        {
            myTarget.ball = (GameObject)EditorGUILayout.ObjectField("Ball", myTarget.ball, typeof(GameObject), true);
        }
        else if (myTarget.powerType == PowerType.ExtraBall5)
        {
            myTarget.ball = (GameObject)EditorGUILayout.ObjectField("Ball", myTarget.ball, typeof(GameObject), true);
        }
        else if(myTarget.powerType == PowerType.EnlargeBall)
		{
			myTarget.value = EditorGUILayout.FloatField("Enlarge Value", myTarget.value);
		}
		else if(myTarget.powerType == PowerType.ShrinkBall)
		{
			myTarget.value = EditorGUILayout.FloatField("Shrink Value", myTarget.value);
		}

		if(GUI.changed)
		{
			EditorUtility.SetDirty(myTarget);
		}
	}
}
