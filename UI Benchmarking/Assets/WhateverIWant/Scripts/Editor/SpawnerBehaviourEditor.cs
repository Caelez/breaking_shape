﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(SpawnerBehaviour))]
public class SpawnerBehaviourEditor : Editor
{
	private ReorderableList list;

	private void OnEnable()
	{
		list = new ReorderableList(serializedObject, serializedObject.FindProperty("trapType"), true, true, true, true);

		list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
		{
			var element = list.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;
			EditorGUI.PropertyField(
				new Rect(rect.x, rect.y, 120, EditorGUIUtility.singleLineHeight), 
				element.FindPropertyRelative("m_object"), GUIContent.none);
			EditorGUI.PropertyField(
				new Rect(rect.x + 120, rect.y, rect.width - 120 - 60, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("m_spawnRate"), GUIContent.none);
		};
	}

//	public override void OnInspectorGUI()
//	{
//		SpawnerBehaviour myTarget = (SpawnerBehaviour)target;
//
//		myTarget.spacing = EditorGUILayout.FloatField("Spacing", myTarget.spacing);
//		myTarget.healthIncrement = EditorGUILayout.IntField("Health Increment", myTarget.healthIncrement);
//		serializedObject.Update();
//		list.DoLayoutList();
//		serializedObject.ApplyModifiedProperties();
//
//
//		if(GUI.changed)
//		{
//			EditorUtility.SetDirty(myTarget);
//		}
//	}
}
