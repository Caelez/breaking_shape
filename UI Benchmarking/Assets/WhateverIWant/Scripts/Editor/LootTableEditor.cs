﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(LootTable))]
public class LootTableEditor : Editor
{
	ReorderableList list;
	int total = 0;
	LootTable myTarget;

	private void OnEnable()
	{
		myTarget = (LootTable)target;
		list = CreateList(serializedObject, serializedObject.FindProperty("items"), "Items", 3);
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		serializedObject.Update();
		list.DoLayoutList();

		myTarget.totalItem = 0;
		foreach (LootItem li in myTarget.items)
		{
			myTarget.totalItem += li.value;
		}

		if (total != myTarget.totalItem)
		{
			total = myTarget.totalItem;
		}

		EditorGUILayout.LabelField("Total: " + total);

		serializedObject.ApplyModifiedProperties();
	}

	ReorderableList CreateList(SerializedObject obj, SerializedProperty prop, string label, int expandedLines)
	{
		ReorderableList list = new ReorderableList(obj, prop, true, true, true, true);

		list.drawHeaderCallback = (Rect rect) =>
		{
			EditorGUI.LabelField(rect, label);
		};

		list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
		{
			SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);

			// Add Padding
			rect.y += 2;

			float rectPosX = rect.x;
			float rectPosY = rect.y;
			float rectWidth = rect.width / 3.0f;
			float rectHeight = EditorGUIUtility.singleLineHeight;
			float singleLineSpace = EditorGUIUtility.singleLineHeight * 1.25f;
			float contant = 0.0f;

			// Create Foldout for the element
			element.FindPropertyRelative("foldout").boolValue = EditorGUI.Foldout(new Rect(rectPosX + 10, rectPosY, rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("foldout").boolValue, name);

			float totalItemPercentage = ((float)element.FindPropertyRelative("value").intValue / (float)total);
			float totalPercentage = myTarget.percentage * totalItemPercentage;

			if (total == 0)
			{
				EditorGUI.LabelField(
				new Rect(rectPosX + 10, rectPosY + singleLineSpace * contant, rectWidth - 10.0f, rectHeight),
				element.FindPropertyRelative("index").stringValue + " - 0.000%");
			}
			else
			{
				EditorGUI.LabelField(
				new Rect(rectPosX + 10, rectPosY + singleLineSpace * contant, rectWidth * 3.0f - 10.0f, rectHeight),
				element.FindPropertyRelative("index").stringValue + " - " + (totalItemPercentage * 100.0f).ToString("F3") + "% - " + totalPercentage.ToString("F3") + "%");
			}
			++contant;

			if (element.FindPropertyRelative("foldout").boolValue)
			{
				EditorGUI.LabelField(new Rect(rectPosX, rectPosY + singleLineSpace * contant, rectWidth - 10.0f, rectHeight), "Index");
				EditorGUI.PropertyField(
					new Rect(
						rectPosX + rectWidth,
						rectPosY + singleLineSpace * contant,
						rectWidth * 2.0f - 10.0f,
						rectHeight),
					element.FindPropertyRelative("index"),
					GUIContent.none);
				++contant;

				EditorGUI.LabelField(new Rect(rectPosX, rectPosY + singleLineSpace * contant, rectWidth - 10.0f, rectHeight), "Value");
				EditorGUI.PropertyField(
					new Rect(
						rectPosX + rectWidth,
						rectPosY + singleLineSpace * contant,
						rectWidth * 2.0f - 10.0f,
						rectHeight),
					element.FindPropertyRelative("value"),
					GUIContent.none);
				++contant;
			}
		};

		list.elementHeightCallback = (int index) =>
		{
			var element = list.serializedProperty.GetArrayElementAtIndex(index);
			float height = EditorGUIUtility.singleLineHeight * 1.25f;

			if (element.FindPropertyRelative("foldout").boolValue)
			{
				height = EditorGUIUtility.singleLineHeight * 1.25f * expandedLines + 2.0f;
			}

			return height;
		};

		return list;
	}
}
