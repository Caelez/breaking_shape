﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(Level))]
public class LevelEditor : Editor
{
    private ReorderableList list;

    private void OnEnable()
    {
        // Create ReorderableList;
        list = CreateListWaves(serializedObject, serializedObject.FindProperty("wavesList"), "Wave List", 1);
    }

    public override void OnInspectorGUI()
    {
		Level myTarget = (Level)target;
		myTarget.name = EditorGUILayout.TextField("Name", myTarget.name);
		myTarget.levelID = EditorGUILayout.TextField("Level ID", myTarget.levelID);
        myTarget.levelNumber = EditorGUILayout.IntField("Level Number", myTarget.levelNumber);
        myTarget.expRates = EditorGUILayout.FloatField("Exp Multiplier", myTarget.expRates);

        EditorGUILayout.LabelField("Required For Stars");
        myTarget.requirement1.requirement = (RequirementsTypes)EditorGUILayout.EnumPopup("Requirement 1 : ", myTarget.requirement1.requirement);
        myTarget.requirement1.number = EditorGUILayout.IntField("Requirement 2 : ", myTarget.requirement1.number);
        myTarget.requirement2.requirement = (RequirementsTypes)EditorGUILayout.EnumPopup("Requirement 1 : ", myTarget.requirement2.requirement);
        myTarget.requirement2.number = EditorGUILayout.IntField("Requirement 2 : ", myTarget.requirement2.number);
        myTarget.requirement3.requirement = (RequirementsTypes)EditorGUILayout.EnumPopup("Requirement 3 : ", RequirementsTypes.Clear);



        if (GUI.changed)
		{
			EditorUtility.SetDirty(myTarget);
		}

		serializedObject.Update(); // Needed for free good editor functionality
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties(); // Needed for zero-hassle good editor functionality
    }

    ReorderableList CreateListWaves(SerializedObject obj, SerializedProperty prop, string label, int expandedLines)
    {
        ReorderableList list = new ReorderableList(obj, prop, true, true, true, true);

        list.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, label);
        };

        // Main draw callback for the ReorderableList
        list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);

            // Add padding
            rect.y += 2;

            // Prefab
            EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Waves " + (index + 1).ToString("00"));
            EditorGUI.PropertyField(
                new Rect(
                    rect.x + (rect.width / 3),
                    rect.y,
                    rect.width / 3 * 2 - 10,
                    EditorGUIUtility.singleLineHeight),
                element,
                GUIContent.none);
        };

        // Adjust the heights based on whether the element foldout is true
        list.elementHeightCallback = (int index) =>
        {
            return EditorGUIUtility.singleLineHeight * 1.25f * expandedLines + 2;
        };

        return list;
    }
}
