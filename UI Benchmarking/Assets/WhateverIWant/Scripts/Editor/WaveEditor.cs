﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(Wave))]
public class WaveEditor : Editor
{
    private ReorderableList list;
    
    private void OnEnable()
    {
        // Create ReorderableList;
        list = CreateList(serializedObject, serializedObject.FindProperty("wave"), "Wave Info", 1);
       
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update(); // Needed for free good editor functionality
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties(); // Needed for zero-hassle good editor functionality
    }

    // Create a ReorderableList with Foldout function
    ReorderableList CreateList(SerializedObject obj, SerializedProperty prop, string label, int expandedLines)
    {
        ReorderableList list = new ReorderableList(obj, prop, true, true, true, true);

        list.drawHeaderCallback = (Rect rect) =>
        {
            EditorGUI.LabelField(rect, label);
        };

        // Main draw callback for the ReorderableList
        list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);

            // Manage the Foldout for the element in the list
            string name = "(empty)"; // To prevent null reference
            List<SpriteType> sprites = new List<SpriteType>();
            List<PowerSpriteType> powerSprites = new List<PowerSpriteType>();
            List<TrapSpriteType> trapSprites = new List<TrapSpriteType>();
            List<string> nameList = new List<string>();
            if (element.FindPropertyRelative("prefab").objectReferenceValue)
            {
                GameObject go = (GameObject)element.FindPropertyRelative("prefab").objectReferenceValue;
                name = go.name;
                if(go.CompareTag("Obstacle") && go.GetComponent<SpriteManager>())
                {
                    sprites = go.GetComponent<SpriteManager>().sprites;
                    for (int i = 0; i < sprites.Count; ++i)
                    {
                        nameList.Add(sprites[i].name);
                    }
                }
                if (go.CompareTag("Power") && go.GetComponent<PowerSpriteManager>())
                {
                    powerSprites = go.GetComponent<PowerSpriteManager>().sprites;
                    for (int i = 0; i < powerSprites.Count; ++i)
                    {
                        nameList.Add(((PowerType)i).ToString());
                    }
                }
                if (go.CompareTag("Trap") && go.GetComponent<TrapSpriteManager>())
                {
                    trapSprites = go.GetComponent<TrapSpriteManager>().sprites;
                    for (int i = 0; i < trapSprites.Count; ++i)
                    {
                        nameList.Add(((TrapType)i).ToString());
                    }
                }
            }

            // Create Foldout for the element
            element.FindPropertyRelative("foldout").boolValue = EditorGUI.Foldout(new Rect(rect.x + 10, rect.y, rect.width, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("foldout").boolValue, name);

            // Add padding
            rect.y += 2;

            // If foldout is true, it will show the property of the element
            if (element.FindPropertyRelative("foldout").boolValue)
            {
                int pos = 1;
                // Prefab
                EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Prefab");
                
                EditorGUI.PropertyField(
                    new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("prefab"),
                    GUIContent.none);
                ++pos;

                // Position
                EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Position");
                EditorGUI.PropertyField(
                    new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("position"),
                    GUIContent.none);
                ++pos;

                // Rotation
                EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Rotation");
                EditorGUI.PropertyField(
                    new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("rotation"),
                    GUIContent.none);
                ++pos;

                // Scale
                EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Scale");
                EditorGUI.PropertyField(
                    new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                    element.FindPropertyRelative("scale"),
                    GUIContent.none);
                ++pos;
                

                if (((GameObject)element.FindPropertyRelative("prefab").objectReferenceValue) != null)
                {
                    if (((GameObject)element.FindPropertyRelative("prefab").objectReferenceValue).CompareTag("Obstacle"))
                    {
                        // Health
                        EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Health");
                        EditorGUI.PropertyField(
                            new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                            element.FindPropertyRelative("health"),
                            GUIContent.none);
                        ++pos;

                        if (sprites.Count != 0)
                        {
                            // Sprite Type
                            EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Sprite Type");
                            element.FindPropertyRelative("selectedSpriteType").intValue = EditorGUI.Popup(
                                new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                                element.FindPropertyRelative("selectedSpriteType").intValue,
                                nameList.ToArray<string>());
                            ++pos;

                            element.FindPropertyRelative("sprite").objectReferenceValue = sprites[element.FindPropertyRelative("selectedSpriteType").intValue].sprite;
                            element.FindPropertyRelative("elementType").enumValueIndex = (int)sprites[element.FindPropertyRelative("selectedSpriteType").intValue].type;

                            // Element Type
                            EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Element");
                            EditorGUI.LabelField(new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                            ((ElementType)element.FindPropertyRelative("elementType").intValue).ToString(), EditorStyles.boldLabel);
                            ++pos;

                            // Sprite
                            EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Sprite");
                            EditorGUI.LabelField(new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                            ((Sprite)element.FindPropertyRelative("sprite").objectReferenceValue).name, EditorStyles.boldLabel);
                            ++pos;

                            EditorGUI.DrawTextureTransparent(new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, EditorGUIUtility.singleLineHeight * 1.25f * 3.0f, EditorGUIUtility.singleLineHeight * 1.25f * 3.0f),
                                ((Sprite)element.FindPropertyRelative("sprite").objectReferenceValue).texture);
                            ++pos;
                        }
                    }
                    else if (((GameObject)element.FindPropertyRelative("prefab").objectReferenceValue).CompareTag("Power"))
                    {
                        EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Sprite Type");
                        element.FindPropertyRelative("selectedSpriteType").intValue = EditorGUI.Popup(
                            new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                            element.FindPropertyRelative("selectedSpriteType").intValue,
                            nameList.ToArray<string>());
                        ++pos;
                       
                        element.FindPropertyRelative("sprite").objectReferenceValue = powerSprites[element.FindPropertyRelative("selectedSpriteType").intValue].sprite;
                        element.FindPropertyRelative("powerType").enumValueIndex = (int)powerSprites[element.FindPropertyRelative("selectedSpriteType").intValue].type;

                        // Element Type
                        EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Element");
                        EditorGUI.LabelField(new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                        ((PowerType)element.FindPropertyRelative("powerType").intValue).ToString(), EditorStyles.boldLabel);
                        ++pos;

                        // Sprite
                        EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Sprite");
                        EditorGUI.LabelField(new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                        ((Sprite)element.FindPropertyRelative("sprite").objectReferenceValue).name, EditorStyles.boldLabel);
                        ++pos;

                        EditorGUI.DrawTextureTransparent(new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, EditorGUIUtility.singleLineHeight * 1.25f * 3.0f, EditorGUIUtility.singleLineHeight * 1.25f * 3.0f),
                            ((Sprite)element.FindPropertyRelative("sprite").objectReferenceValue).texture);
                        ++pos;
                    }
                    else if (((GameObject)element.FindPropertyRelative("prefab").objectReferenceValue).CompareTag("Trap"))
                    {
                        EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Sprite Type");
                        element.FindPropertyRelative("selectedSpriteType").intValue = EditorGUI.Popup(
                            new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                            element.FindPropertyRelative("selectedSpriteType").intValue,
                            nameList.ToArray<string>());
                        ++pos;

                        element.FindPropertyRelative("sprite").objectReferenceValue = trapSprites[element.FindPropertyRelative("selectedSpriteType").intValue].sprite;
                        element.FindPropertyRelative("trapType").enumValueIndex = (int)trapSprites[element.FindPropertyRelative("selectedSpriteType").intValue].type;

                        // Element Type
                        EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Element");
                        EditorGUI.LabelField(new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                        ((TrapType)element.FindPropertyRelative("trapType").intValue).ToString(), EditorStyles.boldLabel);
                        ++pos;

                        // Sprite
                        EditorGUI.LabelField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 - 10, EditorGUIUtility.singleLineHeight), "Sprite");
                        EditorGUI.LabelField(new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, rect.width / 3 * 2 - 10, EditorGUIUtility.singleLineHeight),
                        ((Sprite)element.FindPropertyRelative("sprite").objectReferenceValue).name, EditorStyles.boldLabel);
                        ++pos;

                        EditorGUI.DrawTextureTransparent(new Rect(rect.x + (rect.width / 3), rect.y + EditorGUIUtility.singleLineHeight * 1.25f * pos, EditorGUIUtility.singleLineHeight * 1.25f * 3.0f, EditorGUIUtility.singleLineHeight * 1.25f * 3.0f),
                            ((Sprite)element.FindPropertyRelative("sprite").objectReferenceValue).texture);
                        ++pos;
                    }
                }
            }
        };

        // Adjust the heights based on whether the element foldout is true
        list.elementHeightCallback = (int index) =>
        {
            var element = list.serializedProperty.GetArrayElementAtIndex(index);

            float height = EditorGUIUtility.singleLineHeight * 1.25f;
            GameObject go = (GameObject)element.FindPropertyRelative("prefab").objectReferenceValue;

            if (element.FindPropertyRelative("foldout").boolValue)
            {
               
                    height = EditorGUIUtility.singleLineHeight * 1.25f * (expandedLines + 10 + 2);
                
            }

            return height;
        };

        return list;
    }
}
