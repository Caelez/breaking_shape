﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelTestingManager))]
public class LevelTestingEditor : Editor
{
    LevelTestingManager levelTesting;
    Editor levelEditor;
    Editor waveEditor;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        using (var check = new EditorGUI.ChangeCheckScope())
        {
            if (check.changed)
            {
                levelTesting.GenerateWave();
            }
        }

        if (GUILayout.Button("Clear"))
        {
            levelTesting.ClearObstacles();
        }

        if (GUILayout.Button("Generate Wave"))
        {
            levelTesting.GenerateWave();
        }

        DrawWaveEditor(levelTesting.level, levelTesting.OnWaveUpdated, ref levelTesting.levelFoldouts, ref levelEditor);
        DrawWaveEditor(levelTesting.level.wavesList[levelTesting.waveNumber - 1], levelTesting.OnWaveUpdated, ref levelTesting.waveFoldouts, ref waveEditor);
    }

    void DrawWaveEditor(Object wave, System.Action onWaveUpdated, ref bool foldout, ref Editor editor)
    {
        if(wave != null)
        {
            foldout = EditorGUILayout.InspectorTitlebar(foldout, wave);
            using (var check = new EditorGUI.ChangeCheckScope())
            {
                if(foldout)
                {
                    CreateCachedEditor(wave, null, ref editor);
                    editor.OnInspectorGUI();

                    if(check.changed)
                    {
                        if(onWaveUpdated != null)
                        {
                            onWaveUpdated();
                        }
                    }
                }
            }
        }
    }

    private void OnEnable()
    {
        levelTesting = (LevelTestingManager)target;
    }
}
