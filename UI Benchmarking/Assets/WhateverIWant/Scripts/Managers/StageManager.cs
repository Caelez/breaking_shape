﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageManager : MonoBehaviour
{
	#region Instance

	public static StageManager instance;

	void Awake()
	{
		instance = this;
	}

	#endregion

    // Background Image
	public GameObject background;
	public float backgroundTopPos = 0.0f;
	public float backgroundBottomPos = 0.0f;

	[SerializeField]
    private float moveSpeed = 1.0f;
	private float moveDistance = 0.0f;
	private bool isMovable = false;
	private Vector3 destination = Vector3.zero;

/*    // Stage Announcer's text
    public GameObject stageAnnouncer;
    private Text stageAnnouncerText;
    [SerializeField]
    private float displayDuration = 2.0f;
    private float displayTimer = 0.0f;
*/
	public int currentWave = 0;

	public Level level;

	public List<GameObject> obstacles = new List<GameObject>();
    private List<int> unwantedRefreshIDObjects = new List<int>();

	public void InitializeBackground()
	{
		float totalDistance = Mathf.Abs(backgroundTopPos) + Mathf.Abs(backgroundBottomPos);
		moveDistance = totalDistance / (float)level.wavesList.Count;
		background.transform.position = new Vector3(0.0f, backgroundTopPos, 0.0f);
	}

	private void Update()
	{
		if(isMovable && background.transform.position.y != destination.y)
		{
			background.transform.position = Vector3.Lerp(background.transform.position, destination, Time.deltaTime * moveSpeed);
			if(Mathf.Abs(background.transform.position.y - destination.y) < 0.05f)
			{
				background.transform.position = destination;
				SpawnWave(currentWave - 1);
				isMovable = false;
			}
		}
    }

    public void ResetStage(int iD)
    {
        for (int i = obstacles.Count-1; i >= 0; i--)
        {
            Destroy(obstacles[i]);
        }
        obstacles = new List<GameObject>();
        unwantedRefreshIDObjects.Add(iD);
        SpawnWave(currentWave - 1);
        isMovable = false;
    }

	public void SetNewBackgroundDestination()
	{
		destination = background.transform.position + new Vector3(0.0f, moveDistance, 0.0f);
		isMovable = true;
	}

	public void SpawnWave(int index)
	{
		if(index > level.wavesList.Count - 1)
		{
			return;
		}
		GameplayManager.instance.UpdateWaveText(currentWave, currentWave == level.wavesList.Count);
        int iD = 0;
        foreach (WaveInfo waveInfo in level.wavesList[index].wave)
		{
            if (waveInfo.prefab)
            {
                iD++;
                if (!unwantedRefreshIDObjects.Contains(iD))
                {
                    
                    GameObject go = Instantiate(waveInfo.prefab, waveInfo.position, Quaternion.Euler(0.0f, 0.0f, waveInfo.rotation));
                    go.name = waveInfo.prefab.name;
                    go.GetComponent<MapObjectsBase>().SetID(iD);
                   

                    if (go.CompareTag("Obstacle"))
                    {
                        go.GetComponent<ObstacleBehaviour>().ResetGameObject();
                        go.GetComponent<ObstacleBehaviour>().SetHealth(waveInfo.health);
//                        go.GetComponent<ObstacleBehaviour>().SetElement(waveInfo.elementType);
                        go.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(0));
                        if (waveInfo.sprite)
                        {
                            go.GetComponent<SpriteManager>().UseSprite(waveInfo.sprite);
                        }
                    }
                    if (go.CompareTag("Power"))
                    {
                        go.GetComponent<PowerBehaviour>().SetPowerBehavior(waveInfo.powerType);
                        go.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(2));
                    }

                    if (go.CompareTag("Trap"))
                    {
                        go.GetComponent<TrapBehaviour>().SetTrapType(waveInfo.trapType);
                        go.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(1));
                    }
                    obstacles.Add(go);                    
                }               
            }
            else
            {
                Debug.LogWarning("Warning. An empty object in the list.");
            }
		}
        obstacles.Sort((f1, f2) => f1.transform.position.y.CompareTo(f2.transform.position.y));
        obstacles.Reverse();
    }

    public void MoveObstacleUp()
	{
		if (obstacles.Count > 0)
		{
			for(int i = 0; i < obstacles.Count; i++)
			{
				if(obstacles[i].CompareTag("Obstacle"))
				{
					obstacles[i].GetComponent<ObstacleBehaviour>().CheckAfterEffect();
					if(!obstacles[i].GetComponent<ObstacleBehaviour>())
					{
						if(obstacles.Count == 0) break;
						i--;
					}
				}
				else if(obstacles[i].CompareTag("Power"))
				{
					obstacles[i].GetComponent<PowerBehaviour>().CheckAfterEffect();
				}
				else if(obstacles[i].CompareTag("Trap"))
				{
					obstacles[i].GetComponent<TrapBehaviour>().CheckAfterEffect();
				}
			}

			// Move up all obstacles
			foreach(GameObject go in obstacles)
			{
                if (go.GetComponent<MapObjectsBase>() != null)
                {
                    go.GetComponent<MapObjectsBase>().MoveUp();
                }
			}
		}
		LauncherBehaviour.instance.currentActivateTraps = LauncherBehaviour.instance.baseActivateTraps;
	}

	public void RemoveFromList(GameObject g)
	{
		if(obstacles.Contains(g))
		{			
			obstacles.Remove(g);
			Destroy(g);
        }
    }

	public void ClearObstaclesList()
	{		
		while(obstacles.Count > 0)
		{ 
			if (obstacles[0].GetComponent<BallBehaviour>() != null)
			{
				obstacles[0].SetActive(false);
				obstacles.RemoveAt(0);
			}
			else
			{
				Destroy(obstacles[0].gameObject);
				obstacles.RemoveAt(0);
			}			
		}
	}

	public bool WaveCleared()
	{       
        if (obstacles.Count > 0)
		{

            foreach (GameObject go in obstacles)
            {
                if (go.CompareTag("Obstacle"))
                {
                    return false;
                }
            }
		}
        return true;
	}

	public bool NextWave()
	{
        if (RequirementCaculationManager.instance != null)
        {
            RequirementCaculationManager.instance.SetClearWaveBy(GameplayManager.instance.GetTurnCount() - 1);
            RequirementCaculationManager.instance.ChangeRewardManager();
            RequirementCaculationManager.instance.ResetWave();
        }
        if (currentWave < level.wavesList.Count)
        {
			SetNewBackgroundDestination();
			currentWave ++;
			ClearObstaclesList();
            unwantedRefreshIDObjects = new List<int>();
			return true;
		}                
        return false;
	}

/*    public void DisplayStageAnnouncer()
    {
        if (stageAnnouncer)
        {
            if (!stageAnnouncerText)
            {
                stageAnnouncerText = stageAnnouncer.GetComponent<Text>();
            }

           // stageAnnouncerText.text = GameplayManager.instance.waveText.text;
            displayTimer = 0.0f;

            if (!stageAnnouncer.activeSelf)
            {
                stageAnnouncer.SetActive(true);
            }
        }
    }
*/
    public void ClearBottomWave()
    {
        for (int i = obstacles.Count-1; i > 0; i --)
        {
            if (obstacles[i].transform.position.y < -8f)
            {
                Destroy(obstacles[i]);
                obstacles.Remove(obstacles[i]);
            }
        }
    }
}

