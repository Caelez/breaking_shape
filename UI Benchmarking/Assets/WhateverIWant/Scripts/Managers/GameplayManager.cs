﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameMode
{
	NormalMode = 0,
	EndlessMode,
	StoryMode,
    TutorialMode,
    TimeMode,
	DefenceMode,
    OtherMode,
	Total_Mode
}

public class GameplayManager : MonoBehaviour
{
    private PlaySceneDataManager dataManager;

    #region Singleton

    public static GameplayManager instance;
    void Awake()
    {
        instance = this;
        dataManager = PlaySceneDataManager.instance;
    }

    #endregion   
	
	public Text playerHealthText;
    public Text turnCountText;

    // Timer
//    private float timer = 0.0f;
//    private bool isTimePause = false;
    private bool isEnd = false;

    // Gameplay
	private int score = 0;
	private int obstacleCleared = 0;
	public int turnCount = 0;
	public int totalScore;
    public int premiumCurrencyCollected;
    private bool isEndGameRewarded = false;
    
	public float currentPlayerHealth;
	public float maxPlayerHealthHealth;

	public float defaultSpeed = 1.5f;
	[SerializeField] private float speedUp = 4.0f;

	// Skill Effect
//	private bool isNonbuffSkillActivated = false; // For non-buff type skill
	[HideInInspector] public bool isTidalWave = false;

    // Item Activated
    //[HideInInspector] public bool isItemActivated = false;
    //[HideInInspector] public Item activatedItem = null;
    public GameObject ball;
    public GameObject emptyGameObjectHolder;
	public GameObject AOEEffect;
    
    public GameObject scoreText;
    public GameObject turnsTextObject;
    public GameObject scoreObject;
    public GameObject premiumCrystalObjects;
    public GameObject endPanel;
    public Text[] endPanelText = new Text[3];

    public List<GameObject> scores = new List<GameObject>();
    public List<GameObject> turns = new List<GameObject>();
    public List<GameObject> crystals = new List<GameObject>();
    public List<Sprite> numberSprites = new List<Sprite>();

    private Vector3 barHealth;
    public GameObject hpBar;
//    private bool lose;

    // Use this for initialization
    void Start ()
    {
        dataManager.UpdateUpgradeValue();
        Application.targetFrameRate = 60;
        barHealth = hpBar.transform.localScale;
        PresetAllPassives();
        maxPlayerHealthHealth = dataManager.MaxHP;
		currentPlayerHealth = maxPlayerHealthHealth;
		DisplayPlayerHealth();

		Screen.autorotateToLandscapeLeft = false;
		Screen.autorotateToLandscapeRight = false;
		Screen.orientation = ScreenOrientation.Portrait;

		//endPanel.SetActive(false);
		isEnd = false;
//		isNonbuffSkillActivated = false;
		NormalSpeed();
		score = 0;
        totalScore = 0;
        turnCount = 0;
        isEndGameRewarded = false;
		string s = score.ToString();

        for (int i = 0; i < s.Length; i++)
        {
            if (scores.Count < s.Length)
            {
                GameObject g = Instantiate(scoreObject);
                scores.Add(g);
                g.transform.SetParent(scoreText.transform);
            }
            if (s[i].ToString() == "0")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.white;
            }else if (s[i].ToString() == "1")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.yellow;
            }
            else if (s[i].ToString() == "2")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.blue;
            }
            else if (s[i].ToString() == "3")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.red;
            }
            else if (s[i].ToString() == "4")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.green;
            }
            else if (s[i].ToString() == "5")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.blue;
            }
            else if (s[i].ToString() == "6")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.magenta;
            }
            else if (s[i].ToString() == "7")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.white;
            }
            else if (s[i].ToString() == "8")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.magenta;
            }
            else if (s[i].ToString() == "9")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.magenta;
            }
            scores[i].transform.position = new Vector3(-1.5f + i * .3f, 6.5f, 0f);
        }
        s = turnCount.ToString();

        for (int i = 0; i < s.Length; i++)
        {
            if (turns.Count < s.Length)
            {
                GameObject g = Instantiate(scoreObject);
                turns.Add(g);
                g.transform.SetParent(turnsTextObject.transform);
            }
            if (s[i].ToString() == "0")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.white;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[0];
            }
            else if (s[i].ToString() == "1")
            {

                turns[i].GetComponent<SpriteRenderer>().color = Color.yellow;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[1];
            }
            else if (s[i].ToString() == "2")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.blue;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[2];
            }
            else if (s[i].ToString() == "3")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.red;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[3];
            }
            else if (s[i].ToString() == "4")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.green;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[4];
            }
            else if (s[i].ToString() == "5")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.blue;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[5];
            }
            else if (s[i].ToString() == "6")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[6];
            }
            else if (s[i].ToString() == "7")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.white;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[7];
            }
            else if (s[i].ToString() == "8")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[8];
            }
            else if (s[i].ToString() == "9")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[9];
            }
            turns[i].transform.position = new Vector3(-1.5f + i * .3f, 7.2f, 0f);
        }

        s = premiumCurrencyCollected.ToString();

        for (int i = 0; i < s.Length; i++)
        {
            if (crystals.Count < s.Length)
            {
                GameObject g = Instantiate(scoreObject);
                crystals.Add(g);
                g.transform.SetParent(premiumCrystalObjects.transform);
            }
            if (s[i].ToString() == "0")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.white;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[0];
            }
            else if (s[i].ToString() == "1")
            {

                crystals[i].GetComponent<SpriteRenderer>().color = Color.yellow;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[1];
            }
            else if (s[i].ToString() == "2")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.blue;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[2];
            }
            else if (s[i].ToString() == "3")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.red;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[3];
            }
            else if (s[i].ToString() == "4")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.green;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[4];
            }
            else if (s[i].ToString() == "5")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.blue;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[5];
            }
            else if (s[i].ToString() == "6")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[6];
            }
            else if (s[i].ToString() == "7")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.white;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[7];
            }
            else if (s[i].ToString() == "8")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[8];
            }
            else if (s[i].ToString() == "9")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[9];
            }
            crystals[i].transform.position = new Vector3(2.5f + i * .3f, 6.6f, 0f);
        }
    }

	void PresetAllPassives()
	{
		LauncherBehaviour.instance.SpawnBalls();
    }
	

	public void UpdateScore(float value)
	{
		score += (int)value;
        string s = score.ToString();
        for (int i = 0; i < s.Length; i++)
        { 
            if (scores.Count < s.Length)
            {
                GameObject g = Instantiate(scoreObject);
                scores.Add(g);
                g.transform.SetParent(scoreText.transform);
            }
            if (s[i].ToString() == "0")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.white;
                scores[i].GetComponent<SpriteRenderer>().sprite = numberSprites[0];
            }
            else if (s[i].ToString() == "1")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.yellow;
                scores[i].GetComponent<SpriteRenderer>().sprite = numberSprites[1];
            }
            else if (s[i].ToString() == "2")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.blue;
                scores[i].GetComponent<SpriteRenderer>().sprite = numberSprites[2];
            }
            else if (s[i].ToString() == "3")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.red;
                scores[i].GetComponent<SpriteRenderer>().sprite = numberSprites[3];
            }
            else if (s[i].ToString() == "4")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.green;
                scores[i].GetComponent<SpriteRenderer>().sprite = numberSprites[4];
            }
            else if (s[i].ToString() == "5")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.blue;
                scores[i].GetComponent<SpriteRenderer>().sprite = numberSprites[5];
            }
            else if (s[i].ToString() == "6")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                scores[i].GetComponent<SpriteRenderer>().sprite = numberSprites[6];
            }
            else if (s[i].ToString() == "7")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.white;
                scores[i].GetComponent<SpriteRenderer>().sprite = numberSprites[7];
            }
            else if (s[i].ToString() == "8")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                scores[i].GetComponent<SpriteRenderer>().sprite = numberSprites[8];
            }
            else if (s[i].ToString() == "9")
            {
                scores[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                scores[i].GetComponent<SpriteRenderer>().sprite = numberSprites[9];
            }
            scores[i].transform.position = new Vector3(-1.5f + i * .3f, 6.5f, 0f);
        }
    }

	public int GetObstacleCleared()
	{
		return obstacleCleared;
	}

	public void UpdateObstacleCleared(int value)
	{
		obstacleCleared += value;

		/*if(obstacleClearedText)
		{
			obstacleClearedText.text = "Cleared: " + obstacleCleared.ToString();
		}*/
	}

	public int GetTurnCount()
	{
		return turnCount;
	}

	public void UpdateTurnCount(int value)
	{
		turnCount += value;
		if(turnCountText)
		{
			turnCountText.text = "Turn " + turnCount.ToString();
		}

		if(Time.timeScale > defaultSpeed)
		{
			NormalSpeed();
		}

        string s = turnCount.ToString();

        for (int i = 0; i < s.Length; i++)
        {
            if (turns.Count < s.Length)
            {
                GameObject g = Instantiate(scoreObject);
                turns.Add(g);
                g.transform.SetParent(turnsTextObject.transform);
            }
            if (s[i].ToString() == "0")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.white;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[0];
            }
            else if (s[i].ToString() == "1")
            {

                turns[i].GetComponent<SpriteRenderer>().color = Color.yellow;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[1];
            }
            else if (s[i].ToString() == "2")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.blue;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[2];
            }
            else if (s[i].ToString() == "3")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.red;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[3];
            }
            else if (s[i].ToString() == "4")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.green;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[4];
            }
            else if (s[i].ToString() == "5")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.blue;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[5];
            }
            else if (s[i].ToString() == "6")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[6];
            }
            else if (s[i].ToString() == "7")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.white;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[7];
            }
            else if (s[i].ToString() == "8")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[8];
            }
            else if (s[i].ToString() == "9")
            {
                turns[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                turns[i].GetComponent<SpriteRenderer>().sprite = numberSprites[9];
            }
            turns[i].transform.position = new Vector3(-1.5f + i * .3f, 7.2f, 0f);
        }
    }

	public bool GetIsEnd()
	{
		return isEnd;
	}

	public void DisplayPlayerHealth()
	{
		playerHealthText.text = currentPlayerHealth.ToString() ;
        hpBar.transform.localScale = new Vector3((currentPlayerHealth / maxPlayerHealthHealth) * barHealth.x, barHealth.y, barHealth.z);
        if (currentPlayerHealth <= 0)
        {
            currentPlayerHealth = 0;
        }
        else
        {
            CheckWaveClear(false);
        }
        // UIManager.instance.LifeBar.fillAmount = (float)(currentPlayerHealth / maxPlayerHealthHealth);
        //UIManager.instance.LifeBar.transform.GetChild(0).GetComponent<Text>().text = currentPlayerHealth.ToString();


    }

	public void PlayerHealthTakeDamage(float damage)
	{
		currentPlayerHealth -= damage;
		DisplayPlayerHealth();
       
        if (currentPlayerHealth <= 0)
		{
			currentPlayerHealth = 0;
			EndGame();
		}
		else
		{
			CheckWaveClear(false);
		}
	}

	// End the match
	public void EndGame()
	{
        Debug.Log("Score is: " + score);
        isEnd = true;
        endPanel.SetActive(true);
        endPanelText[0].text = "Score : \n " + score.ToString() ; 
        endPanelText[1].text = "Turns : \n  " + turnCount.ToString();
        endPanelText[2].text = "Premium Crystal : \n " + premiumCurrencyCollected.ToString();
        if(!isEndGameRewarded)
        {
            GameManager.instance.resourceManager.GainResource((int)(Math.Floor((float)score / 2)), false);
            GameManager.instance.resourceManager.GainResource(premiumCurrencyCollected, true);
            isEndGameRewarded = true;
        }
        Time.timeScale = 0.0f;
    }

	public void SpeedUp()
	{
		Time.timeScale = speedUp;
	}

	public void NormalSpeed()
	{
		Time.timeScale = defaultSpeed;
	}

	public void VibrateFor()
	{
		//Vibration.Vibrate();
	}

	public void UpdateWaveText(int current, bool final)
	{
		/*if(final)
		{
			waveText.text = "Final Wave";
		}
		else
		{ 
			waveText.text = GetOrdinal(current) + " Wave";
		}*/
	}

	private string GetOrdinal(int num)
	{
		switch(num % 100)
		{
		case 11:
		case 12:
		case 13:
			return num.ToString() + "th";
		}

		switch(num % 10)
		{
		case 1:
			return num.ToString() + "st";

		case 2:
			return num.ToString() + "nd";

		case 3:
			return num.ToString() + "rd";

		default:
			return num.ToString() + "th";
		}
	}

    public void CheckWaveClear(bool moveUp)
	{		
		if (moveUp)
		{
			StageManager.instance.MoveObstacleUp();
		}
	}
    
    public int GetScore()
    {
        return score;
    }
    public void EnemyBulletDamage(int damage = 1)
    {
        currentPlayerHealth -= damage;
        DisplayPlayerHealth();
    }

    public int GetTotalScore( )
    {
        return totalScore;
    }

    public void SetTotalScore(int temp)
    {
        totalScore += temp;
    }

    public void DisplayPremiumCrystalsCollected()
    {
        string s = premiumCurrencyCollected.ToString();

        for (int i = 0; i < s.Length; i++)
        {
            if (crystals.Count < s.Length)
            {
                GameObject g = Instantiate(scoreObject);
                crystals.Add(g);
                g.transform.SetParent(premiumCrystalObjects.transform);
            }
            if (s[i].ToString() == "0")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.white;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[0];
            }
            else if (s[i].ToString() == "1")
            {

                crystals[i].GetComponent<SpriteRenderer>().color = Color.yellow;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[1];
            }
            else if (s[i].ToString() == "2")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.blue;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[2];
            }
            else if (s[i].ToString() == "3")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.red;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[3];
            }
            else if (s[i].ToString() == "4")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.green;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[4];
            }
            else if (s[i].ToString() == "5")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.blue;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[5];
            }
            else if (s[i].ToString() == "6")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[6];
            }
            else if (s[i].ToString() == "7")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.white;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[7];
            }
            else if (s[i].ToString() == "8")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[8];
            }
            else if (s[i].ToString() == "9")
            {
                crystals[i].GetComponent<SpriteRenderer>().color = Color.magenta;
                crystals[i].GetComponent<SpriteRenderer>().sprite = numberSprites[9];
            }
            crystals[i].transform.position = new Vector3(2.5f + i * .3f, 6.6f, 0f);
        }
    }
}


