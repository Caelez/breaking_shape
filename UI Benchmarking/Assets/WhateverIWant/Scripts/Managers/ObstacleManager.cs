﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour {

    public static ObstacleManager instance;
    
    public List<PowerType> PowerUps = new List<PowerType>();
    public List<TrapType> Traps = new List<TrapType>();

    public GameObject[] baseObjects = new GameObject[8];
    public Vector4 Squareparameters = new Vector4();
    public int howManyLanes = 0;

    public List<GameObject> spawnedObjects = new List<GameObject>();
    public List<GameObject> rerollList = new List<GameObject>();
    public RuntimeAnimatorController trapAnimator;
    public RuntimeAnimatorController powerAnimator;

    public float timer;
    public int randomLowObstacleCount;
    public int randomHighObstacleCount;
    private int savedNumber;

    public int Difficulty;
    public bool onlyMirrored;
    private int DeathCounter;
    private bool premiumCurrencyReleased = true;
    private int level;

    private void Awake()
    {
        instance = this;
        timer = 0;
    }
    // Use this for initialization
    void Start() {
        for (int i = 0; i < howManyLanes + 1; i++)
        {
            RandomizeLanes(i);
        }
        foreach (GameObject g in spawnedObjects)
        {
            g.GetComponent<MapObjectsBase>().ResetGameObject();
            if (g.GetComponent<ObstacleBehaviour>() != null)
            {
                g.GetComponent<Collider2D>().isTrigger = false;
            }
            StageManager.instance.obstacles.Add(g);
        }
        spawnedObjects.Clear();
    }

    void RandomizeLanes(int yPosition)
    {
        int numberOfObstacles = Random.Range(randomLowObstacleCount, randomHighObstacleCount);
        int inCaseOfOddNumbers = numberOfObstacles;

        bool mirrored = (Random.Range(0f, 1f) > .5f);
        if (mirrored)
        {
            if (inCaseOfOddNumbers % 2 == 1)
            {
                inCaseOfOddNumbers--;
            }
            for (int i = 0; i < inCaseOfOddNumbers / 2f; i++)
            {
                int whatObject = Random.Range(0, baseObjects.Length);
                if (whatObject >= 8  )
                {
                    whatObject = Random.Range(0, 7);
                }
                
                int difficulty = Random.Range(Difficulty * 10 - 50, Difficulty * 10 + 51);
                if (DeathCounter > 0)
                {
                    if (Random.Range(0f, 1f) > .5f)
                    {
                        whatObject = 6;
                        DeathCounter--;
                    }
                }
                if (whatObject == baseObjects.Length - 1)
                {
                    DeathCounter++;
                }
                if (premiumCurrencyReleased && whatObject == 6)
                {
                    whatObject = Random.Range(0, 5);
                }
                GameObject spawnedObject = Instantiate(baseObjects[whatObject]);
                Vector3 newPosition = new Vector3();
                bool test = true;
                spawnedObject.name = baseObjects[whatObject].name;

                float shift = Squareparameters.y;
                if (numberOfObstacles % 2 == 1)
                {
                    shift = -0.7f;
                }
                else
                {
                    shift = -spawnedObject.GetComponent<SpriteRenderer>().bounds.size.x / 2f;
                }
                int tries = 0;
                do
                {
                    test = false;
                    newPosition = new Vector3(Random.Range(Squareparameters.x, shift), (Squareparameters.z +
                        -((Mathf.Abs(Squareparameters.w - Squareparameters.z) * ((float)yPosition / (float)howManyLanes)))), 0f);

                    tries++;
                    foreach (GameObject g in spawnedObjects)
                    {
                        if (g.transform.position.y == newPosition.y)
                        {
                            if ((g.GetComponent<SpriteRenderer>().bounds.size.x / 2f + spawnedObject.GetComponent<SpriteRenderer>().bounds.size.x / 2f) >
                            Mathf.Abs(newPosition.x - g.transform.position.x))
                            {
                                test = true;
                                break;
                            }
                        }
                    }

                } while (test && tries < 50);
                if (tries >= 50)
                {
                    Destroy(spawnedObject);
                    continue;
                }
                spawnedObject.transform.position = newPosition;

                spawnedObjects.Add(spawnedObject);

                GameObject spawnedObject1 = Instantiate(baseObjects[whatObject], new Vector3(-newPosition.x, newPosition.y, newPosition.z), Quaternion.identity);
                spawnedObjects.Add(spawnedObject1);
                spawnedObject.GetComponent<MapObjectsBase>().SetMirroredObject(spawnedObject1);
                spawnedObject1.GetComponent<MapObjectsBase>().SetMirroredObject(spawnedObject);
                spawnedObject.GetComponent<MapObjectsBase>().SetWhichSide(MapObjectsBase.WhichSide.Left);
                spawnedObject1.GetComponent<MapObjectsBase>().SetWhichSide(MapObjectsBase.WhichSide.Right);
                spawnedObject.GetComponent<MapObjectsBase>().placedID = i;
                spawnedObject1.GetComponent<MapObjectsBase>().placedID = i;
                spawnedObject1.name = baseObjects[whatObject].name;
                spawnedObject = InitializeSpriteAndObject(spawnedObject, whatObject, difficulty, -1);
                spawnedObject1 = InitializeSpriteAndObject(spawnedObject1, whatObject, difficulty, savedNumber);
            }
            if (numberOfObstacles % 2 == 1)
            {
                int whatObject = Random.Range(0, baseObjects.Length);
                if (whatObject >= 8)
                {
                    whatObject = Random.Range(0, 7);
                }
                if (premiumCurrencyReleased && whatObject == 6)
                {
                    whatObject = Random.Range(0, 5);
                }

                if (DeathCounter > 0)
                {
                    if (Random.Range(0f, 1f) > .5f)
                    {
                        whatObject = 6;
                        DeathCounter--;
                    }
                }
                if (whatObject == baseObjects.Length - 1)
                {
                    DeathCounter++;
                }
                GameObject spawnedObject = Instantiate(baseObjects[whatObject]);
                Vector3 newPosition = new Vector3();
                newPosition = new Vector3(0f, (Squareparameters.z +
                    -(Mathf.Abs(Squareparameters.z - Squareparameters.w) * ((float)yPosition / (float)howManyLanes))), 0f);
                spawnedObject.name = baseObjects[whatObject].name;

                spawnedObject.transform.position = newPosition;
                spawnedObjects.Add(spawnedObject);
                spawnedObject.GetComponent<MapObjectsBase>().SetWhichSide(MapObjectsBase.WhichSide.Middle);
                spawnedObject = InitializeSpriteAndObject(spawnedObject, whatObject, -1);
            }
        }
        else
        {
            for (int i = 1; i < inCaseOfOddNumbers; i++)
            {
                int whatObject = Random.Range(0, baseObjects.Length);
                if (whatObject >= 8)
                {
                    whatObject = Random.Range(0, 7);
                }
                if (premiumCurrencyReleased && whatObject == 6)
                {
                    whatObject = Random.Range(0, 5);
                }
                if (DeathCounter > 0)
                {
                    if (Random.Range(0f, 1f) > .5f)
                    {
                        whatObject = 6;
                        DeathCounter--;
                    }
                }
                if (whatObject == baseObjects.Length - 1)
                {
                    DeathCounter++;
                }
                GameObject spawnedObject = Instantiate(baseObjects[whatObject]);
                int difficulty = Random.Range(Difficulty * 10 - 50, Difficulty * 10 + 51);

                bool test = true;

                Vector3 newPosition = new Vector3();
                spawnedObject.name = baseObjects[whatObject].name;
                int tries = 0;
                do
                {
                    test = false;
                    newPosition = new Vector3(Random.Range(-2.9f, 2.9f), (Squareparameters.z +
                        -((Mathf.Abs(Squareparameters.w - Squareparameters.z) * ((float)yPosition / (float)howManyLanes)))), 0f);

                    tries++;
                    foreach (GameObject g in spawnedObjects)
                    {
                        if (g.transform.position.y == newPosition.y)
                        {
                            if ((g.GetComponent<SpriteRenderer>().bounds.size.x / 2f + spawnedObject.GetComponent<SpriteRenderer>().bounds.size.x / 2f) >
                                Mathf.Abs(newPosition.x - g.transform.position.x))
                            {
                                test = true;
                                break;
                            }
                        }
                    }

                } while (test && tries < 50);
                if (tries >= 50)
                {
                    Destroy(spawnedObject);
                    continue;
                }
                spawnedObject.transform.position = newPosition;
                spawnedObjects.Add(spawnedObject);
                spawnedObject.GetComponent<MapObjectsBase>().placedID = i;
                spawnedObject = InitializeSpriteAndObject(spawnedObject, whatObject, difficulty, -1);
                spawnedObject.GetComponent<MapObjectsBase>().SetWhichSide(MapObjectsBase.WhichSide.Random);
            }
        }
    }

    private GameObject InitializeSpriteAndObject(GameObject g, int whatObject, int difficulty, int SavedNumber = -1)
    {
        int randomizeType = SavedNumber;
       
        if (difficulty < 33)
        {
        } else if (difficulty < 66) {
            g.name += " Silver";
        } else {
            g.name += " Gold";
        }

		if (whatObject < 7)
		{
            if (whatObject == 0)
			{
                g.GetComponent<ObstacleBehaviour>().SetHealth(GetHP());
                g.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(0));
                //g.GetComponent<SpriteRenderer>().color = Color.yellow;
            }
			else if (whatObject == 1)
            {
                g.GetComponent<ObstacleBehaviour>().SetHealth(GetHP());
                g.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(0));
                //g.GetComponent<SpriteRenderer>().color = Color.green;
            }
			else if (whatObject == 2)
			{
                g.GetComponent<ObstacleBehaviour>().SetHealth(GetHP());
                g.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(0));
                //g.GetComponent<SpriteRenderer>().color = Color.red;
            }
			else if (whatObject == 3)
			{
                g.GetComponent<ObstacleBehaviour>().SetHealth(GetHP());
                g.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(0));
                //g.GetComponent<SpriteRenderer>().color = Color.green;
            }
			else if (whatObject == 4)
			{
                g.GetComponent<ObstacleBehaviour>().SetHealth(GetHP());
                g.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(0));
                //g.GetComponent<SpriteRenderer>().color = Color.magenta;
            }
			else if (whatObject == 5)
			{
                g.GetComponent<ObstacleBehaviour>().SetHealth(GetHP());
                g.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(0));
                //g.GetComponent<SpriteRenderer>().color = Color.cyan;
            }
            else if (whatObject == 6)
            {
                premiumCurrencyReleased = true;

                g.GetComponent<ObstacleBehaviour>().SetHealth(GetHPForCrystal());
                g.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(0));
                //g.GetComponent<SpriteRenderer>().color = Color.yellow;
            }
        }
		else if (whatObject == 7)
		{
			if (SavedNumber == -1)
			{
				randomizeType = Random.Range(0, PowerUps.Count);
			}
            g.GetComponent<Animator>().runtimeAnimatorController = powerAnimator;

            g.GetComponent<PowerBehaviour>().powerType = PowerUps[randomizeType];
			g.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(2));

		}
		else if (whatObject == 8)
		{
			if (SavedNumber == -1)
			{
				randomizeType = Random.Range(0, Traps.Count);
			}
			g.GetComponent<Animator>().runtimeAnimatorController = trapAnimator;
			g.GetComponent<TrapBehaviour>().trapType = Traps[randomizeType];
			g.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(1));
		}
		savedNumber = randomizeType;
		
		return g;
	}

	void RandomizeWaves(int numberOfWaves)
	{
		int numberOfObstacles = Random.Range(randomLowObstacleCount, randomHighObstacleCount);
		int inCaseOfOddNumbers = numberOfObstacles;

		bool mirrored = (Random.Range(0f, 1f) > .5f);
        for (int j = 0; j < numberOfWaves; j++)
        {
            if (mirrored)
            {
                if (inCaseOfOddNumbers % 2 == 1)
                {
                    inCaseOfOddNumbers--;
                }
                for (int i = 0; i < inCaseOfOddNumbers / 2f; i++)
                {
                    int whatObject = Random.Range(0, baseObjects.Length);
                    if (whatObject >= 8)
                    {
                        whatObject = Random.Range(0, 7);
                    }
                    if (premiumCurrencyReleased && whatObject == 6)
                    {
                        whatObject = Random.Range(0, 5);
                    }

                    int difficulty = Random.Range(Difficulty * 10 - 50, Difficulty * 10 + 51);
                    if (DeathCounter > 0)
                    {
                        if (Random.Range(0f, 1f) > .5f)
                        {
                            whatObject = 6;
                            DeathCounter--;
                        }
                    }
                    if (whatObject == baseObjects.Length - 1)
                    {
                        DeathCounter++;
                    }
                    GameObject spawnedObject = Instantiate(baseObjects[whatObject]);
                    Vector3 newPosition = new Vector3();
                    bool test = true;
                    spawnedObject.name = baseObjects[whatObject].name;

                    int tries = 0;
                    do
                    {
                        test = false;
                        newPosition = new Vector3(Random.Range(-2.9f, 2.9f), -8.5f + (j*-1), 0f);

                        tries++;
                        foreach (GameObject g in spawnedObjects)
                        {
                            if (g.transform.position.y == newPosition.y)
                            {
                                if ((g.GetComponent<SpriteRenderer>().bounds.size.x / 2f + spawnedObject.GetComponent<SpriteRenderer>().bounds.size.x / 2f) >
                                Mathf.Abs(newPosition.x - g.transform.position.x))
                                {
                                    test = true;
                                    break;
                                }
                            }
                        }

                    } while (test && tries < 50);
                    if (tries >= 50)
                    {
                        Destroy(spawnedObject);
                        continue;
                    }
                    spawnedObject.transform.position = newPosition;

                    spawnedObjects.Add(spawnedObject);

                    GameObject spawnedObject1 = Instantiate(baseObjects[whatObject], new Vector3(-newPosition.x, newPosition.y, newPosition.z), Quaternion.identity);
                    spawnedObjects.Add(spawnedObject1);
                    spawnedObject.GetComponent<MapObjectsBase>().SetMirroredObject(spawnedObject1);
                    spawnedObject1.GetComponent<MapObjectsBase>().SetMirroredObject(spawnedObject);
                    spawnedObject.GetComponent<MapObjectsBase>().SetWhichSide(MapObjectsBase.WhichSide.Left);
                    spawnedObject1.GetComponent<MapObjectsBase>().SetWhichSide(MapObjectsBase.WhichSide.Right);
                    spawnedObject.GetComponent<MapObjectsBase>().placedID = i;
                    spawnedObject1.name = baseObjects[whatObject].name;
                    spawnedObject1.GetComponent<MapObjectsBase>().placedID = i;
                    spawnedObject = InitializeSpriteAndObject(spawnedObject, whatObject, difficulty, -1);
                    spawnedObject1 = InitializeSpriteAndObject(spawnedObject1, whatObject, difficulty, savedNumber);
                }
                if (numberOfObstacles % 2 == 1)
                {
                    int whatObject = Random.Range(0, baseObjects.Length);
                    if (whatObject >= 8)
                    {
                        whatObject = Random.Range(0, 7);
                    }
                    if (premiumCurrencyReleased && whatObject == 6)
                    {
                        whatObject = Random.Range(0, 5);
                    }
                    if (DeathCounter > 0)
                    {
                        if (Random.Range(0f, 1f) > .5f)
                        {
                            whatObject = 6;
                            DeathCounter--;
                        }
                    }
                    if (whatObject == baseObjects.Length - 1)
                    {
                        DeathCounter++;
                    }
                    GameObject spawnedObject = Instantiate(baseObjects[whatObject]);
                    spawnedObject.name = baseObjects[whatObject].name;
                    Vector3 newPosition = new Vector3();
                    newPosition = new Vector3(0f, -8.5f + (j * -1), 0f);

                    spawnedObject.transform.position = newPosition;
                    spawnedObjects.Add(spawnedObject);
                    spawnedObject.GetComponent<MapObjectsBase>().SetWhichSide(MapObjectsBase.WhichSide.Middle);
                    spawnedObject = InitializeSpriteAndObject(spawnedObject, whatObject, -1);
                }
            }
            else
            {
                for (int i = 1; i < inCaseOfOddNumbers; i++)
                {
                    int whatObject = Random.Range(0, baseObjects.Length);
                    if (whatObject >= 8)
                    {
                        whatObject = Random.Range(0, 7);
                    }
                    if (premiumCurrencyReleased && whatObject == 6)
                    {
                        whatObject = Random.Range(0, 5);
                    }

                    GameObject spawnedObject = Instantiate(baseObjects[whatObject]);
                    if (whatObject == baseObjects.Length - 1)
                    {
                        DeathCounter++;
                    }
                    int difficulty = Random.Range(Difficulty * 10 - 50, Difficulty * 10 + 51);

                    bool test = true;
                    spawnedObject.name = baseObjects[whatObject].name;
                    Vector3 newPosition = new Vector3();
                    int tries = 0;
                    do
                    {
                        test = false;
                        newPosition = new Vector3(Random.Range(-2.9f, 2.9f), -8.5f + (j * -1), 0f);

                        tries++;
                        foreach (GameObject g in spawnedObjects)
                        {
                            if (g.transform.position.y == newPosition.y)
                            {
                                if ((g.GetComponent<SpriteRenderer>().bounds.size.x / 2f + spawnedObject.GetComponent<SpriteRenderer>().bounds.size.x / 2f) >
                                    Mathf.Abs(newPosition.x - g.transform.position.x))
                                {
                                    test = true;
                                    break;
                                }
                            }
                        }

                    } while (test && tries < 50);
                    if (tries >= 50)
                    {
                        Destroy(spawnedObject);
                        continue;
                    }
                    spawnedObject.transform.position = newPosition;
                    spawnedObjects.Add(spawnedObject);
                    spawnedObject.GetComponent<MapObjectsBase>().placedID = i;
                    
                    spawnedObject.GetComponent<MapObjectsBase>().SetWhichSide(MapObjectsBase.WhichSide.Random);
                    spawnedObject = InitializeSpriteAndObject(spawnedObject, whatObject, difficulty);
                }
            }
        }
	}

	public void SpawnNewWave()
	{
        StageManager.instance.ClearBottomWave();
		RandomizeWaves(GetNumberOfWaves());
		foreach (GameObject g in spawnedObjects)
		{
			g.GetComponent<MapObjectsBase>().ResetGameObject();
			if (g.GetComponent<ObstacleBehaviour>() != null)
			{
				g.GetComponent<Collider2D>().isTrigger = false;
			}
			StageManager.instance.obstacles.Add(g);
			
			g.GetComponent<MapObjectsBase>().MoveUpInvoke();
		}
		spawnedObjects.Clear();
	}
	
    public float GetHP()
    {
        float hp = 0 ;
        if (GameplayManager.instance.GetTurnCount() < 5)
        {
            hp = 1;
        }else if (GameplayManager.instance.GetTurnCount() < 15)
        {
            hp = Random.Range(2,5);
        }
        else if (GameplayManager.instance.GetTurnCount() < 35)
        {
            hp = Random.Range(2, 11);
        }
        else if (GameplayManager.instance.GetTurnCount() < 70)
        {
            hp = Random.Range(10, 25);
        }
        else
        {
            hp = Random.Range(10, 25);
            int i = (int)((GameplayManager.instance.GetTurnCount()-70) /10);
            hp += 10 * i;
        }
        return hp;
    }

    public float GetHPForCrystal()
    {
        float hp = 0;
        if (GameplayManager.instance.GetTurnCount() < 5)
        {
            hp = 1;           
        }
        else if (GameplayManager.instance.GetTurnCount() < 15)
        {
            hp = 2;           
        }
        else if (GameplayManager.instance.GetTurnCount() < 35)
        {
            hp = 3;           
        }
        else if (GameplayManager.instance.GetTurnCount() < 70)
        {
            hp = 4;           
        }
        else
        {
            hp = 5;
            int i = (int)((GameplayManager.instance.GetTurnCount() - 70f) / 10);
            hp += 1 * i;            
        }
        return hp;
    }

    public int GetNumberOfWaves()
    {
        int waves;
        if (GameplayManager.instance.GetTurnCount() < 5)
        {
            waves = 2;
            if (level < 1)
            {
                level++;
                premiumCurrencyReleased = false;
            }
        }
        else if (GameplayManager.instance.GetTurnCount() < 15)
        {
            waves = 3;
            if (level < 2)
            {
                level++;
                premiumCurrencyReleased = false;
            }
        }
        else if (GameplayManager.instance.GetTurnCount() < 35)
        {
            waves = 4;
            if (level < 3)
            {
                level++;
                premiumCurrencyReleased = false;
            }
        }
        else if (GameplayManager.instance.GetTurnCount() < 70)
        {
            waves = 5;
            if (level < 4)
            {
                level++;
                premiumCurrencyReleased = false;
            }
        }
        else
        {
            waves = 6;
            int i = (int)((GameplayManager.instance.GetTurnCount() - 70f) / 10);
            if (level < 5 + i)
            {
                level++;
                premiumCurrencyReleased = false;
            }
        }
        return waves;
    }
}
