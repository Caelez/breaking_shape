﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboManager : MonoBehaviour
{
	#region Singleton

	public static ComboManager instance;
	private void Awake()
	{
		if(!instance)
		{
			instance = this;
		}
	}

	#endregion
	[SerializeField] private bool isDurationReset = false;
	[SerializeField] private int displayTarget = 0;
	[SerializeField] private float displayDuration = 1.0f;
	private float timer = 0.0f;
	private int combo = 0;
	[SerializeField] private int highestCombo = 0;
	private Text comboText;

	// Use this for initialization
	void Start ()
	{
		comboText = GetComponent<Text>();
		highestCombo = 0;
		comboText.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
	}

	private void Update()
	{
		if(timer > 0.0f)
		{
			timer -= Time.deltaTime;
		
			if(timer <= 0.0f)
			{
				if(!isDurationReset)
				{
					timer = 0.0f;
				}
				else
				{
					ResetCombo();
					return;
				}
			}

			float alpha = timer/displayDuration;
			comboText.color = new Color(1.0f, 1.0f, 1.0f, alpha);
		}
	}

	public void IncreaseCombo()
	{
		combo ++;

		if(combo > highestCombo)
		{
			highestCombo = combo;
		}

		if(combo >= displayTarget)
		{
			DisplayCombo();
		}
	}

	public void DisplayCombo()
	{
		timer = displayDuration;
		comboText.text = combo.ToString();
	}

	public void ResetCombo()
	{
		timer = 0.0f;
		combo = 0;
		comboText.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
	}

	public int GetHighestCombo()
	{
		return highestCombo;
	}
}
