﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{
//	public Image healthBar;
//	public Text healthBarText;
	[SerializeField] private int maxHealth = 100;
	private int currentHealth;

	// Use this for initialization
	void Start ()
	{
		SetHealth();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void SetHealth()
	{
		currentHealth = maxHealth;
//		healthBarText.text = GetHealthPercentage();
	}

	public void TakeDamage(int value)
	{
		currentHealth -= value;
//		float percentage = (float)currentHealth / (float)maxHealth;
//		healthBar.fillAmount = percentage;
//		healthBarText.text = GetHealthPercentage();
	}

	public int GetHealth()
	{
		return currentHealth;
	}

	public string GetHealthPercentage()
	{
		return currentHealth.ToString() + "/" + maxHealth.ToString();
	}
}
