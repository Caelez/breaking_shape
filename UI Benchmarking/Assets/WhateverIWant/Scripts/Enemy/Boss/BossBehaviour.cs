﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehaviour : MonoBehaviour
{
	public static BossBehaviour instance;
	private ObstacleBehaviour obstacle;
	public ObstacleBehaviour obstacleBehaviour
	{
		get
		{
			return obstacle;
		}
		set
		{
			obstacle = value;
		}
	}

	void Awake()
	{
		instance = this;
	}

	// Use this for initialization
	void Start ()
	{
		obstacle = GetComponent<ObstacleBehaviour>();
	}
}
