﻿/// <summary>
/// This script is to handle spawner of all the obstacles, powers and traps
/// </summary>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnerBehaviour : MonoBehaviour
{
	#region Singleton

	public static SpawnerBehaviour instance;
	
	void Awake()
	{
		instance = this;
	}
	
	#endregion

	#region Public Variable

	[System.Serializable]
	public struct SpawnItem
	{
		public GameObject m_object;
		[Range(0.0f, 100.0f)]
		public int m_spawnWeight;
		//[SerializeField] private string m_percentage;
	}

	public float spacing = 0.0f; // Spacing between the obstacles in a row

	// Spawnable obstacles in a row
	public int minSpawnSize = 5; // Minimun
	public int maxSpawnSize = 6; // Maximun

	// Obstacle health
	public int healthIncrement = 5; // Increase max total health that avaliable to obstacle
	public int turnForHealthIncrease = 2; // Turn for health increase

	// Power
	public int turnForPowerSpawn = 3; // Turn for power spawn

	// Lists of all spawnable type
	public List<SpawnItem> obstaclesType = new List<SpawnItem>(); // Obstacle
	public List<SpawnItem> powerType = new List<SpawnItem>(); // Power
//	public List<SpawnItem> trapType = new List<SpawnItem>(); // Trap

	#endregion

	#region Private Variable

	public List<GameObject> spawnedObstacles = new List<GameObject>(); // Store all the spawned obstacles
	private int spawnNumber = 0; // Obstacle to spawn for the current row
	private int spawnHealthRate = 0; // Maximun health for the obstacle that spawn
	private bool isSpawnPower = false; // Should the spawner spawn power
	private int maxPowerWeight = 0; // Max power weight from the power type list
	private int maxObstacleWeight = 0; // Max obstacle weight form the obstacle type list
//	private int maxTrapWeight = 0; // Max obstacle weight form the trap type list

	#endregion

	[SerializeField] private bool spawnObstacle = false; // For Testing

	// Use this for initialization
	void Start ()
	{
		// Setup all obstacles type to the list
		foreach(SpawnItem p in powerType)
		{
			maxPowerWeight += p.m_spawnWeight; // Storing all the weight of each item in the power type list
		}

		foreach(SpawnItem o in obstaclesType)
		{
			maxObstacleWeight += o.m_spawnWeight; // Storing all the weight of each item in the obstacle type list
		}

//		foreach(SpawnItem t in trapType)
//		{
//			maxTrapWeight += t.m_spawnWeight; // Storing all the weight of each item in the trap type list
//		}

		// Setting up starting spawn rate
		spawnNumber = maxSpawnSize;
		GameplayManager.instance.UpdateTurnCount(1);

		// Spawn Obstacles
		SpawnObstacle();
		//StageManager.instance.SpawnWave(0);
	}
	
	// Update is called once per frame
	void Update ()
	{
		// Testing only
		if(spawnObstacle)
		{
			SpawnObstacle();
			spawnObstacle = false;
		}
	}

	#region Public Function

	// Spawn Manager
	/// <summary>
	/// Spawn Manager
	/// Endless mode spawner will spawn obstacle ramdomly based on the obstacleToSpawn
	/// </summary>
	public void SpawnObstacle()
	{
		// Offset spawn position
		float offset = getOffset();
		
		// Number of obstacles need to spawn
		int obstaclesToSpawn = Random.Range(2, spawnNumber);
		
		// Increase health range for obstacle every 2 steps
		if(GameplayManager.instance.GetTurnCount() % turnForHealthIncrease == 0)
		{
			spawnHealthRate += healthIncrement;
		}
		
		// Check to confirm whether to spawn power
		if(GameplayManager.instance.GetTurnCount() % turnForPowerSpawn == 1 && !isSpawnPower)
		{
			isSpawnPower = true;
		}
		
		// Spawn the obstacles
		for(int i = 0; i < spawnNumber; i++)
		{
			// Check the number of obstacles to spawn is more then 0
			if(obstaclesToSpawn > 0)
			{
				// Get the position of the obstacle to be spawn
				Vector3 pos = new Vector3((spacing * -((float)((spawnNumber / 2) - i)) + offset), 0.0f, 0.0f);
				
				// Check is it still have space to spawn and obstacleToSpawn is more then 1
				if(obstaclesToSpawn < spawnNumber - i && obstaclesToSpawn > 1)
				{
					int spawnRate = 0;
					if(isSpawnPower)
					{
						// Check whether to spawn Power or not
						spawnRate = Random.Range(0, 2);
						if(spawnRate == 1)
						{
							SpawnPowerType(pos);
							isSpawnPower = false;
							obstaclesToSpawn--;
							continue;
						}
					}
					
					spawnRate = Random.Range(0, 2); // Check whether to spawn Obstacle or not
					if(spawnRate == 1)
					{
						SpawnObstacleType(pos);
						obstaclesToSpawn--;
						continue;
					}
				}
				else if(obstaclesToSpawn >= spawnNumber - i)
				{
					if(isSpawnPower)
					{
						SpawnPowerType(pos);
						isSpawnPower = false;
						obstaclesToSpawn--;
						continue;
					}
					
					SpawnObstacleType(pos);
					obstaclesToSpawn--;
					continue;
				}
			}
		}
		
		// Move up all obstacles
		MoveObstaclesUp();
		
		// Change spawn number for next line
		UpdateSpawnNumber();
	}

	// Move all obstacle up
	public void MoveObstaclesUp()
	{
		if (spawnedObstacles.Count > 0)
		{
			for(int i = 0; i < spawnedObstacles.Count; i++)
			{
				if(spawnedObstacles[i].CompareTag("Obstacle"))
				{
					spawnedObstacles[i].GetComponent<ObstacleBehaviour>().CheckAfterEffect();
					if(!spawnedObstacles[i].GetComponent<ObstacleBehaviour>())
					{
						i--;
					}
				}
				else if(spawnedObstacles[i].CompareTag("Power"))
				{
					spawnedObstacles[i].GetComponent<PowerBehaviour>().CheckAfterEffect();
				}
				else if(spawnedObstacles[i].CompareTag("Trap"))
				{
					spawnedObstacles[i].GetComponent<TrapBehaviour>().CheckAfterEffect();
				}
			}

			// Move up all obstacles
			foreach(GameObject go in spawnedObstacles)
			{
				go.GetComponent<MapObjectsBase>().MoveUp();
			}
		}
	}

	// Remove the obstacles from the spawnedObstacles list
	public void RemoveFromList(GameObject g)
	{
		foreach(GameObject go in spawnedObstacles)
		{
			if(go == g)
			{
				spawnedObstacles.Remove(go);
				Destroy(g);
				break;
			}
		}
	}

	#endregion

	#region Private Function

	// Get offset 
	private float getOffset()
	{
		if(spawnNumber % 2 == 0)
		{
			return (spacing / 2.0f);
		}

		return 0.0f;
	}

	// Change spawnNumber between minSpawnSize and maxSpawnSize
	private void UpdateSpawnNumber()
	{
		if(spawnNumber == maxSpawnSize)
		{
			spawnNumber = minSpawnSize;
		}
		else
		{
			spawnNumber = maxSpawnSize;
		}
	}

	// Spawn Obstacle type on position
	private void SpawnObstacleType(Vector3 pos)
	{
		int randObstacleWeight = Random.Range(1, maxObstacleWeight + 1); // Random the type of obstacle to spawn between 1 to max weight from the list
		int obstacleToSpawn = 0; // To store the index of the power to spawn

		// Check for which power type to spawn
		for(int k = 0; k < obstaclesType.Count; k++)
		{
			if(randObstacleWeight <= obstaclesType[k].m_spawnWeight)
			{
				obstacleToSpawn = k;
				break;
			}
			randObstacleWeight -= obstaclesType[k].m_spawnWeight; // Deduct randPowerWeight from the PowerType's weight
		}

		int randHealth = Mathf.Abs(Random.Range(1, spawnHealthRate + 5 + 1)); // Random the health for the obstacle
		// Avoid zero health
		if(randHealth == 0)
		{
			randHealth ++;
		}

		Vector3 angle = new Vector3(0.0f, 0.0f, Random.Range(0.0f, 359.0f)); // Random an angle for the obstacle

		// Spawn Obstacle
//		GameObject obstacle = Instantiate(obstaclesType[obstacleToSpawn].m_object, transform.position + pos, Quaternion.Euler(angle));
		GameObject obstacle = Instantiate(obstaclesType[obstacleToSpawn].m_object);
		obstacle.transform.position = transform.position + pos;

		if(obstacle.CompareTag("Obstacle"))
		{
			obstacle.GetComponent<SpriteManager>().RandomSprite();
			obstacle.transform.rotation = Quaternion.Euler(angle);
			obstacle.GetComponent<ObstacleBehaviour>().SetHealth(randHealth);
		}

		obstacle.transform.SetParent(transform);
		spawnedObstacles.Add(obstacle);
	}

	// Spawn Power type on position
	private void SpawnPowerType(Vector3 pos)
	{
		int randPowerWeight = Random.Range(1, maxPowerWeight + 1); // Random the type of power to spawn between 1 to max weight from the list
		int powerToSpawn = 0; // To store the index of the power to spawn

		// Check for which power type to spawn
		for(int j = 0; j < powerType.Count; j++)
		{
			if(randPowerWeight <= powerType[j].m_spawnWeight)
			{
				powerToSpawn = j;
				break;
			}
			randPowerWeight -= powerType[j].m_spawnWeight; // Deduct randPowerWeight from the PowerType's weight
		}

		// Spawn the power
		GameObject power = Instantiate(powerType[powerToSpawn].m_object, transform.position + pos, Quaternion.identity);
		power.transform.SetParent(transform);
		spawnedObstacles.Add(power);
	}

	#endregion
}
