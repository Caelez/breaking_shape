﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerBehaviour : MapObjectsBase
{
	public PowerType powerType;
	public GameObject ball;
	public float value;

    private PlaySceneDataManager dataManager;

    void Start()
	{
        dataManager = PlaySceneDataManager.instance;
        m_health = 1;
        rotateAround = true;
        if (powerType == PowerType.ExtraBall1)
        {
            transform.name = "Fission";
            GetComponent<Animator>().Play("ExtraBallAnimation");
            descriptionOfObject = "Creates 1 more balls.";
        }
        else if (powerType == PowerType.ExtraBall2)
        {
            transform.name = "Big Fission";
            GetComponent<Animator>().Play("ExtraBall3Animation");
            descriptionOfObject = "Creates 3 more balls.";
        }
        else if (powerType == PowerType.ExtraBall5)
        {
            transform.name = "Mega Fission";
            GetComponent<Animator>().Play("ExtraBall5Animation");
            descriptionOfObject = "Creates 5 more balls.";
        }
        /*else if (powerType == PowerType.EnlargeBall)
        {
            transform.name = "MegaMorph";
            GetComponent<Animator>().Play("EnlargeBallAnimation");
            descriptionOfObject = "Increase size and gravity of ball that hit this boon.";
        }
        else if (powerType == PowerType.ShrinkBall)
        {
            transform.name = "Shrinking";
            GetComponent<Animator>().Play("ShrinkBallAnimation");
            descriptionOfObject = "Shrink ball and decrease gravity of ball that hit this boon.";
        }
        else if (powerType == PowerType.DamageIncrease)
        {
            transform.name = "Damage Increase";
            GetComponent<Animator>().Play("ExtraDamageAnimation");
            descriptionOfObject = "Increase damage of ball that hit this object.";
        }
        else if (powerType == PowerType.FlameAOE)
        {
            transform.name = "Incineration";
            GetComponent<Animator>().Play("FlameAnimation");
            descriptionOfObject = "Deals 3 fire damage to all objects, and leaves them burnt.";
        }
        else if (powerType == PowerType.EarthAOE)
        {
            transform.name = "SandStorm";
            GetComponent<Animator>().Play("SandAnimation");
            descriptionOfObject = "Deals 3 earth damage to all objects, and leaves them in a slow state.";
        }
        else if (powerType == PowerType.IceAOE)
        {
            transform.name = "Blizzard";
            GetComponent<Animator>().Play("SnowAnimation");
            descriptionOfObject = "Deals 3 Water damage to all objects, and leaves them frozen.";
        }
        else if (powerType == PowerType.NatureAOE)
        {
            transform.name = "Parasitic Plague";
            GetComponent<Animator>().Play("ParasiteAnimation");
            descriptionOfObject = "Deals 3 Nature damage to all objects, and changes obstacle type to wood.";
        }*/
        else if (powerType == PowerType.HealUs)
        {
            transform.name = "HealUs";
            GetComponent<Animator>().Play("HealAnimation");
            descriptionOfObject = "Heals " + dataManager.Regen + " HP.";
        }
      
        baseHealth = m_health;
    }
	

	// Check is there after effect

	void OnTriggerEnter2D(Collider2D co)
	{
		if(co.gameObject.CompareTag("Collector") && GameplayManager.instance.isTidalWave)
		{
			isPushedBelow = true;
			GetComponent<Collider2D>().enabled = false;
			GetComponent<SpriteRenderer>().enabled = false;
		}
		if (co.gameObject.CompareTag("Ball"))
		{
            //-----PowerUPs-----//
            if (powerType == PowerType.ExtraBall1)
			{
                if (LauncherBehaviour.instance.hiddenBalls.Count < dataManager.MaxCount)
                {
                    //                    GameObject temp = Instantiate(GameplayManager.instance.ball, transform.position, Quaternion.identity);
                    GameObject temp = BallPool.instance.Spawn(GameplayManager.instance.ball.tag, transform.position, Quaternion.identity);

                    temp.GetComponent<BallBehaviour>().LaunchBallsUp();
                    if (LauncherBehaviour.instance)
                    {
                        LauncherBehaviour.instance.AddBall(temp);
                    }
                    temp.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(3));

                    temp.layer = 8;
                }
                
            }
			else if (powerType == PowerType.ExtraBall2)
			{
				for (int i = 0; i < 2; i++)
				{
                    if (LauncherBehaviour.instance.hiddenBalls.Count < dataManager.MaxCount)
                    {
                        //                    GameObject temp = Instantiate(GameplayManager.instance.ball, transform.position, Quaternion.identity);
                        GameObject temp = BallPool.instance.Spawn(GameplayManager.instance.ball.tag, transform.position, Quaternion.identity);
                        temp.GetComponent<BallBehaviour>().LaunchBallsUp();

                        if (LauncherBehaviour.instance)
                        {
                            LauncherBehaviour.instance.AddBall(temp);
                        }
                        temp.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(3));

                        temp.layer = 8;
                    }                    
                }
			}
			else if (powerType == PowerType.ExtraBall5)
			{
				for (int i = 0; i < 5; i++)
				{
                    if (LauncherBehaviour.instance.hiddenBalls.Count < dataManager.MaxCount)
                    {
                        //                    GameObject temp = Instantiate(GameplayManager.instance.ball, transform.position, Quaternion.identity);
                        GameObject temp = BallPool.instance.Spawn(GameplayManager.instance.ball.tag, transform.position, Quaternion.identity);
                        temp.GetComponent<BallBehaviour>().LaunchBallsUp();

                        if (LauncherBehaviour.instance)
                        {
                            LauncherBehaviour.instance.AddBall(temp);
                        }
                        temp.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(3));

                        temp.layer = 8;
                    }					
                }
			}
			/*else if (powerType == PowerType.EnlargeBall)
			{
                if (!LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().ActivatedBalls.Contains(co.gameObject))
                {
                    BuffAndDebuffBaseClass bADBC = co.gameObject.AddComponent<BuffAndDebuffBaseClass>();
                    bADBC.SetData(true, true, 2, 0, -1, 0, .25f, .25f, StatusEffects.SizeUp);
                    co.gameObject.GetComponent<BallBehaviour>().CallForAllBuffsAndDebuffs();
                }
				
			}
			else if (powerType == PowerType.ShrinkBall)
			{
                if (!LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().ActivatedBalls.Contains(co.gameObject))
                {
                    BuffAndDebuffBaseClass bADBC = co.gameObject.AddComponent<BuffAndDebuffBaseClass>();
                    bADBC.SetData(true, true, 2, 0, -1, 0, -.25f, -.25f, StatusEffects.SizeDown);
                    co.gameObject.GetComponent<BallBehaviour>().CallForAllBuffsAndDebuffs();
                }
			}
			else if (powerType == PowerType.DamageIncrease)
			{
				BuffAndDebuffBaseClass bADBC = co.gameObject.AddComponent<BuffAndDebuffBaseClass>();
				bADBC.SetData(true, true, 0, 1, -1, 0, 0, 0, StatusEffects.DamageHighered);
				co.gameObject.GetComponent<BallBehaviour>().CallForAllBuffsAndDebuffs();
			}*/
            else if (powerType == PowerType.HealUs)
            {
                GameplayManager.instance.currentPlayerHealth += dataManager.Regen;
                if (GameplayManager.instance.currentPlayerHealth > GameplayManager.instance.maxPlayerHealthHealth)
                {
                    GameplayManager.instance.currentPlayerHealth = GameplayManager.instance.maxPlayerHealthHealth;
                }
                GameplayManager.instance.DisplayPlayerHealth();
            }
            //-----Obstacle-----//
            if (SpawnerBehaviour.instance != null)
            {
                if (SpawnerBehaviour.instance.spawnedObstacles.Contains(this.gameObject))
                {
                    SpawnerBehaviour.instance.RemoveFromList(this.gameObject);
                }
            }
            else if (StageManager.instance != null)
            {
                if (StageManager.instance.obstacles.Contains(this.gameObject))
                {
                    StageManager.instance.RemoveFromList(this.gameObject);
                }
                int temp1 = 0;
                for (int i = 0; i < StageManager.instance.obstacles.Count; i++)
                {
                    if (StageManager.instance.obstacles[i - temp1].GetComponent<ObstacleBehaviour>() != null)
                    {
                        if (StageManager.instance.obstacles[i - temp1].GetComponent<ObstacleBehaviour>().GetHealth() <= 0)
                        {
                            StageManager.instance.obstacles[i - temp1].GetComponent<ObstacleBehaviour>().DamageHealthPostWait();
                            temp1++;
                        }
                    }
                }
            }            
        }

        //-----Top Wall-----//
        if (co.gameObject.CompareTag("EndLine"))
        {
            
            if (StageManager.instance)
            {
                StageManager.instance.RemoveFromList(this.gameObject);
            }
            GameplayManager.instance.UpdateObstacleCleared(1);
        }
    }

    public void SetPowerBehavior(PowerType pt)
    {
        powerType = pt;
    }
}
