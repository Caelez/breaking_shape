﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;
//using UnityEditorInternal;
using System.Reflection;

public class TextRendererManager : MonoBehaviour
{
	private Renderer m_renderer;
	public string sortingLayer = "Default";
	public int sortingOrder = 0;

	void Start ()
	{
		m_renderer = GetComponent<Renderer>();
		if(m_renderer)
		{
			m_renderer.sortingLayerName = sortingLayer;
			m_renderer.sortingOrder = sortingOrder;
		}
	}
}
