﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapBehaviour : MapObjectsBase
{
	public TrapType trapType;
    
//	private Vector3 scale;

    private List<BallBehaviour> BallsThatGotHit = new List<BallBehaviour>();



    void Start()
    {
        //ResetGameObject();
    
        if (trapType == TrapType.BlackHole)
        {
            transform.name = "Devouring Void";
            m_health = 1;
            GetComponent<Collider2D>().isTrigger = true;
            GetComponent<Animator>().Play("BlackHoleEffectAnimation02");
            descriptionOfObject = "Destroy any ball that touches this trap. ";
        }
        else if (trapType == TrapType.GravityUp)
        {
            transform.name = "Gravity Intensify";
            m_health = 1;
            GetComponent<Collider2D>().isTrigger = true;
            GetComponent<Animator>().Play("GravityIntensifyAnimation");
            descriptionOfObject = "Increase gravity of ball that touches this trap.";
        }
        else if (trapType == TrapType.Weakening)
        {
            transform.name = "Weakening Trap";
            m_health = 1;
            GetComponent<Collider2D>().isTrigger = true;
            GetComponent<Animator>().Play("WeakeningAnimation");
            descriptionOfObject = "Decrease damage of ball that touches this trap. ";

        }
        else if (trapType == TrapType.Freezing)
        {
            transform.name = "Frozen Trap";
            m_health = 1;
            GetComponent<Collider2D>().isTrigger = true;
            GetComponent<Animator>().Play("FrozenTrapAnimation");
            descriptionOfObject = "Freezes a ball that touches this trap. Ball is frozen for 2 turns or hit it 10 times to free the ball.";
        }
        else if (trapType == TrapType.GravityPull)
        {
            transform.name = "Gravitation Trap";
            m_health = 1;
            GetComponent<Collider2D>().isTrigger = true;
            GetComponent<Animator>().Play("GravitationAnimation");
            descriptionOfObject = "Randomly boosts a ball in a random direction.";
        }
        else if (trapType == TrapType.Specter)
        {
            transform.name = "Specter Transform";
            m_health = 1;
            GetComponent<Collider2D>().isTrigger = true;
            GetComponent<Animator>().Play("SpecterAnimation");
            descriptionOfObject = "Renders any ball that touches this trap, inactive till the next shot.";
        }
        else if (trapType == TrapType.ElementChange)
        {
            transform.name = "Element OutBreak";
            m_health = 1;
            GetComponent<Collider2D>().isTrigger = true;
            GetComponent<Animator>().Play("ElementChangeAnimation");
            descriptionOfObject = "Changes current ball to a different element.";
        }
        else if (trapType == TrapType.ChronoStorm)
        {
            transform.name = "Chrono Storm";
            m_health = 25;
            GetComponent<Collider2D>().isTrigger = false;
            GetComponent<Animator>().Play("ChronoStormAnimation");
            descriptionOfObject = "Reloads stage again if it is hit 25 total times.";
        }
        else if (trapType == TrapType.HealEnemies)
        {
            transform.name = "Heal Enemies Trap";
            m_health = 1;
            GetComponent<Collider2D>().isTrigger = true;
            GetComponent<Animator>().Play("RejuvinationAnimation");
            descriptionOfObject = "Heals all enemies for 10% health.";
        }
        else if (trapType == TrapType.CooldownUp)
        {
            transform.name = "Mind Freeze";
            m_health = 1;
            GetComponent<Collider2D>().isTrigger = true;
            GetComponent<Animator>().Play("MindFreezeAnimation");
            descriptionOfObject = "Increases cooldown of Eion by 2";
        }
        else if (trapType == TrapType.PetrifiedTrap)
        {
            transform.name = "Petrified Trap";
            m_health = 1;
            GetComponent<Collider2D>().isTrigger = true;
            GetComponent<Animator>().Play("PetrifiedTrapAnimation");
            descriptionOfObject = "Increases ball damage by 2 but makes ball brittle. Brittle balls will survive only 10 hits.";
        }
        baseHealth = m_health;
    }



    // Check is there after effect


    void OnTriggerEnter2D(Collider2D co)
	{
		if(co.gameObject.CompareTag("Collector") && GameplayManager.instance.isTidalWave)
		{
			isPushedBelow = true;
			GetComponent<Collider2D>().enabled = false;
			GetComponent<SpriteRenderer>().enabled = false;
		}
        if (co.gameObject.CompareTag("Ball") && !BallsThatGotHit.Contains(co.gameObject.GetComponent<BallBehaviour>()))
        {
			m_health--;
			
			ActivateTrap(co.gameObject.GetComponent<BallBehaviour>(), co.gameObject);

            BallsThatGotHit.Add(co.gameObject.GetComponent<BallBehaviour>());
            
            if (m_health <= 0)
            {
                if (LauncherBehaviour.instance.currentActivateTraps > 0)
                {
                    LauncherBehaviour.instance.currentActivateTraps--;
                    return;
                }
                if (SpawnerBehaviour.instance)
                {
                    SpawnerBehaviour.instance.RemoveFromList(this.gameObject);
                }
                else if (StageManager.instance)
                {
                    StageManager.instance.RemoveFromList(this.gameObject);
                }
                Destroy(this.gameObject);
            }
        }
        if (co.gameObject.CompareTag("EndLine"))
        {

            if (StageManager.instance)
            {
                StageManager.instance.RemoveFromList(this.gameObject);
            }
            GameplayManager.instance.UpdateObstacleCleared(1);
        }
    }

    public void CollisionTraps(GameObject co)
    {
        m_health--;
        ActivateTrap(co.gameObject.GetComponent<BallBehaviour>(), co.gameObject);

        if (m_health <= 0)
        {
            if (LauncherBehaviour.instance.currentActivateTraps > 0)
            {
                LauncherBehaviour.instance.currentActivateTraps--;
                return;
            }
            if (SpawnerBehaviour.instance)
            {
                SpawnerBehaviour.instance.RemoveFromList(this.gameObject);
            }
            else if (StageManager.instance)
            {
                StageManager.instance.RemoveFromList(this.gameObject);
            }
            Destroy(this.gameObject);
        }
    }
    

   void ActivateTrap(BallBehaviour bB, GameObject co)
    {
        if (trapType == TrapType.GravityUp)
        {
			BuffAndDebuffBaseClass bADBC = co.gameObject.AddComponent<BuffAndDebuffBaseClass>();
			bADBC.SetData(false, false, 1, 0, -1, 0, 0, bB.GetComponent<Rigidbody2D>().mass, StatusEffects.DoubleGravity);
            bB.gameObject.GetComponent<BallBehaviour>().CallForAllBuffsAndDebuffs();
        }
        else if (trapType == TrapType.Weakening)
        {
			BuffAndDebuffBaseClass bADBC = co.gameObject.AddComponent<BuffAndDebuffBaseClass>();
			bADBC.SetData(false, true, 0, -1, -1, 0, 0, 0, StatusEffects.DamageLowered);
            bB.gameObject.GetComponent<BallBehaviour>().CallForAllBuffsAndDebuffs();
        }
        else if (trapType == TrapType.Freezing)
        {
			BuffAndDebuffBaseClass bADBC = co.gameObject.AddComponent<BuffAndDebuffBaseClass>();
			bADBC.SetData(false, false, 2, 0, 0, 5, 0, 0, StatusEffects.Frozen);
            bB.gameObject.GetComponent<BallBehaviour>().CallForAllBuffsAndDebuffs();
        }
        else if (trapType == TrapType.GravityPull)
        {
            bB.gameObject.AddComponent<BuffAndDebuffBaseClass>();
            bB.gameObject.GetComponent<BuffAndDebuffBaseClass>().ActivateBlackHole();
            Destroy(bB.gameObject.GetComponent<BuffAndDebuffBaseClass>());
        }
        else if (trapType == TrapType.Specter)
        {
			BuffAndDebuffBaseClass bADBC = co.gameObject.AddComponent<BuffAndDebuffBaseClass>();
			bADBC.SetData(false, false, 1, 0, -1, 0, 0, 0, StatusEffects.Spectered);
            bB.gameObject.GetComponent<BallBehaviour>().CallForAllBuffsAndDebuffs();
        }
        else if (trapType == TrapType.ElementChange)
        {
			BuffAndDebuffBaseClass bADBC = co.gameObject.AddComponent<BuffAndDebuffBaseClass>();
			bADBC.SetData(false, false, 1, 0, -1, 0, 0, 0, StatusEffects.ElementChange);
            bB.gameObject.GetComponent<BallBehaviour>().CallForAllBuffsAndDebuffs();
        }
        else if (trapType == TrapType.ChronoStorm)
        {
            if (m_health <= 0)
            {
                StageManager.instance.ResetStage(iD);
            }
        }
        else if (trapType == TrapType.HealEnemies)
        {
           // StartCoroutine(UIManager.instance.PlayAnimation(3));
            //StageManager.instance.HealEnemies(10);
        }
        else if (trapType == TrapType.BlackHole)
        {
            co.transform.position = transform.position + new Vector3(0f, -100f, 0f);
        }
        else if (trapType == TrapType.PetrifiedTrap)
        {
			BuffAndDebuffBaseClass bADBC = co.gameObject.AddComponent<BuffAndDebuffBaseClass>();
			bADBC.SetData(false, false, 2, 2, -1, 10, 0, 0, StatusEffects.Petrified);
            bB.gameObject.GetComponent<BallBehaviour>().CallForAllBuffsAndDebuffs();
        }
    }

    public void SetTrapType (TrapType tt)
    {
        trapType = tt;
    }
}
