﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShotScript : MonoBehaviour
{
    public TypeOfEnemyShot typeOfEnemyShot;
    float StartPointX = 0;
    float StartPointY = 0;
    float ControlPointX = 20;
    float ControlPointY = 50;
    float EndPointX = 50;
    float EndPointY = 0;
    float CurveX;
    float CurveY;
    float BezierTime = 0;
    Transform mySphere;

    void Update()
    {
        BezierTime = BezierTime + Time.deltaTime;

        if (BezierTime > 1)
        {
            GameplayManager.instance.EnemyBulletDamage();
            AudioManager.instance.PlayEnemyHitEffect();

            CameraShakeWhenDamaged.instance.CameraShake();
            gameObject.SetActive(false);
        }

        if (typeOfEnemyShot == TypeOfEnemyShot.Curved)
        {
            CurveX = (((1 - BezierTime) * (1 - BezierTime)) * StartPointX) + (2 * BezierTime * (1 - BezierTime) * ControlPointX) + ((BezierTime * BezierTime) * EndPointX);
            CurveY = (((1 - BezierTime) * (1 - BezierTime)) * StartPointY) + (2 * BezierTime * (1 - BezierTime) * ControlPointY) + ((BezierTime * BezierTime) * EndPointY);
//            Vector3 temp = transform.position - new Vector3(CurveX, CurveY,0f);
            transform.GetChild(0).GetComponent<UIParticleSystem>().SetEmmisionAngle(new Vector3(CurveX, CurveY, 0),Quaternion.LookRotation(transform.up, new Vector3(CurveX, CurveY, 0f)));
            transform.position = new Vector3(CurveX, CurveY, 0);
            transform.GetChild(0).GetChild(0).transform.position = transform.position;

        }
        else if (typeOfEnemyShot == TypeOfEnemyShot.Straight)
        {
            transform.position = Vector3.Lerp(new Vector3(StartPointX,StartPointY,0), new Vector3(EndPointX, EndPointY, 0), BezierTime);
        }        
    }

    public void SetEndPosition()
    {
        gameObject.SetActive(true);
        StartPointX = transform.position.x;
        StartPointY = transform.position.y;
        EndPointX = LauncherBehaviour.instance.transform.position.x + Random.Range(-1f, 1f);
        EndPointY = LauncherBehaviour.instance.transform.position.y + 0.5f;
        ControlPointX = Random.Range(-2, 2);
        ControlPointY = Random.Range(-2, 2);
        BezierTime = 0;
    }   
}
