﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManagerScript : MonoBehaviour {

    #region Instance
    public static EnemyManagerScript instance;

    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    #endregion

    private bool enemyDoneShooting = false;
    private bool isCoroutineRunning = false;
    

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public bool GetEnemyDoneShooting()
    {
        return enemyDoneShooting;
    }

    public bool GetIsCoroutineRunning()
    {
        return isCoroutineRunning;
    }

    public void SetEnemyDoneShooting(bool temp)
    {
        enemyDoneShooting = temp;
    }

    public void SetIsCoroutineRunning(bool temp)
    {
        isCoroutineRunning = temp;
    }

    public IEnumerator InitiateEnemyShot()
    {
        if (StageManager.instance != null)
        {
            do
            {
                StageManager.instance.obstacles.Remove(null);
            } while (StageManager.instance.obstacles.Contains(null));
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(.05f);
            foreach (GameObject g in StageManager.instance.obstacles)
            {
                if (g.GetComponent<ObstacleBehaviour>() != null)
                {
                    if (g.transform.position.y > -8f)
                    {
                        g.GetComponent<ObstacleBehaviour>().ShootPlayer();
                        yield return new WaitForSeconds(.05f);
                    }
                }
            }
        }
        enemyDoneShooting = true;
    }
}
