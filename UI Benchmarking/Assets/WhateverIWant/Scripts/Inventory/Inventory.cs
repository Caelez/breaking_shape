﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Singleton
    public static Inventory instance;
    private void Awake()
    {
		if (!instance)
		{
			instance = this;
		}
		else
		{
			if (instance != this)
			{
				Destroy(gameObject);
			}
		}
    }
    #endregion

    public delegate void OnItemChange();
    public OnItemChange onItemChangeCallBack;

    public Item item;

    public bool addItem = false;

    [SerializeField] private int space = 25;
    public int Space
    {
        get
        {
            return space;
        }
        set
        {
            space = value;
        }
    }

    public void Update()
    {
        if(addItem)
        {
            if (!AddItem(item))
            {
                Debug.Log("Unable to Add item");
            }
            else
            {
                Debug.Log("Added Item");
            }
            addItem = false;
        }
    }

    public List<Item> items = new List<Item>();

    public bool AddItem(Item item)
    {
        if(items.Count >= space)
        {
            Debug.Log("Not enough space to store item");
            return false;
        }

        items.Add(item);

        if(onItemChangeCallBack != null)
        {
            Debug.Log("Call Back");
            onItemChangeCallBack.Invoke();
        }

        return true;
    }

    public void RemoveItem(Item item)
    {
        items.Remove(item);

        if (onItemChangeCallBack != null)
        {
            onItemChangeCallBack.Invoke();
        }
    }
}
