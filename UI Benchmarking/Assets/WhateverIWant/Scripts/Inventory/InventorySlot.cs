﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public Image itemSprite;

    Item item;

    public void AddItem(Item newItem)
    {
        item = newItem;

        itemSprite.sprite = item.sprite;
        itemSprite.enabled = true;
    }

    public void ClearSlot()
    {
        item = null;

        itemSprite.sprite = null;
        itemSprite.enabled = false;
    }

    public void UseItem()
    {
        if(item != null)
        {
            item.Use();
			Inventory.instance.RemoveItem(item);
        }
    }
}
