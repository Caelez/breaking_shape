﻿// This script is to store public enums

public enum SkillType
{
	None,
	Ray,
	RayHit,
	FullScreenAOE,
	TargetedAOE,
	ActivePassive,
	Max
}

public enum TargetSkill
{
	None,
	Single,
	Multiple,
	Max
}

public enum TypeOfSkillBuff{
	None,
	Damage,
	ElementDamageUp,
	ElementChange,
	HigherCritChance,
	HigherCritDamage,
	Max,
}

public enum ElementType
{
	None,
	Fire,
	Water,
	Nature,
	Thunder,
	Earth,
	Total_ElementType
}

public enum ResponseType
{
    None,
    Success,
    Failed_Offline,
    Failed_Time_Out,
    Failed_Technical_Issue,
    Failed_Exception,
    Failed_Unauthorised,
    Failed_Outdated,
    Failed_Error,
}

public enum MaterialType
{
    Ore,
    Silver,
    Gold,
    Bug,
    Total_MaterialType
}

public enum PowerType
{
    HealUs = 0,
    ExtraBall1 ,
    ExtraBall2,
    ExtraBall5,
    EnlargeBall,
    ShrinkBall,
    DamageIncrease,
    FlameAOE,
    IceAOE,
    NatureAOE,
    EarthAOE,
    Total_PowerType
};

public enum StatusEffects
{
    None = 0,
    Frozen,
    Burn,
    ElementChange,
    Earth,
    DamageLowered,
    DamageHighered,
    DoubleGravity,
    LoweredGravity,
    Spectered,
    SizeUp,
    SizeDown,
    Petrified,
    Max
}

public enum TrapType
{
    BlackHole = 0,
    GravityUp,
    Weakening,
    Freezing,
    GravityPull,
    Specter,
    ElementChange,
    ChronoStorm,
    HealEnemies,
    CooldownUp,
    PetrifiedTrap,
    Total_TrapType
};

public enum RequirementsTypes
{
    Score,
    Combos,
    NumberOfRounds,
    DamageInOneRound,
    TotalDamage,
    DestroyedObstaclesInOneRound,
    ActivatePowerUp,
    AvoidTraps,
    SkillUsage,
    BallClear,
    ClearOneWaveUnderXTurn,
    UseLessEions,
    UseTeamTypeEions,
    Clear,
    MaxRequiredTraps
};


public enum TypeOfShot
{
    Straight,
    Split,
    Shotgun,
    Impact,
    Explosive,


    Penetration,
    Gatling
}

public enum TypeOfEnemyShot
{
    Straight,
    Curved,
    None
}