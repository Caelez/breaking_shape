﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
	[System.Serializable]
	public class Pool
	{
		public string tag;
		public GameObject prefab;
		public int size;
	}

	#region Singleton

	public static ObjectPooler instance;

	private void Awake()
	{
		instance = this;
	}

	#endregion

	public List<Pool> pools;

	public Dictionary<string, Queue<GameObject>> poolDictionary;
    public GameObject hitEffect;
    public GameObject enemyShots;
    public List<GameObject> enemyShotPool = new List<GameObject>();
    GameObject tempObject;
    public GameObject brokenObject;

    // Use this for initialization
    void Start ()
	{
		poolDictionary = new Dictionary<string, Queue<GameObject>>();

		foreach(Pool pool in pools)
		{
			Queue<GameObject> objectPool = new Queue<GameObject>();

			for(int i = 0; i < pool.size; i++)
			{
				//Instantiate();
				GameObject go = Instantiate(pool.prefab, gameObject.transform);
				go.SetActive(false);
				objectPool.Enqueue(go);
			}

			poolDictionary.Add(pool.tag, objectPool);
		}
        for (int i = 0; i < 100; i++)
        {
            tempObject = Instantiate(brokenObject);
            tempObject.SetActive(false);
            tempObject.transform.SetParent(transform.GetChild(2));
        }
	}

	public GameObject SpawnFromPool (string tag, Vector3 position, Quaternion rotation)
	{
		if(!poolDictionary.ContainsKey(tag))
		{
			Debug.LogWarning("Pool with tag " + tag + " doesn't exist");
			return null;
		}

		GameObject objectToSpawn = poolDictionary[tag].Dequeue();

		objectToSpawn.SetActive(true);
		objectToSpawn.transform.position = position;
		objectToSpawn.transform.rotation = rotation;

		poolDictionary[tag].Enqueue(objectToSpawn);

		return objectToSpawn;
	}

    public GameObject PoolForHitObjects()
    {
        bool gotObject = false;
        foreach (Transform gO in transform.GetChild(1))
        {
            if (gO.GetComponent<ColorHitEffect>().GetIfAnimationDone())
            {
                tempObject = gO.gameObject;

                gotObject = true;
            }
        }
        if (!gotObject)
        { 
            tempObject = Instantiate(hitEffect);
        }
        return tempObject;
    }

    public GameObject PoolForBrokenObjects()
    {
        for (int i = 0; i < 100; i ++ )
        {
            if (!transform.GetChild(2).GetChild(i).gameObject.activeSelf)
            {
                tempObject = transform.GetChild(2).GetChild(i).gameObject;
            }
        }
        tempObject.transform.SetParent(transform.GetChild(2));
        tempObject.SetActive(true);
        tempObject.GetComponent<SlowFadeOutScript>().ResetObject();
        return tempObject;
    }

    public void PutHitObjectsInPool(GameObject g)
    {
        g.transform.SetParent(transform.GetChild(1));
    }    

    public void CreateEnemyShots(Vector3 temp)
    {
        bool tempBool = false;
        GameObject g = this.gameObject;
        foreach (GameObject g1 in ObjectPooler.instance.enemyShotPool)
        {
            if (!g1.activeSelf)
            {
                g = g1;
                tempBool = true;
                break;
            }
        }
        if (tempBool == false)
        {
           g = Instantiate(ObjectPooler.instance.enemyShots, this.transform.position, Quaternion.identity);
        }
        g.transform.SetParent(UIManager.instance.enemyShots.transform);
        g.transform.position = temp;
        for (int i = 0; i < g.GetComponent<TrailRenderer>().positionCount; i++)
        {
            g.GetComponent<TrailRenderer>().SetPosition(i, temp);
        }
        g.transform.localScale = new Vector3(.5f, .5f, 1);
        g.GetComponent<EnemyShotScript>().SetEndPosition();
        ObjectPooler.instance.enemyShotPool.Add(g);
    }
}
