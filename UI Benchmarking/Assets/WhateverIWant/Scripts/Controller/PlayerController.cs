﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
	//public Image selectedEionSprite;
	//[SerializeField] private GameObject eionSelected;
	//private bool b_isSelected;
	private LauncherBehaviour launcher;
	private GameplayManager gameplayManager;
   
	private bool b_shootable;


    private MapObjectsBase mOBDescription;
    private RaycastHit2D hit;
    private Vector3 ray;

	// Use this for initialization
	void Start ()
	{
		launcher = LauncherBehaviour.instance;
		gameplayManager = GameplayManager.instance;
	}

    // Update is called once per frame
    void Update()
    {        
        if (launcher.GetIsReturn())
        {
            if (!gameplayManager.GetIsEnd() && Input.touchCount > 0 && !IsPointerOverUI())
            {
                if (Input.GetTouch(0).phase != TouchPhase.Ended)
                {
                    b_shootable = true;
                    launcher.FaceTouchPosition();
                    IndicatorScript.instance.SetIndicatorPosition();
                }

                if (Input.GetTouch(0).phase == TouchPhase.Ended && b_shootable)
                {
                    b_shootable = false;
                    launcher.ShootBall();
                    IndicatorScript.instance.DisableIndicator();
                }
            }
            if (!gameplayManager.GetIsEnd() && !IsPointerOverUI() && Input.GetMouseButton(0))
            {
                if (Input.GetMouseButton(0))
                {
                    b_shootable = true;
                    launcher.FaceTouchPosition();
                    IndicatorScript.instance.SetIndicatorPosition();
                }
            }
            if (Input.GetMouseButtonUp(0) && b_shootable)
            {
                b_shootable = false;
                launcher.ShootBall();
                IndicatorScript.instance.DisableIndicator();
            }
        }
		else
		{
            if (Input.GetMouseButtonDown(0))
            {
                if (launcher.GetComponent<TypeOfShotScript>().splitOnCommand)
                {
                    if (!launcher.GetComponent<TypeOfShotScript>().released)
                    {
                        launcher.GetComponent<TypeOfShotScript>().released = true;
                        foreach (GameObject g in launcher.balls)
                        {
                            if (g.activeSelf != false)
                            {
                                for (int i = 0; i < 5; i++)
                                {
                                    GameObject newBall = Instantiate(launcher.ball);
                                    newBall.SetActive(true);
                                    newBall.transform.position = this.transform.position;
                                    newBall.transform.rotation = Quaternion.Euler(0,0,Random.Range(0,360f));
                                    newBall.name = "NewBall";
                                    newBall.GetComponent<Rigidbody2D>().AddForce(newBall.transform.up * 50, ForceMode2D.Impulse);

                                    LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().AddToTempList(newBall);
                                }
                            }
                        }
                    }
                }
            }
		}
	}

	private bool IsPointerOverUI()
	{
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		return results.Count > 0;
	}
}
