﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearRowBehaviour : MonoBehaviour
{
	public float yAxis = 0.0f;

	public float duration = 0.5f;
	private float timer = 0.0f;

	private void Update()
	{
		if(timer >= duration)
		{
			Destroy(this.gameObject);
		}
		else
		{
			timer += Time.deltaTime;
		}
	}

	void OnTriggerEnter2D(Collider2D co)
	{
		if(co.gameObject.CompareTag("Obstacle"))
		{
            co.gameObject.GetComponent<ObstacleBehaviour>().DamageHealth(3);
		}
	}
}
