﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBehaviour : MonoBehaviour
{
	public GameObject boomEffect;

	void OnCollisionEnter2D(Collision2D co)
	{
		if(co.gameObject.CompareTag("Obstacle"))
		{
//			Debug.Log("Boom");
//			Destroy(co.gameObject);
			Handheld.Vibrate();
			ObjectPooler.instance.SpawnFromPool("BoomEffect", transform.position, Quaternion.identity);
			//GameObject temp = Instantiate(boomEffect, transform.position, Quaternion.identity);
			//transform.SetParent(temp.transform);
			//this.gameObject.SetActive(false);
			Destroy(this.gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D co)
	{
		if(co.gameObject.CompareTag("Collector"))
		{
			Debug.Log("Dropped");
			Destroy(this.gameObject);
		}
	}
}
