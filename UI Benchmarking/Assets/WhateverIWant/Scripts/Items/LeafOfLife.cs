﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Size
{
	None,
	Small,
	Medium,
	Large,
	total_Size
}

[CreateAssetMenu(menuName = "Item/LeafOfLife")]
public class LeafOfLife : Item
{
	public int staminaValue;
	public Size size;

	public override void Use()
	{
		base.Use();
		//DataManager.userData.currentStamina += staminaValue;
		DataManager.UploadData();
	}
}
