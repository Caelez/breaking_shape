﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class CameraShakeWhenDamaged : MonoBehaviour {

    #region
    public static CameraShakeWhenDamaged instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        
    }
    #endregion
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CameraShake()
    {
        CameraShaker.Instance.ShakeOnce(2f, 2f, .1f, .15f);
    }
}
