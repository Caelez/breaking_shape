﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TrapSpriteType
{
    public TrapType type;
    public Sprite sprite;
}

public class TrapSpriteManager : MonoBehaviour
{
	public List<TrapSpriteType> sprites = new List<TrapSpriteType>();

	// Use this for initialization
	void Start ()
	{
		
	}

	public void UseSprite(Sprite sprite)
	{
		GetComponent<SpriteRenderer>().sprite = sprite;
	}

	public void RandomSprite()
	{
		if(sprites.Count != 0)
		{
			GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Count)].sprite;
		}
	}
}
