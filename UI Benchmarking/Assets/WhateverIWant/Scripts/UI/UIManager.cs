﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Singleton
    public static UIManager instance;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    #endregion

    public Image LifeBar;
    public GameObject inventoryUI;
    public GameObject settingUI;
	public GameObject cutInUI;
    public GameObject tutorialUI;
    public GameObject settingButton;

    //public GameObject AOEEffects;
    //public GameObject ParasiteObject;

    public GameObject description;
    public GameObject enemyShots;


    public List<Animator> listAnimator = new List<Animator>();
    //Animator anim;

    void Start()
    {
        //anim = AOEEffects.GetComponent<Animator>();
        //if (DataManager.userData.isFirstTime)
        //{
        //    if (settingButton != null)
        //    {
        //        settingButton.SetActive(false);
        //    }
            
        //}
        //else
        //{
        //    if (settingButton != null)
        //    {
        //        settingButton.SetActive(true);
        //    }
        //}

		if (settingButton != null)
		{
			settingButton.SetActive(true);
		}
	}

    private float beforePauseTimeScale;

    public void ToggleInvnetory()
    {
        inventoryUI.SetActive(!inventoryUI.activeSelf);
    }

    public void ToggleSetting()
    {
        settingUI.SetActive(!settingUI.activeSelf);
		if(Time.timeScale != 0.0f)
		{
			beforePauseTimeScale = Time.timeScale;
			Time.timeScale = 0.0f;
		}
		else
		{
			Time.timeScale = beforePauseTimeScale;
		}

		AudioManager.instance.UpdateBallHitSFXText();
	}

	public void DisplayCutIn(RuntimeAnimatorController animatorContorller)
	{
		cutInUI.GetComponent<Animator>().runtimeAnimatorController = animatorContorller;
		cutInUI.SetActive(true);
	}

    public void DisplayTutorialUI()
    {
        if (!tutorialUI)
        {
            return;
        }
        ToggleTutorial();
    }

    public void ToggleTutorial()
    {
        tutorialUI.SetActive(!tutorialUI.activeSelf);
    }

  /*  public IEnumerator PlayAnimation(int number)
    {
        if (number == 0)
        {
            anim.Play("IncinerationFullScreenAoE", -1, 0f);
        }
        else if (number == 1)
        {
            anim.Play("SandStorm", -1, 0f);
        }
        else if (number == 2)
        {
            anim.Play("SnowStorm", -1, 0f);
        }
        else if (number == 3)
        {
            anim.Play("RejuvinationStorm", -1, 0f);
        }
        yield return new WaitForSeconds(0);
    }

    public IEnumerator PlayParasiteAnimation()
    {
        foreach (GameObject g in StageManager.instance.obstacles)
        {
            if (g.GetComponent<ObstacleBehaviour>() != null)
            {
                GameObject temp = Instantiate(ParasiteObject, g.transform.position, Quaternion.identity);
                temp.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(4));
                temp.GetComponent<Animator>().Play("ParasiteEffect",-1,0f);
                StartCoroutine(temp.GetComponent<DestroyObject>().DestroyAfterAnimation());
            }
        }
        yield return new WaitForSeconds(0);
    }*/
}