﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsShowForObject : MonoBehaviour {
    
    public Vector2 leftAndRightWall; 
	

    public void SetMapObjectBase(MapObjectsBase temp = null)
    {
        if (temp != null)
        {
            gameObject.SetActive(true);
            if (temp.transform.position.y < 0)
            {
                transform.position = temp.gameObject.transform.position + (Vector3.up * 3f);
                if (transform.localPosition.x > leftAndRightWall[1])
                {
                    transform.localPosition = new Vector3(leftAndRightWall[1], transform.localPosition.y, transform.position.z);
                }
                else if (transform.localPosition.x < leftAndRightWall[0])
                {
                    transform.localPosition = new Vector3(leftAndRightWall[0], transform.localPosition.y, transform.position.z);
                }
            }
            else
            {
                transform.position = temp.gameObject.transform.position + (Vector3.down * 3f);
                if (transform.position.x > leftAndRightWall[1])
                {
                    
                    transform.localPosition = new Vector3(leftAndRightWall[1], transform.localPosition.y, transform.position.z);
                }
                else if (transform.localPosition.x < leftAndRightWall[0])
                {
                    transform.localPosition = new Vector3(leftAndRightWall[0], transform.localPosition.y, transform.position.z);
                }
            }
            transform.GetChild(0).GetComponent<Image>().sprite = temp.GetComponent<SpriteRenderer>().sprite;
            transform.GetChild(1).GetComponent<Text>().text = temp.transform.name;
            if (temp.GetComponent<ObstacleBehaviour>() != null)
            {
                transform.GetChild(2).GetComponent<Text>().text = "Health : " + temp.GetHealth() + " / " + temp.GetBaseHP();
            }
            else
            {
                transform.GetChild(2).GetComponent<Text>().text = "Activation : " + temp.GetHealth() + " / " + temp.GetBaseHP();
            }
            
            transform.GetChild(3).GetChild(0).GetComponent<Text>().text = temp.GetDescription();
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
