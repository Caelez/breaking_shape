﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITransition : MonoBehaviour
{
	public GameObject interactButton;
	public GameObject selectedUI;
	[Range(0.1f, 1.0f)]
	public float speed = 0.5f;
	[SerializeField] private Vector2 transitionValue = new Vector2();
	[SerializeField] private float zRotation = 0.0f;
	[SerializeField] private bool isHide = true;

	IEnumerator Slide(Vector2 pos)
	{
		while (selectedUI.GetComponent<RectTransform>().anchoredPosition != pos)
		{
			Vector2 deltaPos0 = Vector2.Lerp(selectedUI.GetComponent<RectTransform>().anchoredPosition, pos, speed);
			selectedUI.GetComponent<RectTransform>().anchoredPosition = deltaPos0;

			if(Vector2.Distance(selectedUI.GetComponent<RectTransform>().anchoredPosition, pos) <= 0.5f)
			{
				selectedUI.GetComponent<RectTransform>().anchoredPosition = pos;
			}

			yield return null;
		}

		if(interactButton)
		{
			interactButton.GetComponent<Button>().interactable = true;
		}
	}

	IEnumerator Appear()
	{
		if(selectedUI.GetComponent<Image>().color.a < 1.0f)
		{
			Color color = selectedUI.GetComponent<Image>().color;
			color.a = 1.0f;
			selectedUI.GetComponent<Image>().color = color;
		}

		if(interactButton)
		{
			interactButton.GetComponent<Button>().interactable = true;
		}

		yield return null;
	}

	IEnumerator FadeIn(float value)
	{
		while(selectedUI.GetComponent<Image>().color.a < value)
		{
			Color color = selectedUI.GetComponent<Image>().color;
			color.a = Mathf.Lerp(color.a, value, speed);

			if(color.a >= 1.0f)
			{
				color.a = 1.0f;
			}

			selectedUI.GetComponent<Image>().color = color;

			yield return null;
		}

		if(interactButton)
		{
			interactButton.GetComponent<Button>().interactable = true;
		}

		yield return null;
	}

	IEnumerator FadeOut(float value)
	{
		while(selectedUI.GetComponent<Image>().color.a > value)
		{
			Color color = selectedUI.GetComponent<Image>().color;
			color.a = Mathf.Lerp(color.a, value, speed);

			if(color.a <= 0.0f)
			{
				color.a = 0.0f;
			}

			selectedUI.GetComponent<Image>().color = color;

			yield return null;
		}

		if(interactButton)
		{
			interactButton.GetComponent<Button>().interactable = true;
		}

		yield return null;
	}

	public void ToggleUI()
	{
		isHide = !isHide;
		if(interactButton)
		{
			interactButton.GetComponent<Button>().interactable = false;
		}

		if(isHide)
		{
			StartCoroutine(Slide(transitionValue));
		}
		else
		{
			StartCoroutine(Slide(Vector2.zero));
		}
	}

	public void RotateOnClick(GameObject go)
	{
		float tempZ = go.transform.rotation.eulerAngles.z;
		go.transform.rotation = Quaternion.Euler(0.0f, 0.0f, tempZ + zRotation);
	}

	public void AppearOnChange()
	{
		StartCoroutine(Appear());
	}

	public void FadeOutOnStation()
	{
		StartCoroutine(FadeOut(0.0f));
	}
}
