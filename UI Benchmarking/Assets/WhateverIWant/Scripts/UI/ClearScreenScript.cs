﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearScreenScript : MonoBehaviour {

    public GameObject Masks;
    public GameObject ClearText;
    public GameObject EndPanel;

    public List<GameObject> starsAwaitingAnimation;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public IEnumerator PlayAnimation()
    {
        Masks.SetActive(true);
        ClearText.SetActive(true);
        ClearText.GetComponent<Animator>().Play("ClearAnimation");
        do
        {
            yield return new WaitForSeconds(ClearText.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0).Length);
        } while (!ClearText.GetComponent<BoolCheck>().check);

        EndPanel.SetActive(true);
        EndPanel.GetComponent<Animator>().Play("AnimationForEndPanel");
        do
        {
            yield return new WaitForSeconds(EndPanel.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0).Length);
        } while (!EndPanel.GetComponent<BoolCheck>().check);
        
        foreach ( GameObject g in starsAwaitingAnimation)
        {
            g.SetActive(true);
            g.GetComponent<Animator>().Play("StarEnlargeAnimation");
            do
            {
                yield return new WaitForSeconds(.05f);
            } while (!g.GetComponent<BoolCheck>().check);
        }
        RewardManager.instance.endResults = true;
    }
}
