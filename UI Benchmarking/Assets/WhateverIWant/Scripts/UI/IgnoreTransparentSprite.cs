﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IgnoreTransparentSprite : MonoBehaviour
{
	void Start ()
	{
		Image m_image = GetComponent<Image>();
		m_image.alphaHitTestMinimumThreshold = 0.5f;
	}
}