﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpriteType
{
    public string name;
    public string group;
    public ElementType type;
    public Sprite sprite;
}

public class SpriteManager : MonoBehaviour
{
	public List<SpriteType> sprites = new List<SpriteType>();

	// Use this for initialization
	void Start ()
	{
		
	}

	public void UseSprite(Sprite sprite)
	{
		GetComponent<SpriteRenderer>().sprite = sprite;
	}

	public void RandomSprite()
	{
		if(sprites.Count != 0)
		{
			GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Count)].sprite;
		}
	}
}
