﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
    private int statusDuration;
    private PlaySceneDataManager dataManager;

    private int damage = 3;
    [SerializeField] private float minSize; //= 0.35f;
    [SerializeField] private float maxSize; //= 0.4f;
	[SerializeField] private float counterDuration = 5.0f;

    [SerializeField] private float sizeOffset = 0.05f; 
    [SerializeField] private float sizeWidth = 0.3f; 

    private float baseDamage; //AOE base damage
	private float critRateUP;
	private float critDamageUp;
	private float counterTimer = 0.0f;
	private float bouncinessUp;
	private SpriteRenderer spriteRenderer;
	private Animator animator;
	private Rigidbody2D rb2d;
	private List<GameObject> obstacles = new List<GameObject>();

	private ObjectPooler objectPooler;

    private Vector3 baseSize;
    private string animationToBePlayed;
    private float scale;
    private float baseGravity;
//    private float currentGravity;
    private int baseAdditionalDamage;
    private int currentAdditionalDamage;
    private int startLayer;
    private int currentLayer;
    private int ballDurability;
    private bool readyToFire;
    private float previousY;
    private float updateTimer;

	void Awake()
	{
        dataManager = PlaySceneDataManager.instance;
		spriteRenderer = GetComponent<SpriteRenderer>();
		animator = GetComponent<Animator>();
		rb2d = GetComponent<Rigidbody2D>();
		baseSize = transform.localScale;
		baseDamage = 1;
    }
		
	void Start()
	{
        dataManager.UpdateUpgradeValue();
		objectPooler = ObjectPooler.instance;
		startLayer = 9;
        gameObject.layer = startLayer;
        ballDurability = ReturnDurability();
        damage = dataManager.Damage;
    }

	void Update()
	{
        if (updateTimer < 5f)
        {
            updateTimer += Time.deltaTime;
        }
        if (Mathf.Abs(previousY - transform.position.y) > 0.2f)
        {
            previousY = transform.position.y;
            updateTimer = 0;
        }
        if (transform.position.y < -8f || updateTimer > 5)
        {
            rb2d.velocity = Vector3.zero;
            counterTimer = 0.0f;
            obstacles.Clear();

            objectPooler.SpawnFromPool("BallCollectedEffect", new Vector3(transform.position.x, -8f, 0.0f), Quaternion.identity);
            readyToFire = true;
            gameObject.SetActive(false);
            GetComponent<Collider2D>().enabled = true;

            LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().CheckIfAllBallsHome();
            IfActivatedBallHitFloor();
            ballDurability = ReturnDurability();
        }
    }

    public void ResetObject()
    {
        rb2d.velocity = Vector3.zero;
        counterTimer = 0.0f;
        obstacles.Clear();
        
        readyToFire = true;
        gameObject.SetActive(false);
        GetComponent<Collider2D>().enabled = true;

        LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().CheckIfAllBallsHome();
        IfActivatedBallHitFloor();
        ballDurability = ReturnDurability();
    }

    public float GetMinSize()
	{
		return minSize;
	}

	public float GetMaxSize()
	{
		return maxSize;
	}

    public float GetBaseDamage()
    {
        return baseDamage;
    }

    public float GetAdditionalDamage()
    {
        return currentAdditionalDamage;
    }

    public float GetCritDamage()
    {
        return critDamageUp;
    }

    public float GetCritRate()
    {
        return critRateUP;
    }

    public bool GetReadyToFire()
    {
        return readyToFire;
    }

    public void SetReadyToFire(bool temp)
    {
        readyToFire = temp;
        updateTimer = 0;
    }

    public void ModifyBallStatsUp(float[] temp)
	{
		baseDamage = temp[1];
		baseGravity = temp[5];
		critDamageUp = temp[4];
		critRateUP = temp[3];
		bouncinessUp = temp[6];
	}

	public void ModifyBallPassives(float damageUP, float newGravity, float newSize)
	{
		baseSize = new Vector3(transform.localScale.x + (transform.localScale.x*newSize), transform.localScale.y + (transform.localScale.y * newSize),
			transform.localScale.z + (transform.localScale.z * newSize));
		baseDamage *= damageUP / 100f;
		baseGravity -= newGravity;
		transform.localScale = baseSize;
	}

    public void CallForAllBuffsAndDebuffs()
    {
        BuffAndDebuffBaseClass[] tempHolder = GetComponents<BuffAndDebuffBaseClass>();
        ResetData();
		foreach (BuffAndDebuffBaseClass BAD in tempHolder)
        {
            if (!BAD.beingDestroyed)
            {
                currentAdditionalDamage += BAD.GetDamage();
              

                
                scale += BAD.GetIncrease();
            }
		}
        
        if (scale + baseSize.x > baseSize.x + sizeOffset)
        {
            scale = baseSize.x + sizeOffset;
        }if (baseSize.x + scale < baseSize.x - sizeOffset)
        {
            scale = baseSize.x - sizeOffset;
        }
        if (scale != 0)
        {
            transform.localScale =  new Vector3(scale, scale, 0f);
        }
        else
        {
            transform.localScale = baseSize;
        }
        if (transform.localScale.x <= baseSize.x - sizeOffset)
        {			
			GetComponent<TrailRenderer>().startWidth = sizeWidth - sizeOffset;
			GetComponent<TrailRenderer>().startColor = new Color(0.0f, 1.0f, 1.0f, 1.0f);
			GetComponent<TrailRenderer>().endColor = new Color(0.0f, 1.0f, 1.0f, 0.0f);
		}
        else if (transform.localScale.x >= baseSize.x + sizeOffset)
        {
            GetComponent<TrailRenderer>().startWidth = sizeWidth + sizeOffset;
			GetComponent<TrailRenderer>().startColor = new Color(1f, 0f, 1f, 1.0f);
            GetComponent<TrailRenderer>().endColor = new Color(1f, 0f, 1f, 0.0f);
        }
        else
        {
			GetComponent<TrailRenderer>().startWidth = sizeWidth;
			GetComponent<TrailRenderer>().startColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
			GetComponent<TrailRenderer>().endColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
		}
        gameObject.layer = currentLayer;
		
    }

    public void ResetData()
    {
        currentAdditionalDamage = baseAdditionalDamage;
//        currentGravity = baseGravity;
        currentLayer = startLayer;
        scale = 0;
    }
    
    

    public void ChangeSprite(Sprite sprite)
	{
		animator.runtimeAnimatorController = null;
		spriteRenderer.sprite = sprite;
	}

	public void ChangeAnimation(string controller)
	{
        animationToBePlayed = controller;
       
    }

    public void LaunchBallsUp()
    {
        Vector3 dir = transform.position - (transform.position+ new Vector3((Random.Range(Vector3.left.x * 15f, Vector3.right.x * 15f)), -Vector3.up.y * 10f, 0f));

        rb2d.AddForce(dir * .5f, ForceMode2D.Impulse);
    }

    void OnCollisionEnter2D(Collision2D co)
	{
        AudioManager.instance.PlayBallHitSFX();
        if (co.gameObject.CompareTag("Obstacle"))
		{
            IfActivatedBallHitObstacle();
            ObstacleBehaviour obst = co.gameObject.GetComponent<ObstacleBehaviour>();
            obst.CallFlash();
            GameObject g1 = objectPooler.PoolForHitObjects();
            g1.transform.position = co.GetContact(0).point;
            objectPooler.PutHitObjectsInPool(g1);
            g1.GetComponent<ColorHitEffect>().GradientChange();
            obst.DamageHealth(damage);
			GameplayManager.instance.UpdateScore(damage);

			Vector3 dir = co.gameObject.transform.position - transform.position;
			dir = -dir.normalized;

			rb2d.AddForce(dir.normalized * 1.0f * bouncinessUp, ForceMode2D.Impulse);

            BuffAndDebuffBaseClass[] tempHolder = GetComponents<BuffAndDebuffBaseClass>();
          
            foreach (BuffAndDebuffBaseClass BAD in tempHolder)
            {
                if (!BAD.beingDestroyed)
                {
                    if (BAD.GetStatusEffect() == StatusEffects.Petrified)
                    {
                        BAD.DamageThis();
                        if (BAD.HowManyHitsMore() <=0)
                        {
                            LauncherBehaviour.instance.RemoveBall(this.gameObject);
                            BallPool.instance.Despawn(gameObject);
                        }
                    }
                }
            }

            ballDurability--;
            if (ballDurability <= 0)
            {
                objectPooler.SpawnFromPool("BallCollectedEffect", new Vector3(transform.position.x, transform.position.y, 0.0f), Quaternion.identity);
                gameObject.SetActive(false);
                if (LauncherBehaviour.instance.GetComponent<TypeOfShotScript> ()!= null)
                {
                    if (LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().TempBalls.Contains(this.gameObject))
                    {
                        LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().TempBalls.Remove(this.gameObject);
                    }
                }
                readyToFire = true;
                ballDurability = ReturnDurability();
            }
        }
        if (co.gameObject.CompareTag("Trap"))
        {
            co.gameObject.GetComponent<TrapBehaviour>().CollisionTraps(this.gameObject);
        }
        IfActivatedBallHitObstacle();
    }

    void OnTriggerEnter2D(Collider2D co)
    {
        if (co.gameObject.CompareTag("Obstacle"))
        {
            IfActivatedBallHitObstacle();
            ObstacleBehaviour obst = co.gameObject.GetComponent<ObstacleBehaviour>();
            obst.CallFlash();
            GameObject g1 = objectPooler.PoolForHitObjects();
            g1.transform.position = (co.transform.position - transform.position );
            g1.GetComponent<ColorHitEffect>().GradientChange();
            objectPooler.PutHitObjectsInPool(g1);
            obst.DamageHealth(damage);
            GameplayManager.instance.UpdateScore(damage);

            AudioManager.instance.PlayBallHitSFX();

            BuffAndDebuffBaseClass[] tempHolder = GetComponents<BuffAndDebuffBaseClass>();

            foreach (BuffAndDebuffBaseClass BAD in tempHolder)
            {
                if (!BAD.beingDestroyed)
                {
                    if (BAD.GetStatusEffect() == StatusEffects.Petrified)
                    {
                        BAD.DamageThis();
                        if (BAD.HowManyHitsMore() <= 0)
                        {
                            LauncherBehaviour.instance.RemoveBall(this.gameObject);
                            BallPool.instance.Despawn(gameObject); 
                        }
                    }
                }
            }

            ballDurability--;
            if (ballDurability <= 0)
            {
                objectPooler.SpawnFromPool("BallCollectedEffect", new Vector3(transform.position.x, transform.position.y, 0.0f), Quaternion.identity);
                gameObject.SetActive(false);
                if (LauncherBehaviour.instance.GetComponent<TypeOfShotScript>() != null)
                {
                    if (LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().TempBalls.Contains(this.gameObject))
                    {
                        LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().TempBalls.Remove(this.gameObject);
                    }
                }
                readyToFire = true;
                ballDurability = ReturnDurability();
            }
        }
        if (co.gameObject.CompareTag("Trap"))
        {
            co.gameObject.GetComponent<TrapBehaviour>().CollisionTraps(this.gameObject);
        }
        if (co.gameObject.CompareTag("Wall"))
        {
            co.GetComponent<WallEffect>().PlayWallEffect(transform.position);
            Vector3 direction = GetComponent<Rigidbody2D>().velocity.normalized;
            float speed = GetComponent<Rigidbody2D>().velocity.magnitude;

            Vector3 newDirection = new Vector3(-direction.x, direction.y, direction.z);
            Vector3 newVelocity = newDirection.normalized * speed;
            GetComponent<Rigidbody2D>().velocity = newVelocity;
        }
        IfActivatedBallHitObstacle();
    }

    void OnCollisionStay2D(Collision2D co)
	{
		if(co.gameObject.CompareTag("Obstacle"))
		{
			Vector3 dir = co.gameObject.transform.position - transform.position;
			dir = -dir.normalized;
			rb2d.AddForce(dir.normalized * 1.0f * bouncinessUp, ForceMode2D.Impulse);

			if(!obstacles.Contains(co.gameObject))
			{
				obstacles.Add(co.gameObject);
			}
			else if(obstacles.Contains(co.gameObject))
			{
				counterTimer += Time.deltaTime;

				if(counterTimer >= counterDuration)
				{
					rb2d.velocity = Vector3.zero;
					obstacles.Clear();
					gameObject.SetActive(false);
				}
			}
		}
	}
    

	void OnCollisionExit2D(Collision2D co)
	{
		if(co.gameObject.CompareTag("Obstacle"))
		{
			if(obstacles.Contains(co.gameObject))
			{
				obstacles.Remove(co.gameObject);
				counterTimer = 0.0f;
			}
		}
        if (co.gameObject.CompareTag("Ball"))
        {
            BuffAndDebuffBaseClass[] tempHolder = co.gameObject.GetComponents<BuffAndDebuffBaseClass>();
            foreach (BuffAndDebuffBaseClass BAD in tempHolder)
            {
                if (BAD.GetStatusEffect() == StatusEffects.Frozen)
                {
                    BAD.DamageThis();
                    if (BAD.HowManyHitsMore() < 1)
                    {
                        BAD.ChangeFromFrozen();
                        BAD.beingDestroyed = true;
                        Destroy(BAD);
                        co.gameObject.GetComponent<BallBehaviour>().CallForAllBuffsAndDebuffs();
                    }
                }
            }
        }
    }

    void IfActivatedBallHitObstacle()
    {
        if (LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().ActivatedBalls.Contains(this.gameObject))
        {
            if (LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().typeOfShot == TypeOfShot.Impact)
            {
                LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().ActivateActivatedBallFromActivated(this.gameObject);
                return;
            }
        }
        if (LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().explosionOnImpact)
        {
            LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().ActivateActivatedBallFromActivated(this.gameObject);
        }
    }

    void IfActivatedBallHitFloor()
    {
        if (LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().ActivatedBalls.Contains(this.gameObject))
        {
            LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().RemoveBallFromActivated(this.gameObject);
        }
        if (LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().TempBalls.Contains(this.gameObject))
        {
           LauncherBehaviour.instance.GetComponent<TypeOfShotScript>().TempBalls.Remove(this.gameObject);
           Destroy(this.gameObject);
        }
    }

    public int ReturnDurability()
    {
        return dataManager.Durability;
    }
}
