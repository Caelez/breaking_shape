﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorScript : MonoBehaviour
{
	#region Singleton

	public static IndicatorScript instance;
	private void Awake()
	{
		instance = this;
	}

	#endregion

	public GameObject indicator;
	public int amount = 3;
	private List<GameObject> indicators = new List<GameObject>();
	private bool isActivated = false;
    public float timer;
    public Vector3 lastDirection;

	// Use this for initialization
	void Start ()
	{
		for(int i = 0; i < 50; i++)
		{
			GameObject temp = Instantiate(indicator, transform.position, Quaternion.identity);
			temp.transform.SetParent(this.gameObject.transform);
			indicators.Add(temp);

			temp.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (timer > 1f)
        {
            timer = 0;
        }
        else
        {
            timer += Time.deltaTime;
        }
	}

	public void EnableIndicator()
	{
		foreach(GameObject go in indicators)
		{
			go.SetActive(true);
		}
		isActivated = true;
	}

	public void DisableIndicator()
	{
		foreach(GameObject go in indicators)
		{
			go.SetActive(false);
		}
		isActivated = false;
	}

	public void SetIndicatorPosition()
	{
		if(!isActivated)
		{
			EnableIndicator();
		}
        Vector3 touchPos = Vector3.zero;
        if (Input.touchCount > 0)
        {
            touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
        }
        else
        {
            touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        
        touchPos.z = 0.0f;
		Vector3 direction = touchPos - transform.position;
        direction = direction.normalized;
        if (Vector3.Angle(transform.position, direction)  > 100)
        {
            lastDirection = direction;
            foreach (GameObject g in indicators)
            {
                g.SetActive(false);
            }

            float rayDistance = 50;
            RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, rayDistance);
            float distance = Vector3.Distance(new Vector3(hit.point.x, hit.point.y, 0f), transform.position);
            int i = 0;
            do
            {
                indicators[i].SetActive(true);
                indicators[i].transform.position = transform.position + ((1f * i) * direction) + (direction * timer);
                if (Vector3.Distance(indicators[i].transform.position, transform.position) > Vector3.Distance(hit.point, transform.position))
                {
                    indicators[i].SetActive(false);
                }
                i++;
                rayDistance--;
                distance--;
            } while (distance >= 0);
            indicators[i].SetActive(true);
            indicators[i].transform.position = hit.point;
            Vector3 hitPoint = hit.point;
            if (hit.collider.CompareTag("Wall"))
            {
                i++;
                int i2 = 0;
                direction.x = -direction.x;
                hit = Physics2D.Raycast(hitPoint + direction, direction, rayDistance);
                distance = Vector3.Distance(new Vector3(hit.point.x, hit.point.y, 0f), hitPoint);
                do
                {
                    indicators[i + i2].SetActive(true);
                    indicators[i + i2].transform.position = hitPoint + ((1f * i2) * direction) + (direction * timer);
                    rayDistance--;
                    distance--;
                    if (Vector3.Distance(indicators[i + i2].transform.position, hitPoint) > Vector3.Distance(hit.point, hitPoint))
                    {
                        indicators[i + i2].SetActive(false);
                    }
                    i2++;

                } while (distance >= 0);
                indicators[i + i2].SetActive(true);
                indicators[i + i2].transform.position = hit.point;
            }
        }
        else
        {
            direction = lastDirection;
            lastDirection = direction;
            foreach (GameObject g in indicators)
            {
                g.SetActive(false);
            }
            Debug.Log(Vector3.Angle(transform.position, direction));
            float rayDistance = 50;
            RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, rayDistance);
            float distance = Vector3.Distance(new Vector3(hit.point.x, hit.point.y, 0f), transform.position);
            int i = 0;
            do
            {
                indicators[i].SetActive(true);
                indicators[i].transform.position = transform.position + ((1f * i) * direction) + (direction * timer);
                if (Vector3.Distance(indicators[i].transform.position, transform.position) > Vector3.Distance(hit.point, transform.position))
                {
                    indicators[i].SetActive(false);
                }
                i++;
                rayDistance--;
                distance--;
            } while (distance >= 0);
            indicators[i].SetActive(true);
            indicators[i].transform.position = hit.point;
            Vector3 hitPoint = hit.point;
            if (hit.collider != null)
            {
                if (hit.collider.CompareTag("Wall"))
                {
                    i++;
                    int i2 = 0;
                    direction.x = -direction.x;
                    hit = Physics2D.Raycast(hitPoint + direction, direction, rayDistance);
                    distance = Vector3.Distance(new Vector3(hit.point.x, hit.point.y, 0f), hitPoint);
                    do
                    {
                        indicators[i + i2].SetActive(true);
                        indicators[i + i2].transform.position = hitPoint + ((1f * i2) * direction) + (direction * timer);
                        rayDistance--;
                        distance--;
                        if (Vector3.Distance(indicators[i + i2].transform.position, hitPoint) > Vector3.Distance(hit.point, hitPoint))
                        {
                            indicators[i + i2].SetActive(false);
                        }
                        i2++;

                    } while (distance >= 0);
                    indicators[i + i2].SetActive(true);
                    indicators[i + i2].transform.position = hit.point;
                }
            }
        }
    }
}
