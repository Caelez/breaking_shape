﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LauncherBehaviour : MonoBehaviour
{
	public static LauncherBehaviour instance;
	public RectTransform indicator;

    public float ballSize = 0.35f;

    private PlaySceneDataManager dataManager;
    
	public GameObject spawnPoint;
	public GameObject ball;
    [SerializeField] private int numberOfBall = 3;
	public float offsetDistance = 0.5f;
	public float clampAngle = 80.0f;
	public GameObject ballText;	

	public List<GameObject> balls = new List<GameObject>();
    public List<GameObject> hiddenBalls = new List<GameObject>();
	private int total = 0;
//	private bool b_isShoot = false;
    
	private bool b_isReturn = false;

	private GameObject spawnedBomb;
	private bool isBombSpawned = false;

	[HideInInspector] public bool isExploded = false;

    public RuntimeAnimatorController[] BallAnimator = new RuntimeAnimatorController[5];

	public int baseActivateTraps = 0;
	public int currentActivateTraps = 0;

	public Text test;

	void Awake()
	{
		instance = this;
        dataManager = PlaySceneDataManager.instance;
	}

	void Start ()
	{
        dataManager.UpdateUpgradeValue();
        transform.position = new Vector3(0f, 4.4f, 0f);
        numberOfBall = dataManager.Count;
	}

	public void SpawnBalls()
	{
		for (int i = 0; i < numberOfBall; ++i)
		{
            //			GameObject temp = Instantiate(ball, transform.position, transform.rotation);
            GameObject temp = BallPool.instance.Spawn(ball.tag, transform.position, transform.rotation);
			balls.Add(temp);
			hiddenBalls.Add(temp);

			temp.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(3));
		}
       
		b_isReturn = true;
        foreach (GameObject g in hiddenBalls)
        {
            g.SetActive(false);
        }
	}

	void Update ()
	{

		if (Input.GetKeyDown(KeyCode.Space))
		{
			UpdateGameplay();
		}        

//		if(b_isShoot && total == balls.Count)
//		{
//			b_isShoot = false;
//			total = 0;
//		}

		if (balls.Count == 0)
		{
			UpdateGameplay();
			foreach (GameObject g in hiddenBalls)
			{
				g.GetComponent<Collider2D>().enabled = false;
			}
		}
	}

    private IEnumerator CheckReturn()
    {
        while (!b_isReturn)
        {
            yield return new WaitForSeconds(0.2f);
            if (!instance.GetComponent<TypeOfShotScript>().CheckIfAllBallsHome())
            {                
                continue;
            }

            if (!EnemyManagerScript.instance.GetIsCoroutineRunning())
            {
                EnemyManagerScript.instance.SetIsCoroutineRunning(true);
                EnemyManagerScript.instance.StartCoroutine(EnemyManagerScript.instance.InitiateEnemyShot());
            }
            if (!EnemyManagerScript.instance.GetEnemyDoneShooting())
            {
                continue;
            }

            foreach (GameObject g in ObjectPooler.instance.enemyShotPool)
            {
                if (g.activeSelf)
                {
                    continue;
                }
            }
            b_isReturn = true;

            // Update the turn of the game
            UpdateGameplay();            
        }
        StopCoroutine(CheckReturn());
    }
	public void UpdateGameplay()
	{
		if (StageManager.instance)
		{
			foreach (GameObject g in StageManager.instance.obstacles)
			{
				if (g.GetComponent<MapObjectsBase>() != null)
				{
					if (g.GetComponent<MapObjectsBase>().GetBMove())
					{
						return;
					}
				}
			}
		}
		else if (SpawnerBehaviour.instance)
		{
			SpawnerBehaviour.instance.MoveObstaclesUp();
		}
        foreach(GameObject g in hiddenBalls)
        {
            BuffAndDebuffBaseClass[] tempHolder = g.GetComponents<BuffAndDebuffBaseClass>();
            foreach (BuffAndDebuffBaseClass BAD in tempHolder)
            {
                BAD.ReduceDuration();
            }
            g.GetComponent<BallBehaviour>().CallForAllBuffsAndDebuffs();
        }
        
        // Update step count every turn
        GameplayManager.instance.UpdateTurnCount(1);        
			
		GameplayManager.instance.CheckWaveClear(true);

        if (hiddenBalls.Count < 1)
		{
			GameObject temp = Instantiate(ball, transform.position, transform.rotation);
			temp.SetActive(false);
			temp.transform.SetParent(GameplayManager.instance.emptyGameObjectHolder.transform.GetChild(3));

			AddBall(temp);
		}
        ObstacleManager.instance.SpawnNewWave();
        EnemyManagerScript.instance.SetIsCoroutineRunning(false);
        EnemyManagerScript.instance.SetEnemyDoneShooting(false);
    }

    public void ShootBall()
	{
		if(b_isReturn)
		{
			b_isReturn = false;
            Shoot();
            StartCoroutine(CheckReturn());
        }
	}

    // Enable the ball and reset it position to the launcher then add force to it
    void Shoot()
    {
        GetComponent<TypeOfShotScript>().Shoot(balls);
    }

	// Add ball to the launcher
	public void AddBall(GameObject go)
	{		
		// Increase the number of ball
		numberOfBall ++;

		// Set new ball size to 0.35
		if(go.transform.localScale.x != ballSize)
		{
			Vector3 temp = go.transform.localScale;
			temp.x = ballSize;
			temp.y = ballSize;
			go.transform.localScale = temp;
		}

        // Add ball into 'balls' list
        balls.Add(go);
        hiddenBalls.Add(go);
	}

	// Remove ball from the launcher
	public void RemoveBall(GameObject go)
    {
        go.GetComponent<BallBehaviour>().StopAllCoroutines();
        // Remove ball from the list
        balls.Remove(go);
        hiddenBalls.Remove(go);
       

		// Decrease the number of ball
		numberOfBall --;
	}

    // Rotation of the indicator
    public void FaceTouchPosition()
	{
        // Rotate Y Axis toward the direction
        Vector3 touchPos = Vector3.zero;
        if (Input.touchCount > 0)
        {
            touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
        }
        else
        {
            touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
		        
		Vector2 direction = transform.position - touchPos;
		indicator.up = direction;
        var angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        angle += 90;
		// Clamp the rotation angle
		Vector3 currentRotation = indicator.transform.rotation.eulerAngles;
		currentRotation.z = Mathf.Clamp(currentRotation.z, 180.0f - clampAngle, 180.0f + clampAngle);
		transform.rotation = Quaternion.AngleAxis(angle,Vector3.forward );
        if (transform.localEulerAngles.z > 260f)
        {
            transform.localEulerAngles = new Vector3(0f,0f,260f);
        }
        else if (transform.localEulerAngles.z < 100f)
        {
            transform.localEulerAngles = new Vector3(0f, 0f, 100f);

        }
    }

	// To checks all the ball return yet
	public bool GetIsReturn()
	{
		// True when all ball is disabled
		return b_isReturn;
	}

	public void UseBoom(GameObject bomb)
    {
		if(!isBombSpawned)
		{
			isBombSpawned = true;
			isExploded = false;
			spawnedBomb = Instantiate(bomb);
			spawnedBomb.SetActive(true);
			spawnedBomb.transform.position = this.transform.position;
			spawnedBomb.transform.rotation = this.transform.rotation;
		}
    }

    public void addToTotal()
    {
        total++;
    }
}
