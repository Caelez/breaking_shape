﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Level")]
public class Level : ScriptableObject
{
	new public string name;
	public string levelID;
    public RequirementsForStar requirement1 = new RequirementsForStar();
    public RequirementsForStar requirement2 = new RequirementsForStar();
    public RequirementsForStar requirement3 = new RequirementsForStar();

    public List<Wave> wavesList = new List<Wave>();

    public float expRates = 0;

    public int levelNumber;
}


[System.Serializable]
public class RequirementsForStar{
    public RequirementsTypes requirement = new RequirementsTypes();
    public int number = 0;
}
