﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType
{
    Others,
    Currency,
	Consumable,
	Evolution,
	PowerUp,
    total_ItemType
}

//[CreateAssetMenu(menuName = "Item")]
public class Item : ScriptableObject
{
    new public string name = "New Item";
	public string index;
    public ItemType itemType;
    public Sprite sprite = null;
    public string description = "No description";
	public int value = 0;
	//public GameObject prefab;
	//public int amount;

    public virtual void Use()
    {
		//UIManager.instance.ToggleInvnetory();
        //GameplayManager.instance.UseItem(this);
        Debug.Log("Using " + name);
    }
}