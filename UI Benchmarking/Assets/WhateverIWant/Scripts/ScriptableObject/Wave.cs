﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct WaveInfo
{
	public GameObject prefab;
    public int selectedSpriteType;
	public Vector3 position;
	public float rotation;
    public float scale;
	public int health;
    public Sprite sprite;
    public ElementType elementType;
    public PowerType powerType;
    public TrapType trapType;


    [HideInInspector]
    public bool foldout;
}

[CreateAssetMenu(menuName = "Wave")]
public class Wave : ScriptableObject
{
	public List<WaveInfo> wave = new List<WaveInfo>();
}
