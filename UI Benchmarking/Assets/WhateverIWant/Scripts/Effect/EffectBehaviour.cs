﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectBehaviour : MonoBehaviour
{
	public bool isObjectPulling = false;

	public void LastFrame()
	{
		if(isObjectPulling)
		{
			gameObject.SetActive(false);
		}
		else
		{
			Destroy(this.gameObject);
		}
	}
}
