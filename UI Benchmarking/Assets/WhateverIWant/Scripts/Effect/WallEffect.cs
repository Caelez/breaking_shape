﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallEffect : MonoBehaviour
{
	public float m_angle;
	private Quaternion rotation;

	private ObjectPooler objectPooler;

	// Use this for initialization
	void Start ()
	{
		rotation = Quaternion.Euler(0.0f, 0.0f, m_angle);
		objectPooler = ObjectPooler.instance;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	void OnCollisionEnter2D(Collision2D co)
	{
		if(co.gameObject.CompareTag("Ball"))
		{
			if(co.contacts.Length > 0)
			{
				objectPooler.SpawnFromPool("WallEffect", co.contacts[0].point, rotation);
                Vector3 dir = co.gameObject.transform.position - co.transform.position;
                dir = -dir.normalized;

                co.gameObject.GetComponent<Rigidbody2D>().AddForce(dir.normalized * 1.0f , ForceMode2D.Impulse);
                co.gameObject.GetComponent<Rigidbody2D>().velocity = 15 * (co.gameObject.GetComponent<Rigidbody2D>().velocity.normalized);
            }
		}
	}

    public void PlayWallEffect(Vector3 position)
    {
        Vector3 newPosition = position;
        if (m_angle == 90)
        {
            newPosition = new Vector3(position.x, transform.position.y + (transform.position.y - position.y), position.z);
        }
        else
        {
            newPosition = new Vector3(transform.position.x - (transform.position.x - position.x), position.y, position.z);
        }
        objectPooler.SpawnFromPool("WallEffect", newPosition, rotation);
    }

}
