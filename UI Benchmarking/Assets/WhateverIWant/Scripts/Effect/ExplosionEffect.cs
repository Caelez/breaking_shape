﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEffect : MonoBehaviour
{
	public void EndExplode()
	{
	//	LauncherBehaviour.instance.UpdateGameplay();
	}

	void OnTriggerEnter2D(Collider2D co)
	{
		if(co.gameObject.CompareTag("Obstacle"))
		{

            co.gameObject.GetComponent<ObstacleBehaviour>().DamageHealth(3);
        }
	}
}
