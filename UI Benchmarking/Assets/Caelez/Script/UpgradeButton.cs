﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeButton : MonoBehaviour {

    public Button respondButton;
    public Upgrades upgradeType;

	// Use this for initialization
	void Start () {
        if (respondButton == null) respondButton = gameObject.GetComponent<Button>();
        respondButton.onClick.AddListener(RegisterUpgrade);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void RegisterUpgrade()
    {
        UpgradeUIManager.instance.RegisterCurrentUpgrade(upgradeType, respondButton.GetComponent<Image>().sprite);
    }
}
