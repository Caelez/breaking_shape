﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    #region Singleton
    public static GameManager instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);

        //-----Load_Game-----//
        resourceManager.LoadResource();
        upgradeManager.LoadUpgradeLevel();
    }
    #endregion

    public ResourceManager resourceManager;
    public UpgradeManager upgradeManager;
    public SceneSwitch switchSceneManager;

    public void ExitGame()
    {
        resourceManager.SaveResource();
        upgradeManager.SaveUpgradeLevel();
        
        Application.Quit();
    }
}

