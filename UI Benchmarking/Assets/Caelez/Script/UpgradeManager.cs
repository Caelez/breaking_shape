﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeManager : MonoBehaviour {

    public UpgradesProperties[] upgradesProperties;

    public int GetUpgradeLevel(Upgrades _upgrade)
    {
        foreach(UpgradesProperties up in upgradesProperties)
        {
            if (up.upgradeType == _upgrade)
            {
                return up.upgradeLevel;
            }
        }
        return 0;
    }
    public void SaveUpgradeLevel()
    {
        PlayerPrefs.SetInt("DAMAGE", GetUpgradeLevel(Upgrades.DAMAGE));
        PlayerPrefs.SetInt("DURABILITY", GetUpgradeLevel(Upgrades.DURABILITY));
        PlayerPrefs.SetInt("BALLCOUNT", GetUpgradeLevel(Upgrades.COUNT));
        PlayerPrefs.SetInt("MAXHP", GetUpgradeLevel(Upgrades.MAXHP));
        PlayerPrefs.SetInt("REGEN", GetUpgradeLevel(Upgrades.REGEN));
    }

    public void LoadUpgradeLevel()
    {
        foreach (UpgradesProperties _up in upgradesProperties)
        {
            if(_up.upgradeType == Upgrades.DAMAGE) _up.upgradeLevel = PlayerPrefs.GetInt("DAMAGE", GetUpgradeLevel(Upgrades.DAMAGE));
            else if (_up.upgradeType == Upgrades.DURABILITY) _up.upgradeLevel = PlayerPrefs.GetInt("DURABILITY", GetUpgradeLevel(Upgrades.DURABILITY));
            else if (_up.upgradeType == Upgrades.COUNT) _up.upgradeLevel = PlayerPrefs.GetInt("BALLCOUNT", GetUpgradeLevel(Upgrades.COUNT));
            else if (_up.upgradeType == Upgrades.MAXHP) _up.upgradeLevel = PlayerPrefs.GetInt("MAXHP", GetUpgradeLevel(Upgrades.MAXHP));
            else if (_up.upgradeType == Upgrades.REGEN) _up.upgradeLevel = PlayerPrefs.GetInt("REGEN", GetUpgradeLevel(Upgrades.REGEN));
        }
    }

    public void ResetUpgradeLevel()
    {
        PlayerPrefs.SetInt("DAMAGE", 0);
        PlayerPrefs.SetInt("DURABILITY", 0);
        PlayerPrefs.SetInt("BALLCOUNT", 0);
        PlayerPrefs.SetInt("MAXHP", 0);
        PlayerPrefs.SetInt("REGEN", 0);
    }

}
[System.Serializable]
public class UpgradesProperties
{
    public Upgrades upgradeType;
    public string description;
    public int baseCost;
    public int increment;
    public bool isCrystal = false;
    public int upgradeLevel = 0;
    [HideInInspector]
    public bool maxLevel = false;

    public void ApplyUpgrade()
    {
        upgradeLevel += 1;
        if (upgradeLevel >= 10) maxLevel = true;
        Debug.Log(upgradeType + " upgraded to level: " + upgradeLevel);
    }
}
public enum Upgrades
{
    DAMAGE,
    COUNT,
    DURABILITY,
    MAXHP,
    REGEN
}
