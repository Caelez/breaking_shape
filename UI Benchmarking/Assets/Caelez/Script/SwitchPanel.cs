﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchPanel : MonoBehaviour {

    public static SwitchPanel instance;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
//        DontDestroyOnLoad(gameObject);
    }

    public GameObject[] panel;

    public void ActivatePanel(string _name)
    {
        foreach(GameObject _panel in panel)
        {
            _panel.SetActive(false);
            if (_panel.name == _name) _panel.SetActive(true);
        }
    }
}


