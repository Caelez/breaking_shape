﻿using UnityEngine;
using UnityEngine.UI;

public class SwitchSceneButton : MonoBehaviour {

    [SerializeField] private Button respondButton;
    [SerializeField] private string sceneName;

	// Use this for initialization
	void Start () {
        if (respondButton == null) respondButton = gameObject.GetComponent<Button>();
        if (sceneName == null || sceneName == "") Debug.LogError(respondButton + " do not have a scene string.");
        respondButton.onClick.AddListener(SwitchScene);
	}
	
    private void SwitchScene()
    {
        GameManager.instance.switchSceneManager.SwitchScene(sceneName);
    }
}
