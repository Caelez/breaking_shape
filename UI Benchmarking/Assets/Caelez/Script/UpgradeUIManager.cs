﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeUIManager : MonoBehaviour {

    public static UpgradeUIManager instance;
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private ResourceManager tempRM;

    [SerializeField] private Text insufficientText;
    private bool isDisplayed = false;

    [SerializeField] private Text levelText;
    [SerializeField] private Text pointText;
    [SerializeField] private Text coinText;
    [SerializeField] private Text crystalText;

    [SerializeField] private Image coinImage;
    [SerializeField] private Image crystalImage;

    public Upgrades currentUpgradeType;
    public Image currentImage;
    public Image currentCostIcon;
    public Text currentDescription;
    public Text currentCostText;

    private Sprite defaultImageIcon;
    private Sprite defaultCostIcon;

    private UpgradesProperties currentUpgradeProperties;
    private bool isCurrentSpendingCrystal = false;
    private int currentCost = 0;

    public Image[] progressBar;
    public Button upgrade;

	// Use this for initialization
	void Start () {
        tempRM = GameManager.instance.resourceManager;
        if (currentImage.sprite != null) defaultImageIcon = currentImage.sprite;
        if (currentCostIcon.sprite != null) defaultCostIcon = currentCostIcon.sprite;
        UpdateUIText();
        CleanUIDisplayCache();
	}

    public void RegisterCurrentUpgrade(Upgrades _upgradeType, Sprite _image)
    {
        UpgradeManager um = GameManager.instance.upgradeManager;
        foreach(UpgradesProperties up in um.upgradesProperties)
        {
            if(up.upgradeType == _upgradeType)
            {
                //Do Something
                RegisterCurrentUpgradeProperties(up, _image);
                break;
            }
        }
    }

    private void RegisterCurrentUpgradeProperties(UpgradesProperties _upgrade, Sprite _image)
    {
        upgrade.onClick.RemoveAllListeners();
        CleanUIDisplayCache();

        currentUpgradeProperties = _upgrade;
        currentUpgradeType = currentUpgradeProperties.upgradeType;
        currentImage.sprite = _image;
        currentDescription.text = currentUpgradeProperties.description;
        UpdateCostInfo(currentUpgradeProperties.baseCost, currentUpgradeProperties.increment, currentUpgradeProperties.upgradeLevel, currentUpgradeProperties.isCrystal);
        UpdateProgressBar(currentUpgradeProperties.upgradeLevel);

        upgrade.onClick.AddListener(OnUpdadeButtonClicked);
    }

    private void CleanUIDisplayCache()
    {
        currentUpgradeProperties = null;
        currentDescription.text = "";
        currentImage.sprite = defaultImageIcon;
        currentCostIcon.sprite = defaultCostIcon;
        currentCost = 0;
        currentCostText.text = "";
        UpdateProgressBar(0);
    }
    private void UpdateCostInfo(int _base, int _increment, int _level, bool _isCrystal)
    {
        isCurrentSpendingCrystal = _isCrystal;
        currentCostIcon.sprite = RetrieveCostIcon(isCurrentSpendingCrystal);       
        currentCost = CalculateCost(_base, _increment, _level);
        currentCostText.text = currentCost.ToString();
    }
    private Sprite RetrieveCostIcon(bool _isCrystal)
    {
        if (_isCrystal) return crystalImage.sprite;
        else return coinImage.sprite;
    }
    private int CalculateCost(int _base, int _increment, int _level)
    {
        int cost = (int)(_base * Mathf.Pow(_increment,_level));

        return cost;
    }
    private void UpdateProgressBar(int _level)
    {
        foreach(Image node in progressBar)
        {
            node.color = Color.black;
        }

        for(int i = 0; i < _level; i++)
        {
            progressBar[i].color = Color.red;
        }
    }

    private void OnUpdadeButtonClicked()
    {
        if (!currentUpgradeProperties.maxLevel)
        {
            if (tempRM.AttemptToSpend(currentCost, isCurrentSpendingCrystal))
            {
                currentUpgradeProperties.ApplyUpgrade();
                UpdateProgressBar(currentUpgradeProperties.upgradeLevel);
                UpdateCostInfo(currentUpgradeProperties.baseCost, currentUpgradeProperties.increment, currentUpgradeProperties.upgradeLevel, currentUpgradeProperties.isCrystal);
                UpdateUIText();
            }
            else
                RunErrorText();
        }
    }

    private void UpdateUIText()
    {
        levelText.text = tempRM.Level.ToString();
        pointText.text = tempRM.Point.ToString();
        coinText.text = tempRM.Coin.ToString();
        crystalText.text = tempRM.Crystal.ToString();
    }

    private void RunErrorText()
    {
        if (!isDisplayed)
        {
            insufficientText.gameObject.SetActive(true);
            StartCoroutine(DisableErrorText());
            isDisplayed = true;
        }
    }
    private IEnumerator DisableErrorText()
    {
        yield return new WaitForSeconds(3);
        insufficientText.gameObject.SetActive(false);
        isDisplayed = false;
    }
}
