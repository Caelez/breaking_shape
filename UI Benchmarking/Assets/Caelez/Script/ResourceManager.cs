﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour {

    [SerializeField] private int level;
    [SerializeField] private int point;
    [SerializeField] private int coin;
    [SerializeField] private int crystal;

    public int Level { get { return level; } }
    public int Point { get { return point; } }
    public int Coin { get { return coin; } }
    public int Crystal { get { return crystal; } }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SaveResource()
    {
        PlayerPrefs.SetInt("LEVEL", Level);
        PlayerPrefs.SetInt("POINT", Point);
        PlayerPrefs.SetInt("COIN", Coin);
        PlayerPrefs.SetInt("CRYSTAL", Crystal);
    }
    public void LoadResource()
    {
        level = PlayerPrefs.GetInt("LEVEL", Level);
        point = PlayerPrefs.GetInt("POINT", Point);
        coin = PlayerPrefs.GetInt("COIN", Coin);
        crystal = PlayerPrefs.GetInt("CRYSTAL", Crystal);
    }

    public void GainResource(int _amount, bool _isCrystal = false)
    {
        if (_isCrystal) crystal += _amount;
        else coin += _amount;
    }

    public bool AttemptToSpend(int _amount, bool _isCrystal = false)
    {
        if(_isCrystal)
        {
            if (_amount > crystal) return false;
            else return SpendCrystal(_amount);
        }
        else
        {
            if (_amount > coin) return false;
            else return SpendCoin(_amount);
        }
    }

    private bool SpendCoin(int _amount)
    {
        coin -= _amount;
        return true;
    }

    private bool SpendCrystal(int _amount)
    {
        crystal -= _amount;
        return true;
    }
}
