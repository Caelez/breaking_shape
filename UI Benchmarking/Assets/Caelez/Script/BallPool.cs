﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPool : MonoBehaviour {

    #region Singleton
    public static BallPool instance;
    private void Awake()
    {
        if (instance == null) instance = this;
        if (instance != this) Destroy(gameObject);
        Initiate();

    }
    #endregion

    [SerializeField] private List<BallGameObjectPool> pools;

    private Dictionary<string, Queue<GameObject>> dictionaryPool;
    private Dictionary<string, Transform> parentPool;
    private Dictionary<string, GameObject> prefabCopy;


    // Use this for initialization
    void Start () {
        
    }
	
    public GameObject Spawn(string _tag, Vector3 _position, Quaternion _rotation)
    {
        if (!dictionaryPool.ContainsKey(_tag))
        {
            Debug.LogWarning("Pool with tag " + _tag + " doesn't exist");
            return null;
        }
        GameObject spawnObject = null;
        if (dictionaryPool[_tag].Count > 0) spawnObject = dictionaryPool[_tag].Dequeue();
        else spawnObject = SpawnExtraCopy(_tag);
        if (spawnObject != null)
        {
            spawnObject.SetActive(true);
            spawnObject.transform.position = _position;
            spawnObject.transform.rotation = _rotation;
        }
        return spawnObject;
    }

    public GameObject Spawn(string _tag, Transform _parent)
    {
        return Spawn(_tag, _parent.position, _parent.rotation);
    }

    public void Despawn(GameObject _obj)
    {
        if (dictionaryPool.ContainsKey(_obj.tag))
        {
            _obj.SetActive(false);
            dictionaryPool[_obj.tag].Enqueue(_obj);
            if (parentPool.ContainsKey(_obj.tag)) _obj.transform.SetParent(parentPool[_obj.tag]);
            else _obj.transform.SetParent(gameObject.transform);
        }
    }

    private void Initiate()
    {
        dictionaryPool = new Dictionary<string, Queue<GameObject>>();
        parentPool = new Dictionary<string, Transform>();
        prefabCopy = new Dictionary<string, GameObject>();
        foreach (BallGameObjectPool _bgop in pools)
        {
            if (_bgop.isExpandable)
            {
                prefabCopy.Add(_bgop.tag, _bgop.prefab);
            }
            if (_bgop.parent != null)
            {
                parentPool.Add(_bgop.tag, _bgop.parent.transform);
            }
            Queue<GameObject> poolObject = new Queue<GameObject>();
            for (int i = 0; i < _bgop.size; i++)
            {
                GameObject go;
                if (_bgop.parent != null)
                {
                    go = Instantiate(_bgop.prefab, _bgop.parent.transform);
                    go.transform.SetParent(_bgop.parent.transform);
                }
                else
                {
                    go = Instantiate(_bgop.prefab, gameObject.transform);
                    go.transform.SetParent(gameObject.transform);
                }
                go.SetActive(false);
                poolObject.Enqueue(go);
            }
            dictionaryPool.Add(_bgop.tag, poolObject);
        }
    }

    private GameObject SpawnExtraCopy(string _tag)
    {
        Transform tempParent = transform;
        if (parentPool.ContainsKey(_tag)) tempParent = parentPool[_tag];
        GameObject obj = Instantiate(prefabCopy[_tag], tempParent);
        obj.transform.SetParent(tempParent);
        obj.SetActive(false);
        return obj;
    }

    [System.Serializable]
    public class BallGameObjectPool
    {
        public string tag;
        public GameObject prefab;
        public GameObject parent;
        public int size;
        public bool isExpandable;
    }
}
