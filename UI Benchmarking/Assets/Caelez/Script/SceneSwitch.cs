﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSwitch : MonoBehaviour {

    public string[] sceneName;
    public GameObject loadingScreens;
    public GameObject loadingBar;
    public Image fill;
    public Text progressPercentage;

    public void SwitchScene(string _sceneName)
    {
        if(_sceneName == "Quit") GameManager.instance.ExitGame();

        bool isScene = false;
        foreach(string _sn in sceneName)
        {
            if(_sn == _sceneName)
            {
               // SceneManager.LoadScene (_sceneName);
                SwitchToScene(_sceneName);
                isScene = true;
                break;
            }
        }
        if(!isScene) Debug.LogWarning(_sceneName + " is not registered.");
    }

    public void SwitchToScene(string _sn)
    {
        Time.timeScale = 1.0f;
        StartCoroutine(LoadAsynchronously(_sn));
    }

    public IEnumerator LoadAsynchronously(string _sn)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(_sn);

        loadingScreens.SetActive(true);
        loadingBar.SetActive(true);

        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            fill.fillAmount = progress;
            progressPercentage.text = "Loading " + (progress * 100.0f).ToString("F0") + "%";
            yield return null;
        }

        loadingScreens.SetActive(false);
        loadingBar.SetActive(false);
        yield return null;
    }

}

