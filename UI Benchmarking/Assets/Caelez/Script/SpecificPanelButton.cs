﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecificPanelButton : MonoBehaviour {

    [SerializeField] private string respondPanel;
    [SerializeField] private Button respondButton;
	// Use this for initialization
	void Start () {
        if (respondButton == null) respondButton = gameObject.GetComponent<Button>();
        respondButton.onClick.AddListener(ChangePanel);
	}

    private void ChangePanel()
    {
        SwitchPanel.instance.ActivatePanel(respondPanel);
    }
	
}
