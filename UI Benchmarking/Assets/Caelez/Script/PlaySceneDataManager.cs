﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySceneDataManager : MonoBehaviour {

    #region Singleton
    public static PlaySceneDataManager instance;
    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);
        //will destroy on other scene since only necessary for play scene.
    }
    #endregion

    //-----Upgradable Base Value-----//
    [SerializeField] private int baseDamage = 3;
    [SerializeField] private int baseDurability = 3;
    [SerializeField] private int baseBallCount = 3;
    [SerializeField] private int baseMaxHP = 200;
    [SerializeField] private int baseRegen = 20;
    //-----Upgrade Value-----//
    private int upgradeDamage;
    private int upgradeDurability;
    private int upgradeBallCount;
    private int upgradeMaxHP;
    private int upgradeRegen;
    //-----Multiplier-----//
    [SerializeField] private int damageUpgradeMultiplier = 1;
    [SerializeField] private int durabilityUpgradeMultiplier = 1;
    [SerializeField] private int ballCountUpgradeMultiplier = 1;
    [SerializeField] private int maxHPUpgradeMultiplier = 20;
    [SerializeField] private int regenUpgradeMultiplier = 5;

    //-----Basic Properties-----//
    [SerializeField] private int ballMaxCount = 99;
    
    //-----Data Getter-----//
    public int Damage { get { return baseDamage + upgradeDamage; } }
    public int Count { get { return baseBallCount + upgradeBallCount; } }
    public int Durability { get { return baseDurability + upgradeDurability; } }
    public int MaxHP { get { return baseMaxHP + upgradeMaxHP; } }
    public int Regen { get { return baseRegen + upgradeRegen; } }
    public int MaxCount { get { return ballMaxCount; } }

    public void UpdateUpgradeValue()
    {
        upgradeDamage = GameManager.instance.upgradeManager.GetUpgradeLevel(Upgrades.DAMAGE) * damageUpgradeMultiplier;
        upgradeDurability = GameManager.instance.upgradeManager.GetUpgradeLevel(Upgrades.DURABILITY) * durabilityUpgradeMultiplier;
        upgradeBallCount = GameManager.instance.upgradeManager.GetUpgradeLevel(Upgrades.COUNT) * ballCountUpgradeMultiplier;
        upgradeMaxHP = GameManager.instance.upgradeManager.GetUpgradeLevel(Upgrades.MAXHP) * maxHPUpgradeMultiplier;
        upgradeRegen = GameManager.instance.upgradeManager.GetUpgradeLevel(Upgrades.REGEN) * regenUpgradeMultiplier;
    }
}
